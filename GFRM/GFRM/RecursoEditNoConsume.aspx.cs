﻿using Class_Library;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GFRM
{
    public partial class RecursoEditNoConsume : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilizador user = Session["User"] as Utilizador;
            if (user.Permissao != "Admin" && user.Permissao != "Worker")
            {
                Response.Redirect("ErrorPage.aspx");
            }
            if (!IsPostBack)
            {
                FillCat();
                FillFields();
                Grid();
            }
        }

        protected void ButtonEdit_Click(object sender, EventArgs e)
        {
            RecursoNaoConsumivel recurso = Session["RecursoEdit"] as RecursoNaoConsumivel;
            DAL dal = new DAL();

            recurso.Nome = string.IsNullOrWhiteSpace(TextBoxName.Text) ? recurso.Nome : TextBoxName.Text;
            recurso.Categoria = (DropDownCategory.SelectedIndex == 0) ? recurso.Categoria : DropDownCategory.SelectedValue;
            bool updated = dal.UpdateRecursoNaoConsumivel(recurso);

            if (updated)
            {
                Session["RecursoEdit"] = null;
                Session["comeback"] = "NoConsume";
                ScriptManager.RegisterStartupScript(this, this.GetType(),
                    "alert",
                    $"alert('{recurso.Nome} modificado com sucesso!');window.location ='ResourceMNG.aspx';",
                    true);
            }
        }

        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            Session["RecursoEdit"] = null;
            Session["comeback"] = "NoConsume";
            Response.Redirect("ResourceMNG.aspx");
        }
        protected void FillFields()
        {
            RecursoNaoConsumivel recurso = Session["RecursoEdit"] as RecursoNaoConsumivel;
            if (recurso != null)
            {
                TextBoxName.Text = Server.HtmlDecode(recurso.Nome);
                DropDownCategory.SelectedIndex = DropDownCategory.Items.IndexOf(DropDownCategory.Items.FindByValue(recurso.Categoria));
            }
        }
        private void FillCat()
        {
            DataTable categoria = new DAL().GetCategorias();
            DropDownCategory.DataSource = categoria;
            DropDownCategory.DataTextField = "Nome";
            DropDownCategory.DataValueField = "Nome";
            DropDownCategory.DataBind();
            DropDownCategory.Items.Insert(0, "Selecione a categoria");
        }
        protected void Grid()
        {
            gvFormacoes.DataSource = new DAL().SelectFormacoes(Session["RecursoEdit"] as RecursoNaoConsumivel);
            gvFormacoes.DataBind();
        }
        protected void gvFormacoes_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvFormacoes.PageIndex = e.NewPageIndex;
            Grid();
        }
        protected void btnAvailability_Click(object sender, EventArgs e)
        {
            new DAL().ExecDisponibilidadesResource(Session["RecursoEdit"] as RecursoNaoConsumivel, NoConsumeAvailabilityCalendar.SelectedDate);
            Session["DispononibilidadesNoConsume"] = new DAL().SelectIndisponibilidadeRecurso(Session["RecursoEdit"] as RecursoNaoConsumivel);
            updPanel.Update();
        }
        protected void NoConsumeAvailabilityCalendar_DayRender(object sender, DayRenderEventArgs e)
        {
            try
            {
                List<Disponibilidade> disponibilidades = Session["DispononibilidadesNoConsume"] as List<Disponibilidade>;
                if (disponibilidades.Count != 0 && disponibilidades != null)
                {
                    foreach (Disponibilidade disponibilidade in disponibilidades)
                    {
                        e.Cell.ForeColor = disponibilidade.Data == e.Day.Date && e.Day.Date >= DateTime.Today ? Color.Red : e.Cell.ForeColor;
                        e.Day.IsSelectable = disponibilidade.Data == e.Day.Date && e.Day.Date >= DateTime.Today ? false : e.Day.IsSelectable;
                    }
                }
            }
            catch (Exception ex)
            {
                // Erro
            }
            e.Cell.BackColor = e.Day.Date < DateTime.Today || e.Day.IsWeekend ? Color.LightGray : e.Cell.BackColor;
            e.Day.IsSelectable = e.Day.Date < DateTime.Today || e.Day.IsWeekend ? false : e.Day.IsSelectable;
        }
        protected void NoConsumeAvailabilityCalendar_SelectionChanged(object sender, EventArgs e)
        {

        }
        protected void NoConsumeAvailabilityCalendar_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
        {
            updPanel.Update();
        }
    }
}