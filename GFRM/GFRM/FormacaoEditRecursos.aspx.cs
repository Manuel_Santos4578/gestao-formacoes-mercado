﻿using Class_Library;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GFRM
{
    public partial class FormacaoEditRecursos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilizador user = Session["User"] as Utilizador;
            if (user.Permissao != "Admin" && user.Permissao != "Coordenador")
            {
                Response.Redirect("ErrorPage.aspx");
            }
            if (!IsPostBack)
            {
                HideButtonsLblsTxt();
                SessionSave();
                DatasToDateTimes();
                Grid();
                GridConsumes();
                GridNoConsumes();
            }
        }
        protected void HideButtonsLblsTxt()
        {
            lblConsume.Visible = false;
            txtQtdConsume.Visible = false;
            btnAdicionarConsume.Visible = false;
            btnAdicionarNoConsume.Visible = false;
            btnRemoveConsume.Visible = false;
            btnRemoveNoConsume.Visible = false;
            lblRemoveConsume.Visible = false;
            txtRemoveConsume.Visible = false;
            btnRemoveOne.Visible = false;
        }
        protected void SessionSave()
        {
            Formacao formacao = Session["FormacaoEdit"] as Formacao;
            Session["DatasEdit"] = formacao.ListaDiaFormacao;
            Session["NConsumesOriginal"] = formacao.ListaNConsumiveis ?? new List<RecursoNaoConsumivel>();
            Session["NConsumesEdit"] = formacao.ListaNConsumiveis ?? new List<RecursoNaoConsumivel>();
            Session["ConsumesEdit"] = formacao.ListaConsumivel ?? new List<RecursoConsumivel>();
        }
        protected void DatasToDateTimes()
        {
            try
            {
                Formacao formacao = Session["FormacaoEdit"] as Formacao;
                List<DiaFormacao> diaFormacaos = formacao.ListaDiaFormacao;
                List<DateTime> dateTimes = new List<DateTime>();
                foreach (DiaFormacao diaFormacao in diaFormacaos)
                {
                    dateTimes.Add(diaFormacao.Data);
                }
                Session["DateTimesEdit"] = dateTimes;
            }
            catch (Exception ex)
            {
                Response.Redirect("Formacoes.aspx");
            }
        }
        protected void btnEditarRecursosFormacao_Click(object sender, EventArgs e)
        {
            Formacao formacao = Session["FormacaoEdit"] as Formacao;
            bool updated = new DAL().UpdateDisponibilidadesFormacao(formacao);
            if (updated)
            {
                Session["FormacaoEdit"] = null;
                Response.Redirect("Formacoes.aspx");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Ocorreu um erro ao atualizar');", true);
            }
        }
        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            Session["FormacaoEdit"] = null;
            Session["DatasEdit"] = null;
            Session["DateTimesEdit"] = null;
            Session["NConsumesOriginal"] = null;
            Session["NConsumesEdit"] = null;
            Session["ConsumesEdit"] = null;
            Session["ConsumeListEdit"] = null;
            Response.Redirect("Formacoes.aspx");
        }
        #region Consumegrid 
        protected void Grid()
        {
            DataTable dt = new DAL().GetRecursosConsumiveis();
            //save the datatable into a viewstate for later use
            ViewState["myViewState"] = dt;
            //bind the datasource to the gridview
            GridConsume.DataSource = dt;
            GridConsume.DataBind();
            //to make sure the data isn't loaded in postback
            //use a datatable for storing all the data
            DataTable dt2;
            if (Session["DatasEdit"] as List<DiaFormacao> != null)
            {
                List<DateTime> dateTimes = Session["DateTimesEdit"] as List<DateTime>;
                dt2 = new DAL().GetRecursosNaoConsumiveis(dateTimes);
            }
            else
            {
                dt2 = new DAL().GetRecursosNaoConsumiveis();
            }
            //save the datatable into a viewstate for later use
            ViewState["myViewState2"] = dt2;
            //bind the datasource to the gridview
            GridNoConsume.DataSource = dt2;
            GridNoConsume.DataBind();
        }
        protected void btnAdicionarConsume_Click(object sender, EventArgs e)
        {
            List<RecursoConsumivel> recursoConsumivels =
                Session["ConsumeListEdit"] as List<RecursoConsumivel> ?? new List<RecursoConsumivel>();
            foreach (GridViewRow row in GridConsume.Rows)
            {
                if (row.RowIndex == GridConsume.SelectedIndex)
                {
                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                    RecursoConsumivel recursoConsumivel = new DAL().SelectRecursoConsumivel(int.Parse(row.Cells[0].Text));
                    if (recursoConsumivels.Count != 0)
                    {
                        bool found = false;
                        foreach (RecursoConsumivel Consumivel in recursoConsumivels)
                        {
                            if (Consumivel.Id == recursoConsumivel.Id)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            recursoConsumivel.Saida = int.Parse(txtQtdConsume.Text);
                            if (recursoConsumivel.Saida <= int.Parse(row.Cells[3].Text))
                            {
                                recursoConsumivels.Add(recursoConsumivel);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert",
                                $"alert('Não é possivel exceder o stock existente!');", true);
                            }
                            Session["ConsumeListEdit"] = recursoConsumivels;
                        }
                        else
                        {
                            int index = recursoConsumivels.IndexOf(recursoConsumivels.FirstOrDefault(obj => obj.Id == recursoConsumivel.Id));
                            if ((recursoConsumivels[index].Saida + int.Parse(txtQtdConsume.Text) <= int.Parse(row.Cells[3].Text)))
                            {
                                recursoConsumivels[index].Saida += int.Parse(txtQtdConsume.Text);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert",
                                $"alert('Não é possivel exceder o stock existente!');", true);
                            }
                        }
                    }
                    else
                    {
                        recursoConsumivel.Saida = int.Parse(txtQtdConsume.Text);
                        if (recursoConsumivel.Saida <= int.Parse(row.Cells[3].Text))
                        {
                            recursoConsumivels.Add(recursoConsumivel);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert",
                                $"alert('Não é possivel exceder  o stock existente!');", true);
                        }
                        Session["ConsumeListEdit"] = recursoConsumivels;

                    }
                    GridConsumes();
                }
            }
        }
        protected void GridConsume_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridConsume.PageIndex = e.NewPageIndex;
            Grid();
        }
        protected void GridConsume_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridConsume.Rows)
            {
                if (row.RowIndex == GridConsume.SelectedIndex)
                {
                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                    btnAdicionarConsume.Visible = true;
                    lblConsume.Visible = true;
                    txtQtdConsume.Visible = true;
                }
                else
                {
                    row.BackColor = ColorTranslator.FromHtml("#FFFFFF");
                    row.ToolTip = "Clique para selecionar";
                }
            }
        }
        protected void GridConsume_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(GridConsume, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = "Clique para selecionar";
            }
        }
        #endregion
        #region ConsumeFormacaoGrid
        protected void GridConsumes()
        {
            List<RecursoConsumivel> recursoConsumivels = Session["ConsumeListEdit"] as List<RecursoConsumivel>;
            ConsumeFormacao.DataSource = recursoConsumivels;
            ConsumeFormacao.DataBind();
        }
        protected void btnRemoveConsume_Click(object sender, EventArgs e)
        {
            List<RecursoConsumivel> recursoConsumivels = Session["ConsumeListEdit"] as List<RecursoConsumivel>;
            try
            {
                foreach (GridViewRow row in ConsumeFormacao.Rows)
                {
                    if (row.RowIndex == ConsumeFormacao.SelectedIndex)
                    {
                        row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                        row.ToolTip = string.Empty;
                        recursoConsumivels.Remove(recursoConsumivels.FirstOrDefault(obj => obj.Id == int.Parse(row.Cells[0].Text)));
                        Session["ConsumeListEdit"] = recursoConsumivels;
                    }
                }
            }
            catch (Exception ex) { }
            finally { GridConsumes(); UPFAR.Update(); }
        }
        protected void btnRemoveOne_Click(object sender, EventArgs e)
        {
            List<RecursoConsumivel> recursoConsumivels = Session["ConsumeListEdit"] as List<RecursoConsumivel>;
            foreach (GridViewRow row in ConsumeFormacao.Rows)
            {
                if (row.RowIndex == ConsumeFormacao.SelectedIndex)
                {
                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                    int index = recursoConsumivels.IndexOf(recursoConsumivels.FirstOrDefault(obj => obj.Id == int.Parse(row.Cells[0].Text)));
                    if ((recursoConsumivels[index].Saida - int.Parse(txtRemoveConsume.Text)) > 0)
                    {
                        recursoConsumivels[index].Saida -= int.Parse(txtRemoveConsume.Text);
                        txtRemoveConsume.Text = "1";
                        Session["ConsumeListEdit"] = recursoConsumivels;
                        GridConsumes();
                    }
                    else
                    {
                        recursoConsumivels.Remove(recursoConsumivels.FirstOrDefault(obj => obj.Id == int.Parse(row.Cells[0].Text)));
                        GridConsumes();
                    }
                }
            }
        }
        protected void ConsumeFormacao_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(ConsumeFormacao, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = "Clique para selecionar.";
            }
        }
        protected void ConsumeFormacao_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ConsumeFormacao.PageIndex = e.NewPageIndex;
            GridConsumes();
        }
        protected void ConsumeFormacao_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (GridViewRow row in ConsumeFormacao.Rows)
            {
                if (row.RowIndex == ConsumeFormacao.SelectedIndex)
                {
                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                    btnRemoveConsume.Visible = true;
                    lblRemoveConsume.Visible = true;
                    txtRemoveConsume.Visible = true;
                    btnRemoveOne.Visible = true;
                }
                else
                {
                    row.BackColor = ColorTranslator.FromHtml("#FFFFFF");
                    row.ToolTip = "Clique para selecionar";
                }
            }

        }
        #endregion
        #region NoConsumeGrid
        protected void GridNoConsumes()
        {
            List<RecursoNaoConsumivel> recursoNaoConsumivels = Session["NConsumesEdit"] as List<RecursoNaoConsumivel>;
            NoConsumesFormacao.DataSource = recursoNaoConsumivels;
            NoConsumesFormacao.DataBind();
        }
        protected void btnAdicionarNoConsume_Click(object sender, EventArgs e)
        {
            List<RecursoNaoConsumivel> recursoNaoConsumivels = Session["NConsumesEdit"] as List<RecursoNaoConsumivel>;
            foreach (GridViewRow row in GridNoConsume.Rows)
            {
                if (row.RowIndex == GridNoConsume.SelectedIndex)
                {
                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                    RecursoNaoConsumivel recursoNaoConsumivel = new DAL().SelectRecursoNaoConsumivel(int.Parse(row.Cells[0].Text));
                    if (recursoNaoConsumivels.Count != 0)
                    {
                        bool found = false;
                        foreach (RecursoNaoConsumivel naoConsumivel in recursoNaoConsumivels)
                        {
                            if (naoConsumivel.Id == recursoNaoConsumivel.Id)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            recursoNaoConsumivels.Add(recursoNaoConsumivel);
                            Session["NConsumesEdit"] = recursoNaoConsumivels;
                            GridNoConsumes();
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(),
                            "alert",
                            "alert('Esse já está lá!');",
                            true);
                        }
                    }
                    else
                    {
                        recursoNaoConsumivels.Add(recursoNaoConsumivel);
                        Session["NConsumesEdit"] = recursoNaoConsumivels;
                        GridNoConsumes();
                    }
                }
            }
        }
        protected void GridNoConsume_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridNoConsume.PageIndex = e.NewPageIndex;
            Grid();
        }
        protected void GridNoConsume_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(GridNoConsume, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = "Clique para selecionar..";
            }
        }
        protected void GridNoConsume_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridNoConsume.Rows)
            {
                if (row.RowIndex == GridNoConsume.SelectedIndex)
                {
                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                    btnAdicionarNoConsume.Visible = true;
                }
                else
                {
                    row.BackColor = ColorTranslator.FromHtml("#FFFFFF");
                    row.ToolTip = "Clique para selecionar";
                }
            }
        }
        #endregion
        #region NoConsumeFormacao
        protected void btnRemoveNoConsume_Click(object sender, EventArgs e)
        {
            List<RecursoNaoConsumivel> originalNconsumes = Session["NConsumesOriginal"] as List<RecursoNaoConsumivel>;
            try
            {
                foreach (GridViewRow row in NoConsumesFormacao.Rows)
                {
                    if (row.RowIndex == NoConsumesFormacao.SelectedIndex)
                    {
                        if (originalNconsumes.FirstOrDefault(obj => obj.Id == int.Parse(row.Cells[0].Text)) != null)
                        {
                            return;
                        }
                        List<RecursoNaoConsumivel> recursoNaoConsumivels = Session["NConsumesEdit"] as List<RecursoNaoConsumivel>;
                        recursoNaoConsumivels.Remove(recursoNaoConsumivels.FirstOrDefault(obj => obj.Id == int.Parse(row.Cells[0].Text)));
                        Session["NConsumesEdit"] = recursoNaoConsumivels;

                    }
                }
            }
            catch (Exception ex)
            {
            }
            GridNoConsumes();
            UPFAR.Update();
        }
        protected void NoConsumesFormacao_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(NoConsumesFormacao, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = "Clique para selecionar";
            }
        }
        protected void NoConsumesFormacao_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            NoConsumesFormacao.PageIndex = e.NewPageIndex;
            GridNoConsumes();
        }
        protected void NoConsumesFormacao_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (GridViewRow row in NoConsumesFormacao.Rows)
            {
                if (row.RowIndex == NoConsumesFormacao.SelectedIndex)
                {
                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                    btnRemoveNoConsume.Visible = true;
                }
                else
                {
                    row.BackColor = ColorTranslator.FromHtml("#FFFFFF");
                    row.ToolTip = "Clique para selecionar.";
                }
            }
        }
        #endregion
    }
}