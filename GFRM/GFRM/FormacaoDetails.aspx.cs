﻿using Class_Library;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GFRM
{
    public partial class FormacaoDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilizador user = Session["User"] as Utilizador;
            CalendarAvailability.VisibleMonthChanged += new MonthChangedEventHandler(CalendarAvailability_VisibleMonthChanged);
            if (!IsPostBack)
            {
                Formacao formacao = Session["FormacaoDetails"] as Formacao;
                if (formacao == null) Response.Redirect("home.aspx");
                FillFields();
                Grid();
            }
        }
        protected void FillFields()
        {
            Formacao formacao = Session["FormacaoDetails"] as Formacao;
            if (formacao != null)
            {
                txtEstado.Text = Server.HtmlDecode(formacao.Estado);
                txtNome.Text = Server.HtmlDecode(formacao.Nome);
                txtCliente.Text = Server.HtmlDecode(formacao.Cliente.Nome);
                txtCategoria.Text = Server.HtmlDecode(formacao.Categoria);
                txtFormador.Text = Server.HtmlDecode(formacao.Formador.Nome);
                txtLocal.Text = Server.HtmlDecode(formacao.LocalFormacao);
            }
        }
        protected void Grid()
        {
            Formacao formacao = Session["FormacaoDetails"] as Formacao;
            gvNonConsumeDetails.DataSource = formacao.ListaNConsumiveis;
            gvNonConsumeDetails.DataBind();
        }
        protected void CalendarAvailability_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
        {
            updFormDetails.Update();
        }
        protected void CalendarAvailability_DayRender(object sender, DayRenderEventArgs e)
        {
            e.Day.IsSelectable = false;
            if (e.Day.IsWeekend)
            {
                e.Cell.BackColor = Color.LightGray;
                e.Cell.ForeColor = Color.Black;
            }
            try
            {
                Formacao formacao = Session["FormacaoDetails"] as Formacao;
                List<DiaFormacao> diaFormacaos = formacao.ListaDiaFormacao;
                if (diaFormacaos.Count != 0)
                {
                    foreach (DiaFormacao diaFormacao in diaFormacaos)
                    {
                        if (diaFormacao.Data == e.Day.Date)
                        {
                            e.Cell.ForeColor = Color.Red;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Erro
            }
        }
    }
}