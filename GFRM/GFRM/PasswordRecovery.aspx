﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PasswordRecovery.aspx.cs" Inherits="GFRM.PasswordRecovery" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="Recursos/estilo.css" />
    <link rel="stylesheet" type="text/css" href="Recursos/estiloLogin.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
    <title>GFM</title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <div id="formContent">
                    <div class="margin">
                        <asp:Image ID="Image1" CssClass="animated" runat="server" ImageUrl="~/Recursos/LogoTry.png" alt="GRFM Recovery" Style="padding: 2%" />
                    </div>
                <div>
                    <h5>Recuperação de Password</h5>
                    <p>
                        <img src="Recursos/24x24/user.png" alt="User" /><asp:TextBox ID="txtEmail" runat="server" CssClass="" placeholder="E-mail"></asp:TextBox>
                    </p>
                    <p>
                        <asp:Button ID="btnRecover" runat="server" Text="Confirmar" OnClick="btnRecover_Click" CssClass="btn btn-primary" />
                    </p>
                    <p>
                        <asp:Label ID="lblMsg" runat="server" Font-Size="Small" ForeColor="red" Text=""></asp:Label>
                    </p>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
