﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dashboard.Master" AutoEventWireup="true" CodeBehind="UserMNG.aspx.cs" Inherits="GFRM.UserMNG" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="scpManager" runat="server"></asp:ScriptManager>


            <div class="card">
                <div class="card-body">
                    <asp:Label ID="lblCardHead" runat="server" Text="Utilizadores"></asp:Label>
                    <asp:Button ID="btnAdicionar" runat="server" CssClass="btn btn-primary btnadd" ToolTip="Adicionar Novo/a" OnClick="btnAdicionar_Click" />
                </div>
            </div>
    <div class="card table-responsive">
        <div class="card-body">
        <asp:UpdatePanel ID="updPanelUser" ChildrenAsTriggers="false" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <p style="float: right">
                    <asp:Button ID="search" runat="server" Text="Pesquisar" CssClass="btn btn-primary" OnClick="search_Click" Style="float: right" />
                    <asp:TextBox ID="searchBox" runat="server" CssClass="form-control" Style="width: 300px; float: right; margin-right: 10px; height: 40px;"></asp:TextBox>
                    <asp:DropDownList ID="ddlCat" AutoPostBack="true" OnSelectedIndexChanged="ddlCat_SelectedIndexChanged" runat="server" CssClass="form-control"></asp:DropDownList>
                    <asp:Button ID="reset" runat="server" OnClick="resetSearchButton_Click"/>

                    <div style="float:left">
                        <asp:Button ID="btnEdit" runat="server" CssClass="btn btn-primary btnedit" Visible="false" ToolTip="Editar" OnClick="btnEdit_Click" />
                        <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger btndelete" Visible="false" ToolTip="Apagar" OnClick="btnDelete_Click" 
                            OnClientClick="if ( ! deleteConfirm()) return false;"/>
                    </div>
                </p>
                <p>
                    <asp:GridView ID="GridView1" CssClass="table table-bordered" runat="server" AutoGenerateColumns="false" Style="max-width: 100%;" AllowPaging="True" HeaderStyle-CssClass="text-center" PageSize="15"
                    DataKeyNames="Username" OnPageIndexChanging="GridView1_PageIndexChanging" ShowHeaderWhenEmpty="false" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" OnRowDataBound="OnRowDataBound">
                        <Columns>
                            <asp:BoundField DataField="Username" HeaderText="User" />
                            <asp:BoundField DataField="Nome" HeaderText="Nome" />
                            <asp:BoundField DataField="Email" HeaderText="Email" />
                            <asp:BoundField DataField="CategoriasString" HeaderText="Categorias" />
                            <asp:BoundField DataField="Permissao" HeaderText="Permissao" />
                        </Columns>
                         <EmptyDataTemplate><asp:Label ID="lblEmpty" runat="server" Text="Não existem dados para mostrar"></asp:Label></EmptyDataTemplate>
                    </asp:GridView>
                </p>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="reset" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="GridView1" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="ddlCat" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="search" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</div>
   
    <script type="text/javascript">
        $(document).ready(function () {
            SetKeyPressEvent('<%=searchBox.ClientID %>', '<%=search.ClientID %>',13, 'keypress', false);
        });

        function SetKeyPressEvent(origem, target, key, tipo, setFocus) {
            if (setFocus) {
                let valorOriginal = $('#' + origem).val();
                $('#' + origem).focus().val("").val(valorOriginal);
            }
            $('#' + origem).bind(tipo, function (e) {
                if (e.keyCode === key) { // 13 is enter key
                    $("#" + target).click();
                    e.preventDefault();
                }

            });
        }

        function deleteConfirm() {
            return confirm("Pretende mesmo eliminar este utilizador?");
        }
    </script>
</asp:Content>
