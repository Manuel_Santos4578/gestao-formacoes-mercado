﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary;
using Class_Library;
using System.Data;
using System.Configuration;
using System.Drawing;
using AjaxControlToolkit;

namespace GFRM
{
    public partial class FormacaoAdd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilizador user = Session["User"] as Utilizador;
            if (user.Permissao != "Admin" && user.Permissao != "Coordenador")
            {
                Response.Redirect("ErrorPage.aspx");
            }
            CalendarAvailability.VisibleMonthChanged += new MonthChangedEventHandler(CalendarAvailability_VisibleMonthChanged);
            if (!IsPostBack)
            {
                PreencherCategorias();
                PreencherClientes();
                PreencherFormadores();
                FillLbDatas();
                if (Session["Formacao"] != null)
                {
                    Formacao formacao = Session["Formacao"] as Formacao;
                    txtNome.Text = formacao.Nome;
                    ddlCategoria.SelectedIndex = ddlCategoria.Items.IndexOf(ddlCategoria.Items.FindByValue(formacao.Categoria));
                    ddlCliente.SelectedIndex = ddlCliente.Items.IndexOf(ddlCliente.Items.FindByValue(formacao.Cliente.Entidade));
                    ddlFormador.SelectedIndex = ddlFormador.Items.IndexOf(ddlFormador.Items.FindByValue(formacao.Formador.Username));
                    TextBoxValue.Text = formacao.ValorPagoFormador.ToString();
                    if (formacao.LocalFormacao != "Atec Porto")
                    {
                        rbLocalFormacao.Visible = true;
                        rbLocalFormacao.SelectedValue = "Cliente";
                    }
                    else
                    {
                        rbLocalFormacao.Visible = true;
                        rbLocalFormacao.SelectedValue = "Atec";
                    }

                    List<DiaFormacao> diaComeback = formacao.ListaDiaFormacao as List<DiaFormacao>;
                    if (diaComeback != null)
                    {
                        Session["datas"] = diaComeback;
                    }
                    FillLbDatas();
                }
            }
        }
        #region Categorias
        protected void ddlCategoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            PreencherFormadores();
        }
        protected void PreencherCategorias()
        {
            DataTable categoria = new DAL().GetCategorias();
            ddlCategoria.DataSource = categoria;
            ddlCategoria.DataTextField = "Nome";
            ddlCategoria.DataValueField = "Nome";
            ddlCategoria.DataBind();
            ddlCategoria.Items.Insert(0, "Selecione a categoria");
            ddlCategoria.SelectedIndex = 0;
        }
        #endregion
        #region Calendar
        protected void CalendarAvailability_DayRender(object sender, DayRenderEventArgs e)
        {
            if (e.Day.IsWeekend)
            {
                e.Day.IsSelectable = false;
                e.Cell.ForeColor = Color.Black;
                e.Cell.BackColor = Color.LightGray;
            }
            if (e.Day.Date < DateTime.Today)
            {
                e.Cell.BackColor = Color.LightGray;
                e.Cell.ForeColor = Color.Black;
                e.Day.IsSelectable = false;
            }
            try
            {
                List<Disponibilidade> disponibilidades = Session["Disponibilidades"] as List<Disponibilidade>;
                List<DiaFormacao> dateTimes = Session["Datas"] as List<DiaFormacao>;
                if (disponibilidades.Count != 0)
                {
                    foreach (Disponibilidade disponibilidade in disponibilidades)
                    {
                        if (disponibilidade.Data == e.Day.Date && e.Day.Date >= DateTime.Today)
                        {
                            e.Cell.ForeColor = Color.Red;
                            e.Day.IsSelectable = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Erro
            }
        }
        protected void btnCalendar_Click(object sender, EventArgs e)
        {
            if (ddlFormador.SelectedIndex != 0 && CalendarAvailability.SelectedDate != null)
            {
                List<DiaFormacao> diaFormacaos = Session["Datas"] as List<DiaFormacao>;
                if (diaFormacaos != null && diaFormacaos.Count != 0 /*&& ValidarDateTimeList(CalendarAvailability.SelectedDate.Date)*/)
                {
                    bool exists = false;
                    foreach (DiaFormacao dia in diaFormacaos)
                    {
                        if (dia.Data.Date == CalendarAvailability.SelectedDate.Date) exists = true;
                    }
                    if (!exists)
                    {
                        string txtTimeStart = TextBoxStartTime.Text;
                        string TimeEnd = TextBoxEndTime.Text;
                        TimeSpan time = TimeSpan.Parse(txtTimeStart);
                        DiaFormacao diaFormacao = new DiaFormacao
                        {
                            Data = new DateTime(CalendarAvailability.SelectedDate.Year, CalendarAvailability.SelectedDate.Month, CalendarAvailability.SelectedDate.Day, time.Hours, time.Minutes, time.Seconds),
                            Duracao = int.Parse(TextBoxDuration.Text),
                            HoraFim = TimeSpan.Parse(TimeEnd),
                            HoraInicio = time
                        };
                        diaFormacaos.Add(diaFormacao);
                        DiaFormacao.OrderByDateTime(diaFormacaos);
                        Session["Datas"] = diaFormacaos;
                        FillLbDatas();
                    }
                }
                else
                {
                    string txtTime = TextBoxStartTime.Text;
                    TimeSpan time = TimeSpan.Parse(txtTime);
                    DiaFormacao diaFormacao = new DiaFormacao
                    {
                        Data = new DateTime(CalendarAvailability.SelectedDate.Year, CalendarAvailability.SelectedDate.Month, CalendarAvailability.SelectedDate.Day, time.Hours, time.Minutes, time.Seconds),
                        Duracao = int.Parse(TextBoxDuration.Text),
                    };
                    diaFormacao.HoraInicio = time;
                    diaFormacaos = new List<DiaFormacao>();
                    diaFormacaos.Add(diaFormacao);
                    DiaFormacao.OrderByDateTime(diaFormacaos);
                    Session["Datas"] = diaFormacaos;
                    FillLbDatas();
                }
            }
        }
        protected void CalendarAvailability_SelectionChanged(object sender, EventArgs e)
        {

        }
        protected void CalendarAvailability_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
        {
            updPanel.Update();
        }
        #endregion
        #region Formadores
        protected void ddlFormador_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFormador.SelectedIndex == 0)
            {
                lbDatas.Items.Clear();
                Session["Datas"] = null;
            }
            else
            {
                lbDatas.Items.Clear();
                Session["Disponibilidades"] = new DAL().SelectIndisponibilidadeUtilizador(new Utilizador() { Username = ddlFormador.SelectedValue as string });
                Session["Datas"] =
                    new List<DiaFormacao>();
            }

        }
        protected void PreencherFormadores()
        {
            List<Utilizador> formadores = new List<Utilizador>();
            if (ddlCategoria.SelectedIndex == 0)
            {
                formadores = new DAL().SelectUtilizadores();
            }
            else
            {
                formadores = new DAL().SelectUtilizadores("%", "%", ddlCategoria.SelectedValue);
                updPanel.Update();
            }
            ddlFormador.DataSource = formadores;
            ddlFormador.DataTextField = "Nome";
            ddlFormador.DataValueField = "Username";
            ddlFormador.DataBind();
            ddlFormador.Items.Insert(0, "Selecione o formador");
            ddlFormador.SelectedIndex = 0;
        }
        #endregion
        protected void PreencherClientes()
        {
            DataTable cliente = new DAL().GetClientes();
            ddlCliente.DataSource = cliente;
            ddlCliente.DataTextField = "Nome";
            ddlCliente.DataValueField = "Entidade";
            ddlCliente.DataBind();
            ddlCliente.Items.Insert(0, "Selecione o cliente");
            ddlCliente.SelectedIndex = 0;
        }
        protected void btnCriar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtNome.Text) && ddlFormador.SelectedIndex != 0 && ddlFormador.SelectedIndex != 0 && ddlCliente.SelectedIndex != 0
                && !string.IsNullOrWhiteSpace(TextBoxValue.Text) && !string.IsNullOrWhiteSpace(TextBoxDuration.Text) && !string.IsNullOrWhiteSpace(TextBoxEndTime.Text)
                && !string.IsNullOrWhiteSpace(TextBoxStartTime.Text))
            {
                List<DiaFormacao> diaFormacaos = Session["Datas"] as List<DiaFormacao>;
                if (diaFormacaos == null) diaFormacaos = new List<DiaFormacao>();
                Utilizador utilizador = (Utilizador)Session["User"];
                try
                {
                    Formacao formacao = new Formacao()
                    {
                        Nome = txtNome.Text,
                        Categoria = ddlCategoria.SelectedValue as string,
                        Formador = new Utilizador { Username = ddlFormador.SelectedValue as string },
                        UltimoModificador = new Utilizador { Username = utilizador.Username },
                        DataInicioFormacao = (diaFormacaos == null) ? DateTime.Now : diaFormacaos[0].Data,
                        ValorPagoFormador = double.Parse(TextBoxValue.Text),

                        ListaDiaFormacao = diaFormacaos
                    };
                    formacao.Cliente = new DAL().SelectCliente(ddlCliente.SelectedValue);
                    formacao.LocalFormacao = (rbLocalFormacao.SelectedValue == "Cliente") ? formacao.Cliente.Morada : Formacao.Atec;
                    Session["Formacao"] = formacao;
                    Response.Redirect("FormacaoAddRecurso.aspx");
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(),
                "alert",
                "alert('Adicione pelo menos um dia de Formação');",
                true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(),
                "alert",
                "alert('Existem campos por preencher!');",
                true);
            }

        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Session["Formacao"] = null;
            Response.Redirect("Formacoes.aspx");
        }
        protected void lbDatas_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnRemove.Visible = true;
        }
        protected void ddlCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCliente.SelectedIndex == 0)
            {
                lblLocalFormacao.Visible = false;
                rbLocalFormacao.Visible = false;
            }
            else
            {
                lblLocalFormacao.Visible = true;
                rbLocalFormacao.Visible = true;
            }
        }
        protected void btnRemove_Click(object sender, EventArgs e)
        {
            List<DiaFormacao> diasFormacao = Session["Datas"] as List<DiaFormacao>;
            List<DiaFormacao> diasFormacao2 = new List<DiaFormacao>();
            try
            {
                foreach (DiaFormacao diaFormacao in diasFormacao)
                {
                    if (lbDatas.SelectedItem.Text != diaFormacao.ValorString)
                    {
                        diasFormacao2.Add(diaFormacao);
                    }
                }
                Session["Datas"] = diasFormacao2;
            }
            catch (Exception ex) { }
            finally { FillLbDatas(); updPanel.Update(); }
        }
        protected void FillLbDatas()
        {
            List<DiaFormacao> diasFormacao = Session["Datas"] as List<DiaFormacao>;
            if (diasFormacao == null)
            {
                diasFormacao = new List<DiaFormacao>();
            }
            lbDatas.DataSource = diasFormacao;
            lbDatas.DataValueField = "Data";
            lbDatas.DataTextField = "ValorString";
            lbDatas.DataBind();
        }
    }
}