﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Welcome.aspx.cs" Inherits="GFRM.Welcome" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="Recursos/estilo.css"/>

    <title>GFM</title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="content">
            <asp:Image ID="Image1" CssClass="animated fadeOut welcome-img" runat="server" ImageUrl="~/Recursos/1GFM_ATEC_logo.png"/><br/>
            <asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick" Interval="2000"></asp:Timer>
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        </div>
    </form>
</body>
</html>
