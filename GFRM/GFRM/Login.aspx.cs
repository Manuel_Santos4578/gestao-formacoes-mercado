﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Class_Library;
using ClassLibrary;
using System.Net.Mail;

namespace GRFM
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txtU.Focus();
            if (IsPostBack)
            {
                Image1.CssClass = "";
            }
        }

        protected void Login_Click(object sender, EventArgs e)
        {
            Utilizador utilizador = new Utilizador(txtP.Text);
            utilizador.Username = txtU.Text;
            utilizador.Email = txtU.Text;
            DAL dal = new DAL();

            if (BLL.ValidarLogin(utilizador))
            {
                Utilizador user = dal.SelectUtilizador(utilizador.Username);
                new DAL().InsertLoginLogAsync(user, Auxiliar.GetLocalIPAddress());
                Session["User"] = user;
                Response.Redirect("home.aspx");
            }
            else
            {
                if (string.IsNullOrEmpty(txtP.Text) || string.IsNullOrEmpty(txtU.Text))
                {
                    lblCred.Text = "Por favor preencher os campos em falta!";
                }
                else
                {
                    lblCred.Text = "Credenciais inválidas";
                    txtU.Text = "";
                    txtP.Text = "";
                }
            }
        }
        protected void ImageButtonForgot_Click(object sender, EventArgs e)
        {
            Response.Redirect("PasswordRecovery.aspx");
        }
    }
}