﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dashboard.Master" AutoEventWireup="true" CodeBehind="Statistics.aspx.cs" Inherits="GFRM.Statistics" EnableEventValidation="false" EnableViewState="true" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="scpStatistics" runat="server"></asp:ScriptManager>
    <style>
        body {
            background: #eee;
        }
        span {
            font-size: 15px;
        }
        a {
            text-decoration: none;
            color: #0062cc;
        }
        .box {
            padding: 60px 0px;
        }
        .box-part {
            background: #FFF;
            border-radius: 0;
            padding: 60px 10px;
            margin: 30px 0px;
        }
        .text {
            margin: 20px 0px;
        }
        .fa {
            color: #4183D7;
        }
        .chart {
            width: 100%;
        }
    </style>
    <div class="box">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                    <div class="box-part text-center">
                        <div>
                            <div id="chartContainer" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                    <div class="box-part text-center">
                        <div id="chartContainer2" style="height: 370px; width: 100%;"></div>
                    </div>
                </div>
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                    <div class="box-part text-center">
                        <div id="chartContainer3" style="height:370px;width:100%;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Label ID="lblHidden" Visible="false" runat="server" Text=""></asp:Label>
    <asp:Label ID="lblHidden2" Visible="false" runat="server" Text=""></asp:Label>
    <asp:Label ID="lblHidden3" Visible="false" runat="server" Text=""></asp:Label>
    <script>
        const monthNames = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
            "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
        ];
        const d = new Date();
        window.onload = function () {
            var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: true,
                exportEnabled: true,
                axisX: { interval: 1, labelAngle: -70 },
                theme: "light1", // "light1", "light2", "dark1", "dark2"
                title: {
                    text: "Consumíveis gastos | " + monthNames[d.getMonth()]
                },
                data: [{
                    type: "column", //change type to bar, line, area, pie, etc
                    indexLabel: "{y}", //Shows y value on all Data Points
                    indexLabelFontColor: "#5A5757",
                    indexLabelPlacement: "outside",
                    dataPoints: [
                        <%=Grafico()%>
                    ]
                }]
            });
            chart.render();
            var chart2 = new CanvasJS.Chart("chartContainer2", {
                animationEnabled: true,
                exportEnabled: true,
                theme: "light1", // "light1", "light2", "dark1", "dark2"
                title: {
                    text: "Entrada consumiveis | " + monthNames[d.getMonth()]
                },
                data: [{
                    type: "column", //change type to bar, line, area, pie, etc
                    //indexLabel: "{y}", //Shows y value on all Data Points
                    indexLabelFontColor: "#5A5757",
                    indexLabelPlacement: "outside",
                    dataPoints: [
                        <%=Grafico2()%>
                    ]
                }]
            });
            chart2.render();
            var chart3 = new CanvasJS.Chart("chartContainer3", {
                animationEnabled: true,
                exportEnabled: true,
                theme: "light1", // "light1", "light2", "dark1", "dark2"
                title: {
                    text: "Logins "
                },
                data: [{
                    type: "column", //change type to bar, line, area, pie, etc
                    //indexLabel: "{y}", //Shows y value on all Data Points
                    indexLabelFontColor: "#5A5757",
                    indexLabelPlacement: "outside",
                    dataPoints: [
                        <%=Grafico3()%>
                                ]
                            }]
                        });
            chart3.render();
        }
    </script>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</asp:Content>
