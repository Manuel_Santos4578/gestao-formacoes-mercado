﻿using Class_Library;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GFRM
{
    public partial class FormacaoEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilizador user = Session["User"] as Utilizador;
            if (user.Permissao != "Admin" && user.Permissao != "Coordenador")
            {
                Response.Redirect("ErrorPage.aspx");
            }
            if (!IsPostBack)
            {
                PreencherCategorias();
                PreencherClientes();
                PreencherFormadores();
                FillEverything();
            }
        }

        #region Formadores
        protected void PreencherFormadores()
        {
            List<Utilizador> formadores;
            if (ddlCategoria.SelectedIndex == 0)
            {
                formadores = new DAL().SelectUtilizadores();
            }
            else
            {
                formadores = new DAL().SelectUtilizadores("%", "%", ddlCategoria.SelectedValue);
                updPanel.Update();
            }
            ddlFormador.DataSource = formadores;
            ddlFormador.DataTextField = "Nome";
            ddlFormador.DataValueField = "Username";
            ddlFormador.DataBind();
            ddlFormador.Items.Insert(0, "Selecione o formador");
            ddlFormador.SelectedIndex = 0;
        }
        protected void ddlFormador_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlFormador.SelectedIndex == 0)
            //{
            //    lbDatas.Items.Clear();
            //    Session["Datas"] = null;
            //}
            //else
            //{
            //    lbDatas.Items.Clear();
            //    Session["Disponibilidades"] = new DAL().SelectIndisponibilidadeUtilizador(new Utilizador() { Username = ddlFormador.SelectedValue as string });
            //    Session["Datas"] =
            //        new List<DiaFormacao>();
            //}
        }
        #endregion
        #region Clientes
        protected void PreencherClientes()
        {
            DataTable cliente = new DAL().GetClientes();
            ddlCliente.DataSource = cliente;
            ddlCliente.DataTextField = "Nome";
            ddlCliente.DataValueField = "Entidade";
            ddlCliente.DataBind();
            ddlCliente.Items.Insert(0, "Selecione o cliente");
            ddlCliente.SelectedIndex = 0;
        }
        protected void ddlCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCliente.SelectedIndex == 0)
            {
                lblLocalFormacao.Visible = false;
                rbLocalFormacao.Visible = false;
            }
            else
            {
                lblLocalFormacao.Visible = true;
                rbLocalFormacao.Visible = true;
            }
        }
        #endregion
        #region Categorias
        protected void ddlCategoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            PreencherFormadores();
        }
        protected void PreencherCategorias()
        {
            DataTable categoria = new DAL().GetCategorias();
            ddlCategoria.DataSource = categoria;
            ddlCategoria.DataTextField = "Nome";
            ddlCategoria.DataValueField = "Nome";
            ddlCategoria.DataBind();
            ddlCategoria.Items.Insert(0, "Selecione a categoria");
            ddlCategoria.SelectedIndex = 0;
        }
        #endregion
        #region  Campos
        protected void FillEverything()
        {
            Formacao formacao = Session["FormacaoEdit"] as Formacao;
            Session["DatasEdit"] = formacao.ListaDiaFormacao;
            FillLbDatas(formacao.ListaDiaFormacao);
            FillFields(formacao);
            FillLocal(formacao);
        }
        protected void FillLbDatas(List<DiaFormacao> diaFormacaos)
        {
            diaFormacaos = diaFormacaos ?? new List<DiaFormacao>();
            lbDatas.DataSource = diaFormacaos;
            lbDatas.DataValueField = "Data";
            lbDatas.DataTextField = "ValorString";
            lbDatas.DataBind();
        }
        protected void FillFields(Formacao formacao)
        {
            txtNome.Text = formacao.Nome;
            ddlCategoria.SelectedIndex = ddlCategoria.Items.IndexOf(ddlCategoria.Items.FindByValue(formacao.Categoria));
            ddlFormador.SelectedIndex = ddlFormador.Items.IndexOf(ddlFormador.Items.FindByValue(formacao.Formador.Username));
            ddlCliente.SelectedIndex = ddlCliente.Items.IndexOf(ddlCliente.Items.FindByValue(formacao.Cliente.Entidade));

            TextBoxValue.Text = formacao.ValorPagoFormador.ToString();
        }
        protected void FillLocal(Formacao formacao)
        {
            rbLocalFormacao.SelectedIndex = (formacao.LocalFormacao == "Atec Porto") ?
                rbLocalFormacao.Items.IndexOf(rbLocalFormacao.Items.FindByText("Atec")) :
                rbLocalFormacao.Items.IndexOf(rbLocalFormacao.Items.FindByText("Cliente"));
        }
        #endregion
        protected void CalendarAvailability_DayRender(object sender, DayRenderEventArgs e)
        {
            if (e.Day.IsWeekend)
            {
                e.Day.IsSelectable = false;
                e.Cell.ForeColor = Color.Black;
                e.Cell.BackColor = Color.LightGray;
            }
            if (e.Day.Date < DateTime.Today)
            {
                e.Cell.BackColor = Color.LightGray;
                e.Cell.ForeColor = Color.Black;
                e.Day.IsSelectable = false;
            }
            try
            {
                List<Disponibilidade> disponibilidades = Session["Disponibilidades"] as List<Disponibilidade>;
                List<DiaFormacao> dateTimes = Session["Datas"] as List<DiaFormacao>;
                if (disponibilidades.Count != 0)
                {
                    foreach (Disponibilidade disponibilidade in disponibilidades)
                    {
                        if (disponibilidade.Data == e.Day.Date && e.Day.Date >= DateTime.Today)
                        {
                            e.Cell.ForeColor = Color.Red;
                            e.Day.IsSelectable = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Erro
            }
        }
        protected void CalendarAvailability_SelectionChanged(object sender, EventArgs e)
        {

        }
        protected void CalendarAvailability_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
        {
            updPanel.Update();
        }
        protected void lbDatas_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnRemove.Visible = true;
        }
        protected void btnRemove_Click(object sender, EventArgs e)
        {
            List<DiaFormacao> diasFormacao = Session["DatasEdit"] as List<DiaFormacao>;
            List<DiaFormacao> diasFormacao2 = new List<DiaFormacao>();
            try
            {
                foreach (DiaFormacao diaFormacao in diasFormacao)
                {
                    if (lbDatas.SelectedItem.Text != diaFormacao.ValorString)
                    {
                        diasFormacao2.Add(diaFormacao);
                    }
                }
                Session["DatasEdit"] = diasFormacao2;
                btnRemove.Visible = false;
            }
            catch (Exception ex) { }
            finally
            {
                FillLbDatas(diasFormacao2);
                updPanel.Update();
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Session["FormacaoEdit"] = null;
            Session["DatasEdit"] = null;
            Response.Redirect("Formacoes.aspx");
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Formacao formacao = Session["FormacaoEdit"] as Formacao;
            formacao.Nome = (!string.IsNullOrWhiteSpace(txtNome.Text)) ? txtNome.Text : formacao.Nome;
            formacao.Categoria = (ddlCategoria.SelectedIndex != 0) ? ddlCategoria.SelectedValue : formacao.Categoria;
            formacao.Formador = ddlFormador.SelectedIndex != 0 ? new Utilizador { Username = ddlFormador.SelectedItem.Value, Nome = ddlFormador.SelectedItem.Text } : formacao.Formador;
            formacao.Cliente = ddlCliente.SelectedIndex != 0 ? new Cliente { Entidade = ddlCliente.SelectedItem.Value, Nome = ddlCliente.SelectedItem.Text } : formacao.Cliente;
            formacao.LocalFormacao = (rbLocalFormacao.SelectedValue == "Cliente") ? formacao.Cliente.Morada : Formacao.Atec;
            List<DiaFormacao> diaFormacaos = Session["DatasEdit"] as List<DiaFormacao>;
            formacao.ListaDiaFormacao = diaFormacaos.Count != 0 ? diaFormacaos : formacao.ListaDiaFormacao;
            Session["FormacaoEdit"] = formacao;
            bool updated = new DAL().UpdateFormacao(formacao, Session["User"] as Utilizador);
            if (updated)
            {
                Response.Redirect("FormacaoEditRecursos.aspx");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert",
                "alert('Ocorreu um erro!');", true);
            }
        }

        protected void FormacaoEditMake()
        {
            DAL dal = new DAL();
            Utilizador user = Session["User"] as Utilizador ?? dal.SelectUtilizador("Admin");

            Formacao formacao = Session["FormacaoEdit"] as Formacao ??
                new Formacao
                {
                    Id = 500,
                    Nome = "Teste",
                    Categoria = "Mecanica",
                    Cliente = dal.SelectCliente("D000"),
                    Formador = Session["User"] as Utilizador ?? user,
                    ListaConsumivel = new List<RecursoConsumivel>(),
                    ListaNConsumiveis = new List<RecursoNaoConsumivel>(),
                    ValorPagoFormador = 50.00,
                    ListaDiaFormacao =
                        new DiaFormacao[] {
                            new DiaFormacao {
                                Data = new DateTime(2019, 12, 1),
                                Duracao = 8,
                                HoraInicio = new TimeSpan(8, 10, 0),
                                HoraFim = new TimeSpan(17, 10, 0),
                            },
                            new DiaFormacao
                            {
                                Data = new DateTime(2020, 12, 1),
                                Duracao = 8,
                                HoraInicio = new TimeSpan(8, 10, 0),
                                HoraFim = new TimeSpan(17, 10, 0),
                            }
                        }.ToList(),
                    LocalFormacao = Formacao.Atec,
                    UltimoModificador = Session["User"] as Utilizador ?? user
                };
            formacao.DataInicioFormacao = formacao.ListaDiaFormacao[0].Data;
            Session["FormacaoEdit"] = formacao;
        }
    }
}