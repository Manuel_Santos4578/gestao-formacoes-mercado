﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="GRFM.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="Recursos/estilo.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="Recursos/estiloLogin.css" />
    <title>GFM</title>
</head>
<body>
    <form id="formLogin" runat="server" class="animated fadeIn">
        <asp:ScriptManager ID="scrpLogin" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="updPanelLogin" ChildrenAsTriggers="false" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <div class="container">
                    <div id="formContent" class="margintopbot">
                        <div class="margin">
                            <asp:Image ID="Image1" CssClass="animated" runat="server" ImageUrl="~/Recursos/LogoTry.png" alt="GRFM LOGIN" Style="padding: 2%" />
                        </div>
                        <div>
                            <p>
                                <img src="Recursos/24x24/user.png" alt="User" /><asp:TextBox ID="txtU" runat="server" Text="Admin" placeholder="username/e-mail"></asp:TextBox>
                            </p>
                            <p>
                                <img src="Recursos/24x24/password.png" alt="Pwd" /><asp:TextBox ID="txtP" runat="server" Text="Atec1234" TextMode="Password" placeholder="password" Style="background-color: #f6f6f6; border: none; color: #0d0d0d; padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 5px; width: 85%; border: 2px solid #f6f6f6; border-radius: 5px 5px 5px 5px;"></asp:TextBox>
                            </p>
                            <p>
                                <asp:Button ID="Button" runat="server" Text="Login" OnClick="Login_Click" CssClass="btn btn-primary" />
                            </p>
                            <p>
                                <asp:Label ID="lblCred" runat="server" Font-Size="Small" ForeColor="red" Text=""></asp:Label>
                            </p>
                        </div>
                        <div id="formFooter">
                            <asp:LinkButton runat="server" ID="ImageButtonForgot" OnClick="ImageButtonForgot_Click">
                                <asp:Image AlternateText="ForgotPassword" runat="server" ImageAlign="Middle" 
                                    ToolTip="Recuperação de Password" src="Recursos/24x24/forgot.png" />
                            </asp:LinkButton>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ImageButtonForgot" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="Button" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ImageButtonForgot" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </form>
</body>
<script src="Recursos/vendor/jquery/jquery.min.js"></script>
<script src="Recursos/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript">
    function redirect(url) {
        setTimeout(function () { window.location.href = url; }, 2000);
    }
</script>
</html>
