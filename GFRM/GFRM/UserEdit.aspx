﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dashboard.Master" AutoEventWireup="true" CodeBehind="UserEdit.aspx.cs" Inherits="GFRM.UserEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="Recursos/styleP.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScrMNG" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="updUserEdit" ChildrenAsTriggers="false" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <div class="container-fluid" style="background-color: #f8f9fa; height: 100%;">
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10">
                        <div class="card">
                            <div class="card-body">
                                <asp:Label ID="lblCardHead" runat="server" Text="Editar Utilizador"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="col-1"></div>
                </div>
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10">
                        <div class="card">
                            <div class="card-body">
                                <div style="color: black; font-size: 20px">
                                    <div class="col-md-12 row">
                                        <p class="col-md-4">Username:</p>
                                        <p class="col-md-8">
                                            <asp:TextBox ID="TextBoxUserName" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </p>
                                    </div>
                                    <div class="col-md-12 row">
                                        <p class="col-md-4">Nome:</p>
                                        <p class="col-md-8">
                                            <asp:TextBox ID="TextBoxName" runat="server" CssClass="form-control"></asp:TextBox>
                                        </p>
                                    </div>
                                    <div class="col-md-12 row">
                                        <p class="col-md-4">Email:</p>
                                        <p class="col-md-8">
                                            <asp:TextBox ID="TextBoxEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                        </p>
                                    </div>
                                    <div class="col-md-12 row">
                                        <div class="col-md-4">
                                            <p>Categoria:</p>
                                        </div>
                                        <div class="col-md-8">
                                            <asp:CheckBoxList ID="chkbCategory" runat="server" RepeatColumns="6" RepeatLayout="Flow" CellPadding="10">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                    <div class="col-md-12 row">
                                        <p class="col-md-4">Permissão:</p>
                                        <p class="col-md-8">
                                            <asp:DropDownList ID="DropDownPermition" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </p>
                                    </div>
                                    <div class="col-md-12 row">
                                        <div class="col-md-12" style="text-align:center">
                                            <label id="LblCalDisponibilidade" runat="server">Calendário de Disponibilidades</label>
                                        </div>
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6">
                                            <asp:Calendar ID="CalendarAvailability" SelectedDayStyle-BackColor="LightGray" OnDayRender="CalendarAvailability_DayRender" OnSelectionChanged="CalendarAvailability_SelectionChanged" runat="server"
                                                WeekendDayStyle-BackColor="OrangeRed" WeekendDayStyle-ForeColor="Black" FirstDayOfWeek="Monday" CssClass="calendary"
                                                OnVisibleMonthChanged="CalendarAvailability_VisibleMonthChanged"></asp:Calendar>
                                            <asp:Button ID="buttonAvailability" class="btn btn-primary" runat="server" Text="Mudar" OnClick="buttonAvailability_Click" Style="margin-top: 16px;float:right"/>
                                        </div>
                                        <div class="col-md-3"></div>
                                    </div>
                                    <div class="col-md-12 row">
                                        <div class="col-md-4">
                                            <asp:Label ID="LabelError" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 row" Style="margin-top: 16px;">
                                        <div class=" col-md-10"></div>
                                            <div class=" col-md-2 float-right">
                                                <asp:Button ID="ButtonEdit" class="btn btn-primary btncheck" runat="server" ToolTip="Aceitar Alteração" OnClick="ButtonEdit_Click"/>
                                                <asp:Button ID="ButtonCancel" class="btn btn-danger btncancel" runat="server" ToolTip="Cancelar" OnClick="ButtonCancel_Click" />
                                            </div>                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="CalendarAvailability" EventName="SelectionChanged" />
            <asp:AsyncPostBackTrigger ControlID="buttonAvailability" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    
    <script type="text/javascript">
        function editConfirm() {
            return confirm("Pretende mesmo editar este utilizador?");
        }
    </script>
</asp:Content>
