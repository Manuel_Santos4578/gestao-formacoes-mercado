﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dashboard.Master" AutoEventWireup="true" CodeBehind="FormacaoDetails.aspx.cs" Inherits="GFRM.FormacaoDetails" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="scpFormDetails" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="updFormDetails" ChildrenAsTriggers="false" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <div class="card">
                <div class="card-body">
                    <asp:Label ID="lblCardHead" runat="server" Text="Detalhes"></asp:Label>
                </div>
            </div>
            <div id="cardCat" class="card table-responsive">
                <div class="card-body col-md-12 row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Estado</label>
                            <asp:TextBox ID="txtEstado" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Nome</label>
                            <asp:TextBox ID="txtNome" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                        </div>
                        <!-- form-group// -->
                        <div class="form-group">
                            <label>Cliente</label>
                            <asp:TextBox ID="txtCliente" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                        </div>
                        <!-- form-group// -->
                        <div class="form-group">
                            <label>Categoria</label>
                            <asp:TextBox ID="txtCategoria" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                        </div>
                        <!-- form-group// -->
                        <div class="form-group">
                            <label>Formador</label>
                            <asp:TextBox ID="txtFormador" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                        </div>
                        <!-- form-group// -->
                        <div class="form-group">
                        </div>
                        <!-- form-group// -->
                        <div class="form-group">
                            <label id="lblLocalFormacao" runat="server">Local Formação</label>
                            <asp:TextBox ID="txtLocal" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-6 row">
                        <div class="col-md-12">
                            <div class="col-md-12" style="text-align: center">
                                <label id="LblDiasFormacao" runat="server">Dias de Formação</label>
                            </div>
                            <asp:Calendar ID="CalendarAvailability" WeekendDayStyle-BackColor="LightGray" OnDayRender="CalendarAvailability_DayRender" WeekendDayStyle-ForeColor="Black" FirstDayOfWeek="Monday" runat="server" CssClass="calendary"></asp:Calendar>
                        </div>
                        <div class="col-md-12" style="text-align: center">
                            <label id="LblRecursosFormacao" runat="server">Recurso de Formação</label>
                        </div>
                        <div class="col-md-12">
                            <asp:GridView ID="gvNonConsumeDetails" CssClass="table table-bordered" runat="server" ShowHeaderWhenEmpty="false" AutoGenerateColumns="false" AllowPaging="True" PageSize="5"
                                HeaderStyle-CssClass="text-center" DataKeyNames="Id">
                                <Columns>
                                    <asp:BoundField DataField="Id" Visible="true" ReadOnly="true" HeaderText="ID" />
                                    <asp:BoundField DataField="Nome" HeaderText="Nome" />
                                    <asp:BoundField DataField="Categoria" HeaderText="Categoria" />
                                </Columns>
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label1" runat="server" Style="text-align: center" Text="Não existem recursos para mostrar"></asp:Label>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
