﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dashboard.Master" AutoEventWireup="true" CodeBehind="UserAdd.aspx.cs" Inherits="GFRM.UserAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="scrpManagUser" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="updPanelUserAdd" ChildrenAsTriggers="false" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10">
                        <div class="card">
                            <div class="card-body">
                                <asp:Label ID="lblCardHead" runat="server" Text="Adicionar Utilizador"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="col-1"></div>
                </div>
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10">
                        <div class="card">
                            <div class="card-body">
                                <div style="color: black; font-size: 20px">
                                    <p style="text-align: center;">
                                        <asp:Label ID="lblLog" runat="server" Text="" Style="text-align: center;" ForeColor="Red" Font-Size="Small"></asp:Label>
                                    </p>
                                    <div class="col-md-12 row">
                                        <p class="col-md-4">Username:</p>
                                        <p class="col-md-8">
                                            <asp:TextBox ID="txtUsername" ReadOnly="true" OnTextChanged="txtUsername_TextChanged" runat="server" CssClass="form-control"></asp:TextBox>
                                        </p>
                                    </div>
                                    <div class="col-md-12 row">
                                        <p class="col-md-4">Nome:</p>
                                        <p class="col-md-8">
                                            <asp:TextBox ID="txtNome" runat="server" onblur="GenerateUsername('ContentPlaceHolder1_txtNome', 'ContentPlaceHolder1_txtUsername')" CssClass="form-control"></asp:TextBox>
                                        </p>
                                    </div>
                                    <div class="col-md-12 row">
                                        <p class="col-md-4">Email:</p>
                                        <p class="col-md-8">
                                            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                        </p>
                                    </div>
                                    <div class="col-md-12 row">
                                        <p class="col-md-4">Categoria:</p>
                                        <p class="col-md-8">
                                            <asp:CheckBoxList ID="chkbCategory" runat="server" RepeatColumns="6" RepeatLayout="Flow" CellPadding="10">
                                            </asp:CheckBoxList>
                                        </p>
                                    </div>
                                    <div class="col-md-12 row">
                                        <p class="col-md-4">Permissão:</p>
                                        <p class="col-md-8">
                                            <asp:DropDownList ID="ddlPerm" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </p>
                                    </div>
                                    <div class="col-md-12 row">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-8">
                                            <div class="float-right">
                                                <asp:Button ID="ButtonAdd" class="btn btn-primary btncheck" runat="server" ToolTip="Adicionar" OnClick="ButtonAdd_Click" />
                                                <asp:Button ID="ButtonCancel" class="btn btn-danger btncancel" runat="server" ToolTip="Cancelar" OnClick="ButtonCancel_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-1"></div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonAdd" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updProgress" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="updPanelUserAdd" DisplayAfter="100">
        <ProgressTemplate>
            <div style="background-color: rgba(0,0,0,0.5); width: 100%; height: 100%; z-index: 10; position: fixed; top: 0; left: 0;">
                <div id="circle">
                    <div class="loader">
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <script type="text/javascript" src="Recursos/script.js"></script>
</asp:Content>
