﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dashboard.Master" AutoEventWireup="true" CodeBehind="ResourceAdd.aspx.cs" Inherits="GFRM.ResourceAdd" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="vendors/jquery-1.9.1.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="vendors/easypiechart/jquery.easy-pie-chart.js"></script>
    <script src="assets/scripts.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="panelRadioButtons" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-1"></div>
                <div class="col-10">
                    <div class="card">
                        <div class="card-body">

                            <asp:Label ID="lblCardHead" runat="server" Text="Adicionar Recurso"></asp:Label>
                            <asp:RadioButtonList ID="rb" class="col-md-8" runat="server" AutoPostBack="true" RepeatDirection="Horizontal">
                                <asp:ListItem class="col-md-6" Value="Consumivel">  Consumível</asp:ListItem>
                                <asp:ListItem class="col-md-6" Value="NaoConsumivel">  Não Consumível</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                </div>
                <div class="col-1"></div>
            </div>
            <div class="row">
                <div class="col-1"></div>
                <div class="col-10">
                    <div class="card">
                        <div class="card-body">
                            <div style="color: black; font-size: 20px">
                                <div class="col-md-12 row">
                                    <p class="col-md-4">Nome:</p>
                                    <p class="col-md-8">
                                        <asp:TextBox ID="txtNome" runat="server" CssClass="form-control"></asp:TextBox>
                                    </p>
                                </div>
                                <div class="col-md-12 row">
                                    <p class="col-md-4">Categoria:</p>
                                    <p class="col-md-8">
                                        <asp:DropDownList ID="ddlCat" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </p>
                                </div>
                                <div class="col-md-12 row">
                                    <p class="col-md-4">
                                        <asp:Label ID="lblQtd" runat="server" Text="Quantidade:"></asp:Label>
                                    </p>
                                    <p class="col-md-8">
                                        <asp:TextBox ID="txtQtd" runat="server" CssClass="form-control" placeholder="Quantidade"></asp:TextBox>
                                    </p>
                                </div>
                                <div class="col-md-12 row">
                                    <p class="col-md-4">
                                        <asp:Label ID="lblMinValue" runat="server" Text="Valor Minimo:"></asp:Label>
                                    </p>
                                    <p class="col-md-8">
                                        <asp:TextBox ID="txtMinVal" runat="server" CssClass="form-control" TextMode="Number" placeholder="Valor Minimo"></asp:TextBox>
                                    </p>
                                </div>
                                <div class="col-md-12 row">
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-8">
                                        <div class="float-right">
                                            <asp:Button ID="ButtonAdd" class="btn btn-primary" runat="server" Text="Adicionar" OnClick="ButtonAdd_Click"/>
                                            <asp:Button ID="ButtonCancel" class="btn btn-danger" runat="server" Text="Cancelar" OnClick="ButtonCancel_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-1"></div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updProgress" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="panelRadioButtons" DisplayAfter="100">
        <ProgressTemplate>
            <div style="background-color:rgba(0,0,0,0.5); width:100%; height:100%; z-index:10; position:fixed; top:0; left:0;">
                <div id="circle">
                    <div class="loader">
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <script type="text/javascript">
        function addConfirm() {
            return confirm("Pretende mesmo adicionar este recurso?");
        }
    </script>
</asp:Content>
