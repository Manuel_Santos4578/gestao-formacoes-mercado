﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary;
using Class_Library;
using System.Data;
using System.Drawing;

namespace GFRM
{
    public partial class UserEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilizador userlogged = Session["User"] as Utilizador;
            if (userlogged.Permissao != "Admin" && userlogged.Permissao != "Coordenador")
            {
                Response.Redirect("ErrorPage.aspx");
            }
            CalendarAvailability.VisibleMonthChanged += new MonthChangedEventHandler(CalendarAvailability_VisibleMonthChanged);
            if (!IsPostBack)
            {
                Utilizador user = Session["UserEdit"] as Utilizador;
                DAL dal = new DAL();
                List<Disponibilidade> disponibilidadesDiaformacao = dal.SelectIndisponibilidadeUtilizadorDiasFormacao(user);
                List<Disponibilidade> disponibilidadesNotDiaformacao = dal.SelectIndisponibilidadeUtilizadorNotDiasFormacao(user);
                Session["DiasFormacaoUser"] = disponibilidadesDiaformacao;
                Session["SickDays"] = disponibilidadesNotDiaformacao;
                GetDisponibilidadesUserEdit();
                FillCat();
                FillPerm();
                FillFields();
                chkbFillEdit();

            }
        }
        protected void GetDisponibilidadesUserEdit()
        {
            Utilizador utilizador = Session["UserEdit"] as Utilizador;
            List<Disponibilidade> disponibilidades = new DAL().SelectIndisponibilidadeUtilizador(utilizador);
            Session["DisponibilidadesUserEdit"] = disponibilidades;
        }
        protected void chkbFillEdit()
        {
            Utilizador user = Session["UserEdit"] as Utilizador;
            foreach (string categoria in user.Categorias)
            {
                foreach (ListItem item in chkbCategory.Items)
                {
                    if (item.Value == categoria)
                        item.Selected = true;
                }
            }
        }
        protected void ButtonEdit_Click(object sender, EventArgs e)
        {
            Utilizador utilizador = Session["UserEdit"] as Utilizador;
            utilizador.Username = string.IsNullOrWhiteSpace(TextBoxUserName.Text) ? utilizador.Username : TextBoxUserName.Text;
            utilizador.Nome = string.IsNullOrWhiteSpace(TextBoxName.Text) ? utilizador.Nome : TextBoxName.Text;
            utilizador.Email = (string.IsNullOrWhiteSpace(TextBoxEmail.Text) && Auxiliar.ValidateEmail(TextBoxEmail.Text)) ? utilizador.Email : TextBoxEmail.Text;
            bool atLeastOneChecked = false;
            List<string> categorias = new List<string>();
            foreach (ListItem li in chkbCategory.Items)
            {
                if (li.Selected == true)
                {
                    atLeastOneChecked = true;
                    categorias.Add(li.Text);
                }
            }
            if (atLeastOneChecked)
                utilizador.Categorias = categorias;
            else
                ScriptManager.RegisterStartupScript(this, this.GetType(),
                "alert",
                "alert('O utilizador deve ter pelo menos uma categoria');",
                true);
            utilizador.Permissao = (DropDownPermition.SelectedIndex == 0) ? utilizador.Permissao : DropDownPermition.SelectedValue;
            bool updated = new DAL().UpdateUtilizador(utilizador);
            if (updated)
            {
                Session["UserEdit"] = null;
                Response.Redirect("UserMNG.aspx");
            }
        }
        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserMNG.aspx");
        }
        protected void FillFields()
        {
            Utilizador user = Session["UserEdit"] as Utilizador;
            if (user != null)
            {
                TextBoxUserName.Text = Server.HtmlDecode(user.Username);
                TextBoxName.Text = Server.HtmlDecode(user.Nome);
                TextBoxEmail.Text = Server.HtmlDecode(user.Email);
                DropDownPermition.SelectedIndex = DropDownPermition.Items.IndexOf(DropDownPermition.Items.FindByValue(user.Permissao));
            }
        }
        private void FillCat()
        {
            DataTable categoria = new DAL().GetCategorias();
            chkbCategory.DataSource = categoria;
            chkbCategory.DataTextField = "Nome";
            chkbCategory.DataValueField = "Nome";
            chkbCategory.DataBind();
        }
        protected void FillPerm()
        {
            DataTable permissoes = new DAL().GetPermissoes();
            DropDownPermition.DataSource = permissoes;
            DropDownPermition.DataTextField = "Nome";
            DropDownPermition.DataValueField = "Nome";
            DropDownPermition.DataBind();
            DropDownPermition.Items.Insert(0, "Selecione a permissao");
        }

        protected void CalendarAvailability_DayRender(object sender, DayRenderEventArgs e)
        {
            if (e.Day.IsWeekend)
            {
                e.Day.IsSelectable = false;
            }
            if (e.Day.Date < DateTime.Today)
            {
                e.Cell.BackColor = Color.LightGray;
                e.Day.IsSelectable = false;
            }
            try
            {
                List<Disponibilidade> disponibilidadesDiaformacao = Session["DiasFormacaoUser"] as List<Disponibilidade>;
                List<Disponibilidade> disponibilidadesNotDiaformacao = Session["SickDays"] as List<Disponibilidade>;
                if (disponibilidadesDiaformacao.Count == 0)
                {
                    if (e.Day.Date < DateTime.Today)
                    {
                        e.Cell.BackColor = Color.LightGray;
                        e.Day.IsSelectable = false;
                    }
                }
                else
                {
                    foreach (Disponibilidade disponibilidade in disponibilidadesDiaformacao)
                    {
                        if (disponibilidade.Data == e.Day.Date)
                        {
                            if (e.Day.Date < DateTime.Today)
                            {
                                e.Cell.BackColor = Color.LightGray;
                                e.Day.IsSelectable = false;
                            }
                            else
                            {
                                e.Cell.ForeColor = Color.Red;
                                e.Day.IsSelectable = false;
                            }
                        }
                        else
                        {
                            if (e.Day.Date < DateTime.Today)
                            {
                                e.Cell.BackColor = Color.LightGray;
                                e.Day.IsSelectable = false;
                            }
                        }
                    }
                }
                if (disponibilidadesNotDiaformacao.Count != 0)
                {
                    foreach (Disponibilidade disponibilidade1 in disponibilidadesNotDiaformacao)
                    {
                        if (disponibilidade1.Data == e.Day.Date)
                        {
                            if (e.Day.Date < DateTime.Today)
                            {
                                e.Cell.BackColor = Color.LightGray;
                                e.Day.IsSelectable = false;
                            }
                            else
                            {
                                e.Cell.ForeColor = Color.Blue;
                                e.Day.IsSelectable = true;
                            }
                        }
                        else
                        {
                            if (e.Day.Date < DateTime.Today)
                            {
                                e.Cell.BackColor = Color.LightGray;
                                e.Day.IsSelectable = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Erro
            }
        }

        protected void CalendarAvailability_SelectionChanged(object sender, EventArgs e)
        {

        }

        protected void buttonAvailability_Click(object sender, EventArgs e)
        {
            Utilizador user = Session["UserEdit"] as Utilizador;
            DateTime date = CalendarAvailability.SelectedDate.Date;
            new DAL().ExecDisponibilidadesPessoais(user, date);
            DAL dal = new DAL();
            List<Disponibilidade> disponibilidadesDiaformacao = dal.SelectIndisponibilidadeUtilizadorDiasFormacao(user);
            List<Disponibilidade> disponibilidadesNotDiaformacao = dal.SelectIndisponibilidadeUtilizadorNotDiasFormacao(user);
            Session["DiasFormacaoUser"] = disponibilidadesDiaformacao;
            Session["SickDays"] = disponibilidadesNotDiaformacao;

        }

        protected void CalendarAvailability_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
        {
            updUserEdit.Update();
        }
    }
}