﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dashboard.Master" AutoEventWireup="true" CodeBehind="UserPage.aspx.cs" Inherits="GFRM.UserPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href='../packages/core/main.css' rel='stylesheet' />
    <link href='../packages/daygrid/main.css' rel='stylesheet' />
    <link href='../packages/timegrid/main.css' rel='stylesheet' />
    <link rel="stylesheet" type="text/css" href="Recursos/estilo.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server" ID="scpUserPage"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanelDisponibilidade" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <asp:Label ID="lblCardHead" runat="server" Text="Página Pessoal"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="col-sm-12 row">
                                    <div class="col-sm-6">
                                        <div class="row" style="color: black; font-size: 20px">
                                            <div class="col-md-12 row grid">
                                                <p class=" col-md-4">Nome:</p>
                                                <p class="col-md-8 ">
                                                    <asp:TextBox ID="TextBoxName" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                </p>
                                            </div>
                                            <div class="col-md-12 row">
                                                <p class="col-md-4">Username:</p>
                                                <p class="col-md-8">
                                                    <asp:TextBox ID="TextBoxUsername" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                </p>
                                            </div>
                                            <div class="col-md-12 row">
                                                <p class="col-md-4">Email:</p>
                                                <p class="col-md-8">
                                                    <asp:TextBox ID="TextBoxEmail" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                </p>
                                            </div>
                                            <div class="col-md-12 row">
                                                <p class="col-md-4">Mudar Email:</p>
                                                <p class="col-md-8">
                                                    <asp:TextBox ID="TextBoxChangeEmail" runat="server" placeholder="exemplo@mail.com" CssClass="form-control"></asp:TextBox>
                                                </p>
                                            </div>
                                            <div class="col-md-12 row" style="margin-bottom: 16px;">
                                                <div class="col-md-4">
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="float-right">
                                                        <asp:Button ID="ButtonChangeEmail" class="btn btn-primary btncheck" runat="server" ToolTip="Prosseguir com alteração de email" OnClick="ButtonChangeEmail_Click" />
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="col-md-12 row">
                                                <p class="col-md-4">Password Atual:</p>
                                                <p class="col-md-8">
                                                    <asp:TextBox runat="server" TextMode="Password" ID="txtPwAtual" CssClass="form-control" />
                                                </p>
                                            </div>
                                            <div class="col-md-12 row">
                                                <p class="col-md-4">Nova Password:</p>
                                                <p class="col-md-8">
                                                    <asp:TextBox runat="server" TextMode="Password" ID="txtPwNova" CssClass="form-control" />
                                                </p>
                                            </div>
                                            <div class="col-md-12 row">
                                                <p class="col-md-4">Confirmar Password:</p>
                                                <p class="col-md-8">
                                                    <asp:TextBox runat="server" ID="txtPwConfirmar" TextMode="Password" CssClass="form-control" />
                                                </p>
                                            </div>
                                            <div class="col-md-12 row" style="height: 15px;">
                                                <p class="col-md-12">
                                                    <asp:Label ID="lblFail" runat="server" Text="" Style="color: red" Font-Size="Small"></asp:Label>
                                                </p>
                                            </div>

                                            <div class="col-md-12 row">
                                                <div class="col-md-4">
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="float-right">
                                                        <asp:Button ID="ButtonAdd" class="btn btn-primary btncheck" runat="server" ToolTip="Prosseguir com alteração de password" OnClick="ButtonAdd_Click" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <section id='calendar' class="col-md-12 row">
                                            <asp:Calendar ID="CalendarAvailability" WeekendDayStyle-BackColor="OrangeRed" WeekendDayStyle-ForeColor="Black" OtherMonthDayStyle-ForeColor="Black" FirstDayOfWeek="Monday" OnDayRender="CalendarAvailability_DayRender" OnVisibleMonthChanged="CalendarAvailability_VisibleMonthChanged" runat="server" CssClass="calendary col-md-12"></asp:Calendar>
                                            <asp:Button ID="btnDisponibilidade" runat="server" Text="Mudar Disponibilidade" style="margin:.3rem 0 .3rem 0" CssClass="btn btn-primary float-right" OnClick="btnDisponibilidade_Click" />
                                            <div class="col-md-12" style="margin: .2rem 0 .2rem 0;">
                                                <div style="width: 10px; height: 10px; background-color: red;"></div>
                                                <asp:Label ID="Label1" runat="server" Text="Dia de Formação"></asp:Label>
                                                <div style="width: 10px; height: 10px; background-color: blue; margin-top: 10px;"></div>
                                                <asp:Label ID="Label2" runat="server" Text="Indisponibilidade do Formador"></asp:Label>
                                                <div style="width: 10px; height: 10px; background-color: black; margin-top: 10px;"></div>
                                                <asp:Label ID="Label3" runat="server" Text="Disponível"></asp:Label>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-1">
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonAdd" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnDisponibilidade" />
            <asp:AsyncPostBackTrigger ControlID="CalendarAvailability" EventName="SelectionChanged" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
