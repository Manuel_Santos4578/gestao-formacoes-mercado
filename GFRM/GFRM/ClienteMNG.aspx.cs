﻿using Class_Library;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GFRM
{
    public partial class ClienteMNG : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilizador user = Session["User"] as Utilizador;
            if (user.Permissao != "Admin" && user.Permissao != "Coordenador")
            {
                Response.Redirect("ErrorPage.aspx");
            }
            btnDelete.Visible = false;
            btnEdit.Visible = false;
            if (!Page.IsPostBack)
            {
                Grid();
            }
        }

        #region Botões Search e Reset
        protected void searchButton_Click(object sender, EventArgs e)
        {
            string searchTerm = searchBox.Text.ToLower();
            //check if the search input is at least 3 chars
            if (searchTerm.Length >= 3)
            {
                //always check if the viewstate exists before using it
                if (ViewState["myViewState"] == null)
                    return;
                //cast the viewstate as a datatable
                DataTable dt = ViewState["myViewState"] as DataTable;
                //make a clone of the datatable
                DataTable dtNew = dt.Clone();
                //search the datatable for the correct fields
                foreach (DataRow row in dt.Rows)
                {
                    //add your own columns to be searched here
                    if (row["Nome"].ToString().ToLower().Contains(searchTerm) || row["Categoria"].ToString().ToLower().Contains(searchTerm))
                    {
                        //when found copy the row to the cloned table
                        dtNew.Rows.Add(row.ItemArray);
                    }
                }
                //rebind the grid
                GridView1.DataSource = dtNew;
                GridView1.DataBind();
            }
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), $"SetKeyPressEvent('{searchBox.ClientID}','{search.ClientID}', 13, 'keypress',true);", true);
        }
        protected void resetSearchButton_Click(object sender, EventArgs e)
        {
            searchBox.Text = "";
            Grid();
            //always check if the viewstate exists before using it
            //if (ViewState["myViewState"] == null)
            //    return;
            ////cast the viewstate as a datatable
            //DataTable dt = ViewState["myViewState"] as DataTable;
            ////rebind the grid
            //GridView1.DataSource = dt;
            //GridView1.DataBind();
            //searchBox.Text = "";
        }
        #endregion
        #region GridView
        protected void Grid()
        {
            DataTable dt = new DAL().GetClientes();
            ViewState["myViewState"] = dt;
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(GridView1, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = "Clique para selecionar.";
            }
        }
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridView1.Rows)
            {
                if (row.RowIndex == GridView1.SelectedIndex)
                {
                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                }
                else
                {
                    row.BackColor = ColorTranslator.FromHtml("#FFFFFF");
                    row.ToolTip = "Clique para selecionar.";
                }
            }
        }
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            Grid();
        }
        #endregion
        #region buttons
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            DAL dal = new DAL();
            foreach (GridViewRow row in GridView1.Rows)
            {
                if (row.RowIndex == GridView1.SelectedIndex)
                {
                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                    Cliente cliente = new Cliente
                    {
                        Entidade = row.Cells[0].Text,
                        Nome = row.Cells[1].Text,
                        Contacto = row.Cells[2].Text,
                        Morada = row.Cells[3].Text
                    };
                    bool deleted = dal.DeleteCliente(cliente);
                    if (deleted) Grid();
                }
            }
        }
        protected void btnAdicionar_Click(object sender, EventArgs e)
        {
            Response.Redirect("ClienteAdd.aspx");
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridView1.Rows)
            {
                if (row.RowIndex == GridView1.SelectedIndex)
                {
                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                    Cliente cliente = new Cliente
                    {
                        Entidade = row.Cells[0].Text,
                        Nome = row.Cells[1].Text,
                        Contacto = row.Cells[2].Text,
                        Morada = row.Cells[3].Text
                    };
                    Session["ClientEdit"] = cliente;
                    Response.Redirect("ClienteEdit.aspx");
                }
            }
        }
        protected void search_Click(object sender, EventArgs e)
        {
            string searchTerm = searchBox.Text.ToLower();
            //check if the search input is at least 3 chars
            if (searchTerm.Length >= 0)
            {
                //always check if the viewstate exists before using it
                if (ViewState["myViewState"] == null)
                    return;
                //cast the viewstate as a datatable
                DataTable dt = ViewState["myViewState"] as DataTable;
                //make a clone of the datatable
                DataTable dtNew = dt.Clone();
                //search the datatable for the correct fields
                foreach (DataRow row in dt.Rows)
                {
                    //add your own columns to be searched here
                    if (row["Nome"].ToString().ToLower().Contains(searchTerm)/* || row["Categoria"].ToString().ToLower().Contains(searchTerm)*/)
                    {
                        //when found copy the row to the cloned table
                        dtNew.Rows.Add(row.ItemArray);
                    }
                }
                //rebind the grid
                GridView1.DataSource = dtNew;
                GridView1.DataBind();
            }
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), $"SetKeyPressEvent('{searchBox.ClientID}','{search.ClientID}',13, 'keypress',true);", true);
        }
        #endregion
    }
}