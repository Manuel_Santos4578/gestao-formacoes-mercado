﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dashboard.Master" AutoEventWireup="true" CodeBehind="home.aspx.cs" Inherits="GRFM.Paginas.home" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="scrpHome" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="updHome" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="card">
                <div class="card-body">
                    <asp:Label ID="lblCardHead" runat="server" Text="Suas Formações"></asp:Label>
                    <asp:Button ID="Button1" runat="server" OnClick="btnDownload_Click" CssClass="btn btn-primary" Style="float: right;" ToolTip="Exportar para Excel"/>
                </div>
            </div>
            <div class="card table-responsive">
                <div class="card-body">
                    <p style="float: right">
                        <asp:Button ID="reset" runat="server" OnClick="reset_Click" />
                        <asp:DropDownList ID="ddlCliente" AutoPostBack="true" OnSelectedIndexChanged="ddlCliente_SelectedIndexChanged" runat="server" CssClass="form-control"></asp:DropDownList>

                        <div style="float: left">
                            <asp:Button ID="btnEdit" runat="server" CssClass="btn btn-primary btnedit" Visible="false" ToolTip="Editar" OnClick="btnEdit_Click" />
                        </div>
                    </p>
                    <p>
                        <asp:GridView ID="gvHome" CssClass="table table-bordered" runat="server" AutoGenerateColumns="false" AllowPaging="True" HeaderStyle-CssClass="text-center"
                            DataKeyNames="Id" OnPageIndexChanging="gvHome_PageIndexChanging" ShowHeaderWhenEmpty="false" OnSelectedIndexChanged="gvHome_SelectedIndexChanged"
                            OnRowDataBound="gvHome_RowDataBound" PageSize="15">
                            <Columns>
                                <asp:BoundField DataField="Id" Visible="true" HeaderText="ID" />
                                <asp:BoundField DataField="Estado" HeaderText="Estado" />
                                <asp:BoundField DataField="Nome" HeaderText="Formação" />
                                <asp:BoundField DataField="Cliente.Nome" HeaderText="Cliente" />
                                <asp:BoundField DataField="Categoria" HeaderText="Categoria" />
                                <asp:BoundField DataField="LocalFormacao" HeaderText="Local" />
                                <asp:BoundField DataField="DataInicioString" HeaderText="Data Inicio" />

                            </Columns>
                            <EmptyDataTemplate>
                                <asp:Label ID="lblForm" runat="server" Text="Não tem formações marcadas"></asp:Label>
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </p>
                </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Button1" />
            <asp:AsyncPostBackTrigger ControlID="ddlCliente" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
