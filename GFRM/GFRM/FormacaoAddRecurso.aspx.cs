﻿using Class_Library;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GFRM
{
    public partial class FormacaoAddRecurso : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilizador user = Session["User"] as Utilizador;
            if (user.Permissao != "Admin" && user.Permissao != "Coordenador")
            {
                Response.Redirect("ErrorPage.aspx");
            }
            if (!IsPostBack)
            {
                lblConsume.Visible = false;
                txtQtdConsume.Visible = false;
                btnAdicionarConsume.Visible = false;
                btnAdicionarNoConsume.Visible = false;
                btnRemoveConsume.Visible = false;
                btnRemoveNoConsume.Visible = false;
                lblRemoveConsume.Visible = false;
                txtRemoveConsume.Visible = false;
                btnRemoveOne.Visible = false;
                DatasToDateTimes();
                Grid();
                GridConsumes();
                GridNoConsumes();
            }
        }
        protected void DatasToDateTimes()
        {
            try
            {
                List<DiaFormacao> diaFormacaos = Session["Datas"] as List<DiaFormacao>;
                List<DateTime> dateTimes = new List<DateTime>();
                foreach (DiaFormacao diaFormacao in diaFormacaos)
                {
                    dateTimes.Add(diaFormacao.Data);
                }
                Session["DateTimes"] = dateTimes;
            }
            catch (Exception ex)
            {
                Response.Redirect("Formacoes.aspx");
            }
        }

        #region GridViewConsumes
        protected void Grid()
        {
            DataTable dt = new DAL().GetRecursosConsumiveis();
            //save the datatable into a viewstate for later use
            ViewState["myViewState"] = dt;
            //bind the datasource to the gridview
            GridConsume.DataSource = dt;
            GridConsume.DataBind();
            //to make sure the data isn't loaded in postback
            //use a datatable for storing all the data
            DataTable dt2 = new DataTable();
            if (Session["Datas"] as List<DiaFormacao> != null)
            {
                List<DateTime> dateTimes = Session["DateTimes"] as List<DateTime>;
                dt2 = new DAL().GetRecursosNaoConsumiveis(dateTimes);
            }
            else
            {
                dt2 = new DAL().GetRecursosNaoConsumiveis();
            }
            //save the datatable into a viewstate for later use
            ViewState["myViewState2"] = dt2;

            //bind the datasource to the gridview
            GridNoConsume.DataSource = dt2;
            GridNoConsume.DataBind();
        }
        protected void GridConsume_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridConsume.PageIndex = e.NewPageIndex;
            Grid();
        }   //Paginação
        protected void GridConsume_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridConsume.Rows)
            {
                if (row.RowIndex == GridConsume.SelectedIndex)
                {
                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                    btnAdicionarConsume.Visible = true;
                    lblConsume.Visible = true;
                    txtQtdConsume.Visible = true;
                }
                else
                {
                    row.BackColor = ColorTranslator.FromHtml("#FFFFFF");
                    row.ToolTip = "Clique para selecionar.";
                }
            }
        }
        protected void GridConsume_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(GridConsume, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = "Clique para selecionar.";
            }
        }
        protected void btnAdicionarConsume_Click(object sender, EventArgs e)
        {
            List<RecursoConsumivel> recursoConsumivels = Session["ConsumeList"] as List<RecursoConsumivel>;
            if (recursoConsumivels == null)
            {
                recursoConsumivels = new List<RecursoConsumivel>();
            }
            foreach (GridViewRow row in GridConsume.Rows)
            {
                if (row.RowIndex == GridConsume.SelectedIndex)
                {
                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                    RecursoConsumivel recursoConsumivel = new DAL().SelectRecursoConsumivel(int.Parse(row.Cells[0].Text));
                    if (recursoConsumivels.Count != 0)
                    {
                        bool found = false;
                        foreach (RecursoConsumivel Consumivel in recursoConsumivels)
                        {
                            if (Consumivel.Id == recursoConsumivel.Id)
                            {
                                found = true;
                            }
                        }
                        if (!found)
                        {
                            recursoConsumivel.Saida = int.Parse(txtQtdConsume.Text);
                            if (recursoConsumivel.Saida <= int.Parse(row.Cells[3].Text))
                            {
                                recursoConsumivels.Add(recursoConsumivel);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert",
                                $"alert('Não é possivel exceder o stock existente!');", true);
                            }
                            Session["ConsumeList"] = recursoConsumivels;
                        }
                        else
                        {
                            int index = recursoConsumivels.IndexOf(recursoConsumivels.FirstOrDefault(obj => obj.Id == recursoConsumivel.Id));
                            if ((recursoConsumivels[index].Saida + int.Parse(txtQtdConsume.Text) <= int.Parse(row.Cells[3].Text)))
                            {
                                recursoConsumivels[index].Saida += int.Parse(txtQtdConsume.Text);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert",
                                $"alert('Não é possivel exceder o stock existente!');", true);
                            }
                        }
                    }
                    else
                    {
                        recursoConsumivel.Saida = int.Parse(txtQtdConsume.Text);
                        if (recursoConsumivel.Saida <= int.Parse(row.Cells[3].Text))
                        {
                            recursoConsumivels.Add(recursoConsumivel);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert",
                                $"alert('Não é possivel exceder  o stock existente!');", true);
                        }
                        Session["ConsumeList"] = recursoConsumivels;

                    }
                    GridConsumes();
                }
            }
        }
        #endregion
        #region GridViewNoConsumes
        protected void GridNoConsumes()
        {
            List<RecursoNaoConsumivel> recursoNaoConsumivels = Session["NoConsumeList"] as List<RecursoNaoConsumivel>;
            if (recursoNaoConsumivels == null)
            {
                recursoNaoConsumivels = new List<RecursoNaoConsumivel>();
                Session["NoConsumeList"] = recursoNaoConsumivels;
            }
            NoConsumesFormacao.DataSource = recursoNaoConsumivels;
            NoConsumesFormacao.DataBind();
        }
        protected void GridNoConsume_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridNoConsume.PageIndex = e.NewPageIndex;
            Grid();
        }   //Paginação
        protected void GridNoConsume_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridNoConsume.Rows)
            {
                if (row.RowIndex == GridNoConsume.SelectedIndex)
                {
                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                    btnAdicionarNoConsume.Visible = true;
                }
                else
                {
                    row.BackColor = ColorTranslator.FromHtml("#FFFFFF");
                    row.ToolTip = "Clique para selecionar.";
                }
            }
        }
        protected void GridNoConsumeOnRowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(GridNoConsume, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = "Clique para selecionar..";
            }
        }
        protected void btnRemoveNoConsume_Click(object sender, EventArgs e)
        {
            List<RecursoNaoConsumivel> recursoNaoConsumivels = Session["NoConsumeList"] as List<RecursoNaoConsumivel>;
            try
            {
                foreach (GridViewRow row in NoConsumesFormacao.Rows)
                {
                    if (row.RowIndex == NoConsumesFormacao.SelectedIndex)
                    {
                        row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                        row.ToolTip = string.Empty;
                        recursoNaoConsumivels.Remove(recursoNaoConsumivels.FirstOrDefault(obj => obj.Id == int.Parse(row.Cells[0].Text)));
                        Session["NoConsumeList"] = recursoNaoConsumivels;
                    }
                }
                //ViewState["myViewStateConsumes"] = recursoConsumivels;
            }
            catch (Exception ex) { }
            finally { GridNoConsumes(); UPFAR.Update(); }
        }
        #endregion
        #region NoConsumesFormacao

        protected void NoConsumesFormacao_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (GridViewRow row in NoConsumesFormacao.Rows)
            {
                if (row.RowIndex == NoConsumesFormacao.SelectedIndex)
                {
                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                    btnRemoveNoConsume.Visible = true;
                }
                else
                {
                    row.BackColor = ColorTranslator.FromHtml("#FFFFFF");
                    row.ToolTip = "Clique para selecionar.";
                }
            }
        }
        protected void NoConsumesFormacao_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(NoConsumesFormacao, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = "Clique para selecionar.";
            }
        }
        protected void NoConsumesFormacao_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            NoConsumesFormacao.PageIndex = e.NewPageIndex;
            GridNoConsumes();
        }

        protected void btnAdicionarNoConsume_Click(object sender, EventArgs e)
        {
            List<RecursoNaoConsumivel> recursoNaoConsumivels = Session["NoConsumeList"] as List<RecursoNaoConsumivel>;
            if (recursoNaoConsumivels == null) recursoNaoConsumivels = new List<RecursoNaoConsumivel>();
            foreach (GridViewRow row in GridNoConsume.Rows)
            {
                if (row.RowIndex == GridNoConsume.SelectedIndex)
                {
                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                    RecursoNaoConsumivel recursoNaoConsumivel = new DAL().SelectRecursoNaoConsumivel(int.Parse(row.Cells[0].Text));
                    if (recursoNaoConsumivels.Count != 0)
                    {
                        bool found = false;
                        foreach (RecursoNaoConsumivel naoConsumivel in recursoNaoConsumivels)
                        {
                            if (naoConsumivel.Id == recursoNaoConsumivel.Id)
                            {
                                found = true;
                            }
                        }
                        if (!found)
                        {
                            recursoNaoConsumivels.Add(recursoNaoConsumivel);
                            Session["NoConsumeList"] = recursoNaoConsumivels;
                            GridNoConsumes();
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(),
                            "alert",
                            "alert('Esse já está lá!');",
                            true);
                        }
                    }
                    else
                    {
                        recursoNaoConsumivels.Add(recursoNaoConsumivel);
                        Session["NoConsumeList"] = recursoNaoConsumivels;
                        GridNoConsumes();
                    }
                }
            }
        }
        #endregion
        #region ConsumeFormacao
        protected void GridConsumes()
        {
            List<RecursoConsumivel> recursoConsumivels = Session["ConsumeList"] as List<RecursoConsumivel>;
            if (recursoConsumivels == null)
            {
                recursoConsumivels = new List<RecursoConsumivel>();
                Session["ConsumeList"] = recursoConsumivels;

            }
            ConsumeFormacao.DataSource = recursoConsumivels;
            ConsumeFormacao.DataBind();
        }
        protected void ConsumeFormacao_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(ConsumeFormacao, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = "Clique para selecionar.";
            }
        }
        protected void ConsumeFormacao_SelectedIndexChanged(object sender, EventArgs e)
        {

            foreach (GridViewRow row in ConsumeFormacao.Rows)
            {
                if (row.RowIndex == ConsumeFormacao.SelectedIndex)
                {
                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                    btnRemoveConsume.Visible = true;
                    lblRemoveConsume.Visible = true;
                    txtRemoveConsume.Visible = true;
                    btnRemoveOne.Visible = true;
                }
                else
                {
                    row.BackColor = ColorTranslator.FromHtml("#FFFFFF");
                    row.ToolTip = "Clique para selecionar..";
                }
            }
        }
        protected void ConsumeFormacao_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ConsumeFormacao.PageIndex = e.NewPageIndex;
            GridConsumes();
        }
        protected void btnRemoveConsume_Click(object sender, EventArgs e)
        {
            List<RecursoConsumivel> recursoConsumivels = Session["ConsumeList"] as List<RecursoConsumivel>;
            try
            {
                foreach (GridViewRow row in ConsumeFormacao.Rows)
                {
                    if (row.RowIndex == ConsumeFormacao.SelectedIndex)
                    {
                        row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                        row.ToolTip = string.Empty;
                        recursoConsumivels.Remove(recursoConsumivels.FirstOrDefault(obj => obj.Id == int.Parse(row.Cells[0].Text)));
                        Session["ConsumeList"] = recursoConsumivels;
                    }
                }
                //ViewState["myViewStateConsumes"] = recursoConsumivels;
            }
            catch (Exception ex) { }
            finally { GridConsumes(); UPFAR.Update(); }
        }
        protected void btnRemoveOne_Click(object sender, EventArgs e)
        {
            List<RecursoConsumivel> recursoConsumivels = Session["ConsumeList"] as List<RecursoConsumivel>;
            foreach (GridViewRow row in ConsumeFormacao.Rows)
            {
                if (row.RowIndex == ConsumeFormacao.SelectedIndex)
                {
                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                    int index = recursoConsumivels.IndexOf(recursoConsumivels.FirstOrDefault(obj => obj.Id == int.Parse(row.Cells[0].Text)));
                    if ((recursoConsumivels[index].Saida - int.Parse(txtRemoveConsume.Text)) > 0)
                    {
                        recursoConsumivels[index].Saida -= int.Parse(txtRemoveConsume.Text);
                        txtRemoveConsume.Text = "1";
                        Session["ConsumeList"] = recursoConsumivels;

                        GridConsumes();
                    }
                    else
                    {
                        recursoConsumivels.Remove(recursoConsumivels.FirstOrDefault(obj => obj.Id == int.Parse(row.Cells[0].Text)));
                        GridConsumes();
                    }

                }
            }
        }

        protected void btnCriarFormacao_Click(object sender, EventArgs e)
        {
            Formacao formacao = Session["Formacao"] as Formacao;
            List<RecursoNaoConsumivel> naoConsumiveis = Session["NoConsumeList"] as List<RecursoNaoConsumivel>;
            List<RecursoConsumivel> consumiveis = Session["ConsumeList"] as List<RecursoConsumivel>;
            formacao.ListaConsumivel = consumiveis;
            formacao.ListaNConsumiveis = naoConsumiveis;
            bool inserted = new DAL().InsertFormacao(formacao);
            if (inserted)
            {
                Session["Formacao"] = null;
                Session["Datas"] = null;
                Session["ConsumeList"] = null;
                Session["NoConsumeList"] = null;
                Session["Disponibilidades"] = null;
                Response.Redirect("Formacoes.aspx");
            }
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            Response.Redirect("FormacaoAdd.aspx");
        }
    }
    #endregion
}
