﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dashboard.Master" AutoEventWireup="true" CodeBehind="FormacaoAddRecurso.aspx.cs" Inherits="GFRM.FormacaoAddRecurso" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManagerFAR" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UPFAR" ChildrenAsTriggers="false" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <div class="row">
                <aside class="col-sm-6">
                    <p class="title">
                        Consumíveis
                    </p>
                    <div class="card table-responsive" style="min-height: 484px">
                        <article class="card-body">
                            <div>
                                <div class="form-group">
                                    <label>Nome</label>
                                    <asp:TextBox ID="TxtConsumeSearch" runat="server" CssClass="form-control"></asp:TextBox>
                                    <div style="min-height: 50px;margin-top:16px;">
                                        <asp:Button ID="btnAdicionarConsume" runat="server" CssClass="btn btn-primary btnadd" Style="margin-top: 2px; margin-bottom: -2px;" OnClick="btnAdicionarConsume_Click" />
                                        <asp:Label ID="lblConsume" runat="server" Text="Quantidade:"></asp:Label>
                                        <asp:TextBox ID="txtQtdConsume" runat="server" TextMode="Number" min="1" Style="width: 50px;" Text="1"></asp:TextBox>
                                    </div>
                                </div>
                                <!-- form-group// -->
                                <div class="form-group" style="margin-top:-10px;">
                                    <asp:GridView ID="GridConsume" CssClass="table table-bordered" runat="server" AutoGenerateColumns="false" Style="max-width: 100%;" AllowPaging="True"
                                        HeaderStyle-CssClass="text-center" OnPageIndexChanging="GridConsume_PageIndexChanging" PageSize="5" DataKeyNames="Id" OnSelectedIndexChanged="GridConsume_SelectedIndexChanged"
                                        OnRowDataBound="GridConsume_RowDataBound">
                                        <Columns>
                                            <asp:BoundField DataField="Id" Visible="true" HeaderText="ID" />
                                            <asp:BoundField DataField="Nome" HeaderText="Nome" />
                                            <asp:BoundField DataField="Categoria" HeaderText="Categoria" />
                                            <asp:BoundField DataField="Quantidade" HeaderText="Em Stock" />
                                        </Columns>
                                    </asp:GridView>
                                    <div style="min-height: 50px;margin-top:16px;">
                                        <asp:Button ID="btnRemoveConsume" runat="server" CssClass="btn btn-primary btndelete" OnClick="btnRemoveConsume_Click" />
                                        <asp:Label ID="lblRemoveConsume" runat="server" Text="Quantidade:"></asp:Label>                                      
                                        <asp:TextBox ID="txtRemoveConsume" runat="server" TextMode="Number" min="1" Style="width: 50px;" Text="1"></asp:TextBox>
                                        <asp:Button ID="btnRemoveOne" runat="server"  CssClass="btn btn-primary btnminus" Tooltip="Retirar Quantidade" OnClick="btnRemoveOne_Click" />
                                    </div>
                                    <asp:GridView ID="ConsumeFormacao" AutoGenerateColumns="false" AllowPaging="True" CssClass="table table-bordered" DataKeyNames="Id" HeaderStyle-CssClass="text-center" runat="server"
                                        PageSize="5" OnRowDataBound="ConsumeFormacao_RowDataBound" OnPageIndexChanging="ConsumeFormacao_PageIndexChanging" OnSelectedIndexChanged="ConsumeFormacao_SelectedIndexChanged" Style="max-width: 100%;">
                                        <Columns>
                                            <asp:BoundField DataField="Id" Visible="true" HeaderText="ID" />
                                            <asp:BoundField DataField="Nome" HeaderText="Nome" />
                                            <asp:BoundField DataField="Categoria" HeaderText="Categoria" />
                                            <asp:BoundField DataField="Saida" HeaderText="Alocado" />
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <asp:Label ID="lblConsumeFormacao" runat="server" Style="text-align: center" Text="Não existem consumiveis alocados neste momento"></asp:Label>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                                <!-- form-group// -->
                            </div>
                        </article>
                    </div>
                    <!-- card.// -->
                </aside>
                <!-- col.// -->
                <!-- col.// -->
                <aside class="col-sm-6">
                    <p class="title">
                        Não Consumíveis
                    </p>
                    <div class="card table-responsive" style="min-height: 484px">
                        <article class="card-body">
                            <div>
                                <div class="form-group">
                                    <label>Nome</label>
                                    <asp:TextBox ID="txtNoConsumeSearch" runat="server" CssClass="form-control"></asp:TextBox>
                                    <div style="min-height: 50px;margin-top:16px;">
                                        <asp:Button ID="btnAdicionarNoConsume" runat="server" CssClass="btn btn-primary btnadd" Style="margin-top: 2px; margin-bottom: -2px;" OnClick="btnAdicionarNoConsume_Click" />
                                    </div>
                                </div>
                                <!-- form-group// -->
                                <div class="form-group">
                                    <asp:GridView ID="GridNoConsume" CssClass="table table-bordered" runat="server" AutoGenerateColumns="false" Style="max-width: 100%;" AllowPaging="True"
                                        HeaderStyle-CssClass="text-center" OnPageIndexChanging="GridNoConsume_PageIndexChanging" PageSize="5" DataKeyNames="Id" OnRowDataBound="GridNoConsumeOnRowDataBound"
                                        OnSelectedIndexChanged="GridNoConsume_SelectedIndexChanged">
                                        <Columns>
                                            <asp:BoundField DataField="Id" Visible="true" HeaderText="ID" />
                                            <asp:BoundField DataField="Nome" HeaderText="Nome" />
                                        </Columns>
                                    </asp:GridView>
                                    <div style="min-height: 50px;margin-top:16px;">
                                        <asp:Button ID="btnRemoveNoConsume" runat="server" CssClass="btn btn-primary btndelete" OnClick="btnRemoveNoConsume_Click" />
                                    </div>
                                    <asp:GridView ID="NoConsumesFormacao" AutoGenerateColumns="false" AllowPaging="True" CssClass="table table-bordered" DataKeyNames="Id" HeaderStyle-CssClass="text-center" runat="server"
                                        PageSize="5" OnRowDataBound="NoConsumesFormacao_RowDataBound" OnPageIndexChanging="NoConsumesFormacao_PageIndexChanging" OnSelectedIndexChanged="NoConsumesFormacao_SelectedIndexChanged" Style="max-width: 100%;">
                                        <Columns>
                                            <asp:BoundField DataField="Id" Visible="true" HeaderText="ID" />
                                            <asp:BoundField DataField="Nome" HeaderText="Nome" />
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <asp:Label ID="lblNoConsumeFormacao" runat="server" Style="text-align: center" Text="Não existem não consumiveis alocados neste momento"></asp:Label>
                                        </EmptyDataTemplate>
                                    </asp:GridView>

                                </div>
                                <!-- form-group// -->
                            </div>
                        </article>
                    </div>
                    <!-- card.// -->

                </aside>
                <!-- col.// -->
            </div>
            <div class="col-md-12 row" style="margin-top:16px;">
                <div class="col-md-12" >
                    <asp:Button ID="btnCriarFormacao" runat="server" CssClass="btn btn-primary" style="float:right;" OnClick="btnCriarFormacao_Click" Text="Criar" 
                        OnClientClick="if ( ! addConfirm()) return false;"/>
                    <asp:Button ID="btnVoltar" runat="server" Text="Voltar" CssClass="btn btn-danger" OnClick="btnVoltar_Click" />
                </div>
            </div>
    <!--container end.//-->
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="GridNoConsume" EventName="PageIndexChanging" />
            <asp:AsyncPostBackTrigger ControlID="NoConsumesFormacao" EventName="PageIndexChanging" />
            <asp:AsyncPostBackTrigger ControlID="ConsumeFormacao" EventName="PageIndexChanging" />
            <asp:AsyncPostBackTrigger ControlID="GridConsume" EventName="PageIndexChanging" />
            <asp:AsyncPostBackTrigger ControlID="GridNoConsume" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="GridConsume" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="NoConsumesFormacao" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ConsumeFormacao" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="btnAdicionarNoConsume" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnAdicionarConsume" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnRemoveNoConsume" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnRemoveConsume" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnRemoveOne" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnCriarFormacao" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    
    <script type="text/javascript">
        function addConfirm() {
            return confirm("Pretende mesmo adicionar estes recursos a esta formação?");
        }
    </script>
</asp:Content>
