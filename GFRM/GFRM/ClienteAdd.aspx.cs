﻿using Class_Library;
using ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GFRM
{
    public partial class ClienteAdd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilizador user = Session["User"] as Utilizador;
            if (user.Permissao != "Admin" && user.Permissao != "Coordenador")
            {
                Response.Redirect("ErrorPage.aspx");
            }
        }
        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(TextBoxAddress.Text) && !string.IsNullOrEmpty(TextBoxContact.Text) &&
                !string.IsNullOrEmpty(TextBoxName.Text) && !string.IsNullOrEmpty(TextBoxAddress.Text))
            {
                Cliente cliente = new Cliente
                {
                    Entidade = TextBoxEntity.Text,
                    Contacto = TextBoxContact.Text,
                    Morada = TextBoxAddress.Text,
                    Nome = TextBoxName.Text
                };
                if (BLL.ValidarInserirCliente(cliente))
                {
                    bool inserted = new DAL().InsertCliente(cliente);
                    TextBoxEntity.Text = (inserted) ? "" : TextBoxEntity.Text;
                    TextBoxAddress.Text = (inserted) ? "" : TextBoxAddress.Text;
                    TextBoxName.Text = (inserted) ? "" : TextBoxName.Text;
                    TextBoxContact.Text = (inserted) ? "" : TextBoxContact.Text;
                }
            }
        }
        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ClienteMNG.aspx");
        }
    }
}