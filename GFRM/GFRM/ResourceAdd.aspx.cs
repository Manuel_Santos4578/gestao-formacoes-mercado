﻿using Class_Library;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GFRM
{
    public partial class ResourceAdd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilizador user = Session["User"] as Utilizador;
            if (user.Permissao != "Admin" && user.Permissao != "Worker")
            {
                Response.Redirect("ErrorPage.aspx");
            }
            txtMinVal.Visible = false;
            if (rb.SelectedValue == "Consumivel")
            {
                txtQtd.Visible = true;
                lblQtd.Visible = true;
                lblMinValue.Visible = true;
                txtMinVal.Visible = true;
            }
            else
            {
                txtQtd.Visible = false;
                lblQtd.Visible = false;
                lblMinValue.Visible = false;
            }
            if (!IsPostBack)
            {
                preencherCat();
            }
        }
        private void preencherCat()
        {
            DataTable categoria = new DAL().GetCategorias();
            ddlCat.DataSource = categoria;
            ddlCat.DataTextField = "Nome";
            ddlCat.DataValueField = "Nome";
            ddlCat.DataBind();
            ddlCat.Items.Insert(0, "Selecione a categoria");
            ddlCat.SelectedIndex = 0;
        }
        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            if (rb.SelectedValue == "Consumivel")
            {
                if (!string.IsNullOrWhiteSpace(txtNome.Text) && !string.IsNullOrWhiteSpace(txtQtd.Text))
                {
                    RecursoConsumivel recurso = new RecursoConsumivel();
                    recurso.Nome = txtNome.Text;
                    recurso.Categoria = ddlCat.SelectedValue as string;
                    int Quantidade = Convert.ToInt32(txtQtd.Text);
                    recurso.ValorMinimo = int.Parse(txtMinVal.Text);
                    bool inserted = new DAL().InsertRecursoConsumivel(recurso, Quantidade);
                    if (inserted)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(),
                    "alert",
                    $"alert('{recurso.Nome} criado com sucesso!');window.location ='ResourceMNG.aspx';",
                    true);
                    }
                }
            }
            else if (rb.SelectedValue == "NaoConsumivel")
            {
                if (!string.IsNullOrWhiteSpace(txtNome.Text))
                {
                    RecursoNaoConsumivel recurson = new RecursoNaoConsumivel();
                    recurson.Nome = txtNome.Text;
                    recurson.Categoria = ddlCat.SelectedItem.Text;
                    bool inserted = new DAL().InsertRecursoNaoConsumivel(recurson);
                    if (inserted)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(),
                        "alert",
                        $"alert('{recurson.Nome} criado com sucesso!');window.location ='ResourceMNG.aspx';",
                        true);
                    }
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Tem de selecionar o tipo de recurso')", true);
            }
            if (string.IsNullOrWhiteSpace(txtMinVal.Text) || string.IsNullOrWhiteSpace(txtNome.Text) || string.IsNullOrWhiteSpace(txtQtd.Text) || ddlCat.SelectedItem == ddlCat.Items.FindByText("Selecione a categoria"))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Existem campos por preencher!')", true);
            }
        }
        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ResourceMNG.aspx");
        }
    }
}