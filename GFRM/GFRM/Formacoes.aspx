﻿<%@ Page Title="GRFM" Language="C#" MasterPageFile="~/dashboard.Master" AutoEventWireup="true" CodeBehind="Formacoes.aspx.cs" Inherits="GFRM.Formacoes" EnableEventValidation="false"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta charset='utf-8' />
    <link href='../packages/core/main.css' rel='stylesheet' />
    <link href='../packages/daygrid/main.css' rel='stylesheet' />
    <link href='../packages/timegrid/main.css' rel='stylesheet' />
    <script src='../packages/core/main.js'></script>
    <script src='../packages/core/locales/pt.js'></script>
    <script src='../packages/interaction/main.js'></script>
    <script src='../packages/daygrid/main.js'></script>
    <script src='../packages/timegrid/main.js'></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="scpManager" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="updPanelUser" ChildrenAsTriggers="false" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="card">
                <div class="card-body">
                    <asp:Label ID="lblCardHead" runat="server" Text="Formações"></asp:Label>
                    <div style="float: right;">
                        <asp:Button ID="btnDownload" runat="server" OnClick="btnDownload_Click" CssClass="btn btn-primary" ToolTip="Exportar para Excel"/>
                        <asp:Button ID="btnAdicionar" runat="server" CssClass="btn btn-primary btnadd" ToolTip="Adicionar Novo/a" OnClick="btnAdicionar_Click" />
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <asp:DropDownList ID="ddlEstado" AutoPostBack="true" OnSelectedIndexChanged="ddlEstado_SelectedIndexChanged" runat="server" CssClass="form-control"></asp:DropDownList>
                    <asp:DropDownList ID="ddlCliente" AutoPostBack="true" OnSelectedIndexChanged="ddlCliente_SelectedIndexChanged" runat="server" CssClass="form-control"></asp:DropDownList>
                    <asp:DropDownList ID="ddlFormador" AutoPostBack="true" OnSelectedIndexChanged="ddlFormador_SelectedIndexChanged" runat="server" CssClass="form-control"></asp:DropDownList>
                    <p style="float: right">
                    <asp:Button ID="reset" runat="server" OnClick="reset_Click"/>
                    </p>
                    <div style="float:left">
                        <asp:Button runat="server" ID="btnUpdate" ToolTip="Editar" CssClass="btn btn-primary btnedit" Visible="false" OnClick="btnUpdate_Click" />
                        <asp:Button ID="btnEdit" runat="server" CssClass="btn btn-success btnedit" Visible="false" ToolTip="Detalhes" OnClick="btnEdit_Click" />
                        <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger btndelete" Visible="false" ToolTip="Apagar" OnClick="btnDelete_Click" 
                            OnClientClick="if ( ! deleteConfirm()) return false;"/>
                    </div>
                    <div class="table-responsive">
                        <asp:GridView ID="gvFormacoes" CssClass="table table-bordered" runat="server" AutoGenerateColumns="false" AllowPaging="True" HeaderStyle-CssClass="text-center"
                            DataKeyNames="Id" OnPageIndexChanging="gvFormacoes_PageIndexChanging" ShowHeaderWhenEmpty="false" OnSelectedIndexChanged="gvFormacoes_SelectedIndexChanged"
                            OnRowDataBound="gvFormacoes_RowDataBound" PageSize="15" >
                            <Columns>
                                <asp:BoundField DataField="Id" HeaderText="ID" />
                                <asp:BoundField DataField="Estado" HeaderText="Estado" />
                                <asp:BoundField DataField="Nome" HeaderText="Formação" />
                                <asp:BoundField DataField="Formador.Nome" HeaderText="Formador" />
                                <asp:BoundField DataField="Cliente.Nome" HeaderText="Cliente" />
                                <asp:BoundField DataField="Categoria" HeaderText="Categoria" />
                                <asp:BoundField DataField="LocalFormacao" HeaderText="Local" />
                                <asp:BoundField DataField="DataInicioString" HeaderText="Data Inicio" />                           
                            </Columns>
                            <EmptyDataTemplate>
                                <asp:Label ID="lblEmpty" runat="server" Text="Não existem dados para mostrar"></asp:Label>
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                  </div>  
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="gvFormacoes" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlCliente" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlFormador" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlEstado" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnEdit" EventName="Click" />
            <asp:PostBackTrigger ControlID="btnDownload" />
            <asp:AsyncPostBackTrigger ControlID="reset" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
