﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary;
using Class_Library;
using System.Net.Mail;

namespace GFRM
{
    public partial class UserAdd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilizador user = Session["User"] as Utilizador;
            if (user.Permissao != "Admin" && user.Permissao != "Coordenador")
            {
                Response.Redirect("ErrorPage.aspx");
            }
            txtNome.Focus();

            if (!IsPostBack)
            {
                DropDownPermissao();
                FillCategorias();
            }
        }
        protected void FillCategorias()
        {
            chkbCategory.DataSource = new DAL().GetCategorias();
            chkbCategory.DataTextField = "Nome";
            chkbCategory.DataValueField = "Nome";
            chkbCategory.DataBind();
        }
        public void DropDownPermissao()
        {
            DataTable permissions = new DAL().GetPermissoes();
            ddlPerm.DataSource = permissions;
            ddlPerm.DataTextField = "Nome";
            ddlPerm.DataValueField = "Nome";
            ddlPerm.DataBind();
            ddlPerm.Items.Insert(0, "Selecione a permissão");
            ddlPerm.SelectedIndex = 0;
        }
        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            string PW = Auxiliar.RandomStringGen();
            Utilizador user = new Utilizador(PW);
            if (!string.IsNullOrEmpty(PW) && !string.IsNullOrWhiteSpace(txtUsername.Text) && !string.IsNullOrWhiteSpace(txtEmail.Text)
                && !string.IsNullOrWhiteSpace(txtNome.Text) && ddlPerm.SelectedIndex != 0 && Auxiliar.ValidateEmail(txtEmail.Text))
            {
                user.Username = txtUsername.Text.ToLower();
                user.Nome = txtNome.Text;
                user.Email = txtEmail.Text.ToLower();
                user.Permissao = ddlPerm.SelectedValue as string;
                user.FirstPassword = user.Password;
                foreach (ListItem li in chkbCategory.Items)
                {
                    if (li.Selected == true)
                    {
                        user.Categorias.Add(li.Text);
                    }
                }
            }
            if (BLL.ValidarUtilizador(user))
            {
                bool insert = new DAL().InsertUtilizador(user);
                if (insert)
                {
                    if (Auxiliar.SendEmail("no_replyAtecGFRM@outlook.com", user.Email, "atec+123"))
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert",
                        "alert('Utilizador Registado com sucesso!');window.location ='UserMNG.aspx';", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert",
                        "alert('Falhou o envio do e-mail');window.location ='UserMNG.aspx';", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert",
                    "alert('Algo correu mal.');", true);
                }
            }
            else
            {
                txtUsername.ReadOnly = false;
                lblLog.Text = "Username já existe! Por favor escolha outro";
            }
        }
        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserMNG.aspx");
        }

    }
}