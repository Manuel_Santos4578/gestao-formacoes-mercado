﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Class_Library;

namespace GFRM
{
    public partial class UserMNG : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilizador user = Session["User"] as Utilizador;
            if (user.Permissao != "Admin" && user.Permissao != "Coordenador")
            {
                Response.Redirect("ErrorPage.aspx");
            }
            btnEdit.Visible = false;
            btnDelete.Visible = false;
            if (!Page.IsPostBack)
            {
                FillddlCat();
                Grid();
            }
        }
        
        #region Botões Search e Reset
        //protected void searchButton_Click(object sender, EventArgs e)
        //{
        //    string searchTerm = searchBox.Text.ToLower();
        //    //check if the search input is at least 3 chars
        //    if (searchTerm.Length >= 3)
        //    {
        //        //always check if the viewstate exists before using it
        //        if (ViewState["myViewState"] == null)
        //            return;
        //        //cast the viewstate as a datatable
        //        DataTable dt = ViewState["myViewState"] as DataTable;
        //        //make a clone of the datatable
        //        DataTable dtNew = dt.Clone();
        //        //search the datatable for the correct fields
        //        foreach (DataRow row in dt.Rows)
        //        {
        //            //add your own columns to be searched here
        //            if (row["Nome"].ToString().ToLower().Contains(searchTerm) ||
        //                row["Categoria"].ToString().ToLower().Contains(searchTerm) ||
        //                row["Email"].ToString().ToLower().Contains(searchTerm))
        //            {
        //                //when found copy the row to the cloned table
        //                dtNew.Rows.Add(row.ItemArray);
        //            }
        //        }
        //        //rebind the grid
        //        GridView1.DataSource = dtNew;
        //        GridView1.DataBind();
        //    }
        //}
        #endregion
        #region GridView
        protected void Grid()
        {
            List<Utilizador> users = new List<Utilizador>();
            if (!string.IsNullOrWhiteSpace(searchBox.Text))
            {

                users = new DAL().SelectUtilizadores(searchBox.Text, searchBox.Text, ddlCat.SelectedItem.Value);
            }
            else

            {
                users = new DAL().SelectUtilizadores("", "", ddlCat.SelectedItem.Value);
            }

            //ViewState["myViewState"] = dt;
            GridView1.DataSource = users;
            GridView1.DataBind();
        }
        #endregion
        #region Selectable row
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridView1.Rows)
            {
                if (row.RowIndex == GridView1.SelectedIndex)
                {
                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                }
                else
                {
                    row.BackColor = ColorTranslator.FromHtml("#FFFFFF");
                    row.ToolTip = "Clique para selecionar.";
                }
            }
        }
        protected void OnRowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(GridView1, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = "Clique para selecionar.";
            }
        }
        #endregion
        #region buttons
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            DAL dal = new DAL();
            foreach (GridViewRow row in GridView1.Rows)
            {
                if (row.RowIndex == GridView1.SelectedIndex)
                {
                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;

                    Utilizador user = new Utilizador
                    {
                        Username = row.Cells[0].Text,
                        Nome = row.Cells[1].Text,
                        Email = row.Cells[2].Text,
                        Permissao = row.Cells[4].Text
                    };
                    if (user.Permissao != "Admin")
                    {
                        bool deleted = dal.DeleteUtilizador(user);
                        if (deleted) Grid();
                        searchBox.Text = "";
                        btnEdit.Visible = false;
                        btnDelete.Visible = false;
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert",
                            "alert('Utilizador é admin e não pode ser apagado');", true);
                    }
                }
            }
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridView1.Rows)
            {
                if (row.RowIndex == GridView1.SelectedIndex)
                {
                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;

                    Utilizador user = new Utilizador
                    {
                        Username = row.Cells[0].Text,
                        Nome = row.Cells[1].Text,
                        Email = row.Cells[2].Text,
                        Permissao = row.Cells[4].Text
                    };
                    string categoriasString = row.Cells[3].Text;
                    List<string> categorias = categoriasString.Split(',').ToList();

                    for (int i = 0; i < categorias.Count; i++)
                    {
                        if (categorias[i].Contains(' '))
                        {
                            categorias[i] = categorias[i].Remove(categorias[i].IndexOf(" "), 1);
                        }
                    }
                    user.Categorias = categorias;
                    Session["UserEdit"] = user;
                    Response.Redirect("UserEdit.aspx");
                }
            }
        }
        protected void search_Click(object sender, EventArgs e)
        {
            Grid();
            //string searchTerm = searchBox.Text.ToLower();

            ////check if the search input is at least 3 chars
            //if (searchTerm.Length >= 0)
            //{
            //    //always check if the viewstate exists before using it
            //    if (ViewState["myViewState"] == null)
            //        return;

            //    //cast the viewstate as a datatable
            //    DataTable dt = ViewState["myViewState"] as DataTable;

            //    //make a clone of the datatable
            //    DataTable dtNew = dt.Clone();

            //    //search the datatable for the correct fields
            //    foreach (DataRow row in dt.Rows)
            //    {
            //        //add your own columns to be searched here
            //        if (row["Nome"].ToString().ToLower().Contains(searchTerm)/* || row["Categoria"].ToString().ToLower().Contains(searchTerm)*/)
            //        {
            //            //when found copy the row to the cloned table
            //            dtNew.Rows.Add(row.ItemArray);
            //        }
            //    }

            //    //rebind the grid
            //    GridView1.DataSource = dtNew;
            //    GridView1.DataBind();
            //}

            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), $"SetKeyPressEvent('{searchBox.ClientID}','{search.ClientID}',13, 'keypress',true);", true);
        }
        protected void resetSearchButton_Click(object sender, EventArgs e)
        {
            Grid();
            //always check if the viewstate exists before using it
            //if (ViewState["myViewState"] == null)
            //    return;

            ////cast the viewstate as a datatable
            //DataTable dt = ViewState["myViewState"] as DataTable;

            ////rebind the grid
            //GridView1.DataSource = dt;
            //GridView1.DataBind();
            //searchBox.Text = "";
        }
        protected void btnAdicionar_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserAdd.aspx");
        }
        #endregion
        protected void ddlCat_SelectedIndexChanged(object sender, EventArgs e)
        {
            Grid();
        }
        protected void FillddlCat()
        {
            try
            {
                ddlCat.DataSource = new DAL().GetCategorias();
                ddlCat.DataTextField = "Nome";
                ddlCat.DataValueField = "Nome";
                ddlCat.DataBind();
                ListItem item = new ListItem("Selecione a categoria", "%");
                ddlCat.Items.Insert(0, item);
                ddlCat.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                // Erro
            }
        }
    }
}