﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dashboard.Master" AutoEventWireup="true" CodeBehind="FormacaoAdd.aspx.cs" Inherits="GFRM.FormacaoAdd" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta charset='utf-8' />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="scriptManager1" runat="server" />
    <asp:UpdatePanel ID="updPanel" ChildrenAsTriggers="false" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="container">
                <br>
                <p class="text-center">
                    <asp:Label ID="lblCardHead" CssClass="card-body" runat="server" Text="Nova Formação" Style="font-weight: 400; font-size: 24px;"></asp:Label>
                </p>
                <hr>
                <div class="row">
                    <aside class="col-sm-4">
                        <p style="font-weight: 400; font-size: 20px; margin-top: 16px;">Dados Formação</p>
                        <div class="card table-responsive" style="min-height: 671px">
                            <article class="card-body">
                                <div>
                                    <div class="form-group col-md-12 row">
                                        <div class="col-md-12">
                                            <label>Nome</label>
                                        </div>
                                        <asp:TextBox ID="txtNome" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <!-- form-group// -->
                                    <div class="form-group col-md-12 row">
                                        <div class="col-md-12">
                                            <label>Cliente</label>
                                        </div>
                                        <asp:DropDownList ID="ddlCliente" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlCliente_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                    <!-- form-group// -->
                                    <div class="form-group col-md-12 row">
                                        <div class="col-md-12">
                                            <label>Categoria</label>
                                        </div>
                                        <asp:DropDownList ID="ddlCategoria" runat="server" OnSelectedIndexChanged="ddlCategoria_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                    <!-- form-group// -->
                                    <div class="form-group col-md-12 row">
                                        <div class="col-md-12">
                                            <label>Formador</label>
                                        </div>
                                        <asp:DropDownList ID="ddlFormador" AutoPostBack="true" OnSelectedIndexChanged="ddlFormador_SelectedIndexChanged" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                    <!-- form-group// -->
                                    <div class="form-group">
                                    </div>
                                    <!-- form-group// -->
                                    <div class="form-group">
                                        <label id="lblLocalFormacao" runat="server" visible="false">Local Formação</label>
                                        <asp:RadioButtonList ID="rbLocalFormacao" Visible="false" runat="server">
                                            <asp:ListItem Enabled="true">Cliente</asp:ListItem>
                                            <asp:ListItem>Atec</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                    </article>
                </div>
                <!-- card.// -->
                </aside>
                    <!-- col.// -->
                <aside class="col-sm-4">
                    <p style="font-weight: 400; font-size: 20px; margin-top: 16px;">Dias Formação</p>
                    <div class="card table-responsive" style="min-height: 671px">
                        <article class="card-body">
                            <div class="form-group">
                                <asp:Calendar ID="CalendarAvailability" SelectedDayStyle-BackColor="Black" OnDayRender="CalendarAvailability_DayRender" OnSelectionChanged="CalendarAvailability_SelectionChanged" runat="server"
                                    WeekendDayStyle-BackColor="OrangeRed" WeekendDayStyle-ForeColor="Black" FirstDayOfWeek="Monday" CssClass="calendary"></asp:Calendar>
                            </div>
                            <hr>
                            <div>
                                <!-- form-group// -->
                                <div class="form-group">
                                    <asp:Label ID="LabelDuration" runat="server" Text="Duração"></asp:Label>
                                    <asp:TextBox ID="TextBoxDuration" runat="server" TextMode="Number" min="0" max="24" Text="8" placeholder="Duracao" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="LabelStart" runat="server" Text="Hora de Início"></asp:Label>
                                    <asp:TextBox ID="TextBoxStartTime" runat="server" TextMode="Time" value="09:00" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="LabelFinish" runat="server" Text="Hora de Fim"></asp:Label>
                                    <asp:TextBox ID="TextBoxEndTime" runat="server" TextMode="Time" value="18:00" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="LabelValue" runat="server" Text="Valor/Hora pago ao formador"></asp:Label>
                                    <asp:TextBox ID="TextBoxValue" runat="server" placeholder=" Em Euros(€)" CssClass="form-control"></asp:TextBox>
                                </div>
                                <!-- form-group// -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <asp:Button ID="btnCalendar" runat="server" class="btn btn-primary btncheck" OnClick="btnCalendar_Click" ToolTip="Adicionar" />
                                        </div>
                                        <!-- form-group// -->
                                    </div>
                                    <div class="col-md-6 text-right">
                                    </div>
                                </div>
                                <!-- .row// -->
                            </div>
                        </article>
                    </div>
                    <!-- card.// -->
                </aside>
                <!-- col.// -->
                <aside class="col-sm-4">
                    <p style="font-weight: 400; font-size: 20px; margin-top: 16px;">Dias Formação Marcados</p>
                    <div class="card table-responsive" style="min-height: 671px">
                        <article class="card-body">
                            <div class="form-group">
                                <p>
                                    <asp:Label ID="lblDatas" runat="server" Text="Dias Formação"></asp:Label>
                                </p>
                                <asp:ListBox ID="lbDatas" runat="server" AutoPostBack="true" Style="min-width: 100%; min-height: 250px;" OnSelectedIndexChanged="lbDatas_SelectedIndexChanged"></asp:ListBox>
                                <asp:Button ID="btnRemove" Visible="false" runat="server" CssClass="btn btn-danger btndelete" OnClick="btnRemove_Click" ToolTip="Remover" />
                            </div>
                        </article>
                    </div>
                    <!-- card.// -->
                </aside>
                <!-- col.// -->
            </div>
            <!-- row.// -->
            <br>
            <div style="display: inline; float: right;">
                <asp:Button Text="Adicionar Recursos" CssClass=" btn btn-primary" ID="btnCreate" OnClick="btnCriar_Click" runat="server" />
                <asp:Button ToolTip="Cancelar" CssClass=" btn btn-danger btncancel" ID="btnCancel" OnClick="btnCancel_Click" OnClientClick="" runat="server" />
            </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnCreate" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ddlCliente" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlCategoria" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlFormador" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="btnCalendar" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="CalendarAvailability" EventName="SelectionChanged" />
            <asp:AsyncPostBackTrigger ControlID="lbDatas" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="btnRemove" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>

    <%--    <script type="text/javascript">
        function addConfirm() {
            return confirm("Pretende mesmo adicionar esta formação?");
        }
    </script>--%>
</asp:Content>
