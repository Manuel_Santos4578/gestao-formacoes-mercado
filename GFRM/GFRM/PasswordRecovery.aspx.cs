﻿using Class_Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GFRM
{
    public partial class PasswordRecovery : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRecover_Click(object sender, EventArgs e)
        {

            DAL dal = new DAL();
            if (!string.IsNullOrWhiteSpace(txtEmail.Text))
            {
                Utilizador utilizador = dal.SelectUtilizador(txtEmail.Text);
                string password;
                bool recovered = dal.UpdateUtilizadorPasswordReset(utilizador, out password);

                if (utilizador != null)
                {
                    SmtpClient smtpClient = new SmtpClient("smtp.office365.com", 587);
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = new System.Net.NetworkCredential("no_replygfrm@outlook.com", "atec+123");

                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtpClient.EnableSsl = true;
                    MailMessage mail = new MailMessage();
                    mail.Body = $"Foi efetuado um reset á sua password.\n Nova Password: {password}\n " +
                        $"Por questões de segurança, aconselhamos a alteração da sua password\n" +
                        $"Atentamente,\n" +
                        $"Equipa GFRM.";
                    mail.Subject = "Credenciais GFRM (Reset á Password)";

                    //Setting From , To and CC
                    mail.From = new MailAddress("no_replygfrm@outlook.com");
                    mail.To.Add(new MailAddress($"{utilizador.Email}"));
                    try
                    {
                        smtpClient.Send(mail);

                        ScriptManager.RegisterStartupScript(this, this.GetType(),
                        "alert",
                        "alert('Foi enviado para o seu e-mail a sua nova password');window.location ='Login.aspx';",
                        true);
                    }
                    catch (Exception ex)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(),
                        "alert",
                        "alert('Envio do e-mail de recuperação falhado!');window.location ='Login.aspx';",
                        true);
                    }
                }
            }
            else
            {
                lblMsg.Text = "Existem campos por preencher";
            }
        }
    }
}