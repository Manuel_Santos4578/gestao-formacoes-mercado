﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary;
using Class_Library;
using System.Data;

namespace GFRM
{
    public partial class RecursoEditConsume : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilizador user = Session["User"] as Utilizador;
            if (user.Permissao != "Admin" && user.Permissao != "Worker")
            {
                Response.Redirect("ErrorPage.aspx");
            }
            if (!IsPostBack)
            {
                FillCat();
                FillFields();
            }
        }
        protected void ButtonEdit_Click(object sender, EventArgs e)
        {
            RecursoConsumivel recurso = Session["RecursoEdit"] as RecursoConsumivel;
            recurso.Nome = string.IsNullOrWhiteSpace(TextBoxName.Text) ? recurso.Nome : TextBoxName.Text;
            recurso.ValorMinimo = (int.Parse(TextBoxMinVal.Text) <= 0) ? recurso.ValorMinimo : int.Parse(TextBoxMinVal.Text);
            recurso.Categoria = (DropDownCategory.SelectedIndex == 0) ? recurso.Categoria : DropDownCategory.SelectedValue;
            bool updated = new DAL().UpdateRecursoConsumivel(recurso);
            if (updated)
            {
                Session["RecursoEdit"] = null;
                Session["comeback"] = "Consume";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert",
                    $"alert('{recurso.Nome} modificado com sucesso!');window.location ='ResourceMNG.aspx';", true);
            }
        }
        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            Session["RecursoEdit"] = null;
            Session["comeback"] = "Consume";
            Response.Redirect("ResourceMNG.aspx");
        }
        protected void FillFields()
        {
            RecursoConsumivel recurso = Session["RecursoEdit"] as RecursoConsumivel;
            if (recurso != null)
            {
                TextBoxName.Text = Server.HtmlDecode(recurso.Nome);
                TextBoxQuantity.Text = recurso.Quantidade.ToString();
                TextBoxMinVal.Text = recurso.ValorMinimo.ToString();
                DropDownCategory.SelectedIndex = DropDownCategory.Items.IndexOf(DropDownCategory.Items.FindByValue(recurso.Categoria));
            }
        }
        protected void FillCat()
        {
            DataTable categoria = new DAL().GetCategorias();
            DropDownCategory.DataSource = categoria;
            DropDownCategory.DataTextField = "Nome";
            DropDownCategory.DataValueField = "Nome";
            DropDownCategory.DataBind();
            DropDownCategory.Items.Insert(0, "Selecione a categoria");
        }
        protected void btnEntrada_Click(object sender, EventArgs e)
        {
            RecursoConsumivel recurso = Session["RecursoEdit"] as RecursoConsumivel;
            int ent = int.Parse(txtEntrada.Text);
            if (ent > 0)
            {
                bool entrada = new DAL().InsertEntrada(recurso.Id, ent);
                TextBoxQuantity.Text = entrada ? (int.Parse(TextBoxQuantity.Text) + ent).ToString() : TextBoxQuantity.Text;
                recurso.Quantidade = entrada ? int.Parse(TextBoxQuantity.Text) + ent : recurso.Quantidade;
                txtEntrada.Text = entrada ? "0" : txtEntrada.Text;
                Session["RecursoEdit"] = recurso;
            }
        }
        protected void btnSaida_Click(object sender, EventArgs e)
        {
            RecursoConsumivel recurso = Session["RecursoEdit"] as RecursoConsumivel;
            int said = int.Parse(txtSaida.Text);
            if (said > 0)
            {
                bool saida = (BLL.ValidarSaida(recurso.Quantidade, said)) ? new DAL().InsertSaida(recurso, said) : false;
                TextBoxQuantity.Text = saida ? (int.Parse(TextBoxQuantity.Text) - said).ToString() : TextBoxQuantity.Text;
                recurso.Quantidade = saida ? int.Parse(TextBoxQuantity.Text) - said : recurso.Quantidade;
                txtSaida.Text = saida ? "0" : txtSaida.Text;
                Session["RecursoEdit"] = recurso;
            }
        }
    }
}