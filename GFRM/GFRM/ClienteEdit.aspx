﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dashboard.Master" AutoEventWireup="true" CodeBehind="ClienteEdit.aspx.cs" Inherits="GFRM.ClienteEdit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="Recursos/estilo.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid" style="background-color:#f8f9fa;height:100%;">
        <div class="row">
            <div class="col-1"></div>
            <div class="col-10">
                <div class="card">
                    <div class="card-body">
                        <asp:label ID="lblCardHead" runat="server" text="Editar Cliente"></asp:label>
                    </div>
                </div>
            </div>
            <div class="col-1"></div>
        </div>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-10">
                <div class="card">
                    <div class="card-body">
                        <div style="color: black; font-size: 20px">
                            <div class="col-md-12 row">
                                <p class="col-md-4" > Entidade:</p> 
                                <p class="col-md-8"><asp:TextBox ID="TextBoxEntity" runat="server" CssClass="form-control"></asp:TextBox></p>
                            </div>
                            <div class="col-md-12 row">
                                <p class="col-md-4" > Nome Completo:</p> 
                                <p class="col-md-8"><asp:TextBox ID="TextBoxName" runat="server" CssClass="form-control"></asp:TextBox></p>
                            </div>
                            <div class="col-md-12 row">
                                <p class="col-md-4" > Contacto:</p> 
                                <p class="col-md-8"><asp:TextBox ID="TextBoxContact" runat="server" CssClass="form-control"></asp:TextBox></p>
                            </div>
                            <div class="col-md-12 row">
                                <p class="col-md-4" > Morada:</p> 
                                <p class="col-md-8"><asp:TextBox ID="TextBoxAddress" runat="server" CssClass="form-control"></asp:TextBox></p>
                            </div>
                            <div class="col-md-12 row">
                                <div class="col-md-4" >
                                </div> 
                                <div class="col-md-8">
                                    <div class="float-right">
                                        <asp:Button ID="ButtonEdit" class="btn btn-primary btncheck" runat="server" ToolTip="Aceitar Alteração" OnClick="ButtonEdit_Click" OnClientClick="if ( ! editConfirm()) return false;"/>
                                        <asp:Button ID="ButtonCancel" class="btn btn-danger btncancel" runat="server" ToolTip="Cancelar" OnClick="ButtonCancel_Click"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-1"></div>
         </div>        
    </div>

    <script type="text/javascript">
        function editConfirm() {
            return confirm("Pretende mesmo editar este cliente?");
        }
    </script>
</asp:Content>
