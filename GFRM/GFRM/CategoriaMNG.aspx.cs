﻿using Class_Library;
using ClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GFRM
{
    public partial class CategoriaMNG : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilizador user = Session["User"] as Utilizador;
            if (user.Permissao != "Admin")
            {
                Response.Redirect("ErrorPage.aspx");
            }
            btnDelete.Visible = false;
            btnEdit.Visible = false;
            if (!Page.IsPostBack)
            {
                Grid();
            }
        }
        #region GridView
        protected void Grid()
        {
            DataTable dt = new DAL().GetCategorias();
            ViewState["myViewState"] = dt;
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridView1.Rows)
            {

                if (row.RowIndex == GridView1.SelectedIndex)
                {

                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                    txtCategoria.Text = row.Cells[0].Text;


                    txtCategoria.Visible = true;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                }
                else
                {

                    row.BackColor = ColorTranslator.FromHtml("#FFFFFF");
                    row.ToolTip = "Clique para selecionar.";
                }
            }

        }
        protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(GridView1, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = "Clique para selecionar.";
            }
        }
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            Grid();
        }
        #endregion
        #region buttons
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            DAL dal = new DAL();
            foreach (GridViewRow row in GridView1.Rows)
            {
                if (row.RowIndex == GridView1.SelectedIndex)
                {
                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                    bool deleted = dal.DeleteCategoria(row.Cells[0].Text);
                    if (deleted) Grid();
                    txtCategoria.Text = "";
                    txtCategoria.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                }
            }
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridView1.Rows)
            {
                if (row.RowIndex == GridView1.SelectedIndex)
                {
                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                    string categoria = row.Cells[0].Text;
                    bool updated = new DAL().UpdateCategoria(categoria, txtCategoria.Text);
                    if (updated) Grid();
                    txtCategoria.Text = "";
                    txtCategoria.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                }
            }
        }
        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("home.aspx");
        }
        protected void btnAdicionar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtCategoria.Text))
            {
                if (BLL.ValidarInserirCategoria(txtCategoria.Text))
                {
                    bool inserted = new DAL().InsertCategoria(txtCategoria.Text);
                    txtCategoria.Text = inserted ? "" : txtCategoria.Text;

                    if (inserted)
                        Grid();

                    txtCategoria.Text = "";
                    btnCheck.Visible = false;
                    txtCategoria.Visible = false;
                }
            }
        }
        #endregion
        protected void enableBtnAndBox_Click(object sender, EventArgs e)
        {
            btnCheck.Visible = true;
            txtCategoria.Visible = true;
            btnDelete.Visible = false;
            btnEdit.Visible = false;
            txtCategoria.Text = "";
        }
    }
}