<%@ Page Title="" Language="C#" MasterPageFile="~/dashboard.Master" AutoEventWireup="true" CodeBehind="RecursoEditConsume.aspx.cs" Inherits="GFRM.RecursoEditConsume" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    <style>
        .formfont {
            color: black !important;
            font-size: 20px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="sciptManager" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updPanel" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10">
                        <div class="card">
                            <div class="card-body">
                                <asp:Label ID="lblCardHead" runat="server" Text="Editar Recurso Consum�vel"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="col-1"></div>
                </div>
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10">
                        <div class="card">
                            <div class="card-body">
                                <div style="color: black; font-size: 20px">
                                    <div class="col-md-12 row">
                                        <p class="col-md-4">Nome:</p>
                                        <p class="col-md-8">
                                            <asp:TextBox ID="TextBoxName" runat="server" Style="width: 100%;"></asp:TextBox>
                                        </p>
                                    </div>
                                    <div class="col-md-12 row">
                                        <p class="col-md-4">Categoria:</p>
                                        <p class="col-md-8">
                                            <asp:DropDownList ID="DropDownCategory" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </p>
                                    </div>
                                    <div class="col-md-12 row">
                                        <p class="col-md-4">Stock:</p>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="TextBoxQuantity" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-12 row">
                                        <p class="col-md-4">Valor Minimo:</p>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="TextBoxMinVal" runat="server" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-12 row">
                                        <div class="col-md-4">
                                        </div>
                                        <div class="col-md-8">
                                            <div class="float-right">
                                                <asp:Button ID="ButtonEdit" class="btn btn-primary btncheck" runat="server" ToolTip="Aceitar Altera��o" OnClick="ButtonEdit_Click" />
                                                <asp:Button ID="ButtonCancel" class="btn btn-danger btncancel" runat="server" ToolTip="Cancelar" OnClick="ButtonCancel_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <section class="card">
                            <section class="card-body">
                                <div class="col-md-12 row">
                                    <p class="col-md-4 formfont">Adicionar:</p>
                                    <div class="col-md-3">
                                        <asp:TextBox ID="txtEntrada" TextMode="Number" min="0" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                    </div>
                                    <div class="col-md-1 float-right">
                                        <asp:Button runat="server" ID="btnEntrada" class="btn btn-primary float-right" OnClick="btnEntrada_Click" Text="+" />
                                    </div>
                                </div>
                            </section>
                        </section>
                        <section class="card">
                            <section class="card-body">
                                <div class="col-md-12 row">
                                    <p class="col-md-4 formfont">Retirar:</p>
                                    <div class="col-md-3">
                                        <asp:TextBox ID="txtSaida" TextMode="Number" min="0" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                    </div>
                                    <div class="col-md-1 float-right">
                                        <asp:Button ID="btnSaida" runat="server" class="btn btn-primary float-right" OnClick="btnSaida_Click" Text="-" />
                                    </div>
                                </div>
                            </section>
                        </section>
                    </div>
                    <div class="col-1"></div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnEntrada" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSaida" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript">
        function editConfirm() {
            return confirm("Pretende mesmo editar este recurso?");
        }
    </script>
</asp:Content>
