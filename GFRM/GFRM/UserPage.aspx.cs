﻿using Class_Library;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GFRM
{
    public partial class UserPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CalendarAvailability.VisibleMonthChanged += new MonthChangedEventHandler(CalendarAvailability_VisibleMonthChanged);
            if (!IsPostBack)
            {
                Utilizador utilizador = Session["User"] as Utilizador;

                DAL dal = new DAL();
                List<Disponibilidade> disponibilidadesDiaformacao = dal.SelectIndisponibilidadeUtilizadorDiasFormacao(utilizador);
                List<Disponibilidade> disponibilidadesNotDiaformacao = dal.SelectIndisponibilidadeUtilizadorNotDiasFormacao(utilizador);
                Session["DiasFormacaoUser"] = disponibilidadesDiaformacao;
                Session["SickDays"] = disponibilidadesNotDiaformacao;
                Session["DisponibilidadesUser"] = new DAL().SelectIndisponibilidadeUtilizador(utilizador);
                TextBoxName.Text = utilizador.Nome;
                TextBoxUsername.Text = utilizador.Username;
                TextBoxEmail.Text = utilizador.Email;
                if (utilizador.Password == utilizador.FirstPassword)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(),
                        "alert",
                        "alert('Por favor modifique a sua password');",
                        true);
                }
            }
        }
        protected void CalendarAvailability_DayRender(object sender, DayRenderEventArgs e)
        {
            try
            {
                List<Disponibilidade> disponibilidadesDiaformacao = Session["DiasFormacaoUser"] as List<Disponibilidade>;
                List<Disponibilidade> disponibilidadesNotDiaformacao = Session["SickDays"] as List<Disponibilidade>;
                if (disponibilidadesDiaformacao.Count != 0)
                {
                    foreach (Disponibilidade disponibilidade in disponibilidadesDiaformacao)
                    {
                        if (disponibilidade.Data == e.Day.Date && e.Day.Date >= DateTime.Today)
                        {
                            e.Cell.ForeColor = Color.Red;
                            e.Day.IsSelectable = false;
                        }
                    }
                }
                if (disponibilidadesNotDiaformacao.Count != 0)
                {
                    foreach (Disponibilidade disponibilidade1 in disponibilidadesNotDiaformacao)
                    {
                        if (disponibilidade1.Data == e.Day.Date && e.Day.Date >= DateTime.Today)
                        {
                            e.Cell.ForeColor = Color.Blue;
                            e.Day.IsSelectable = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Erro
            }
            e.Cell.BackColor = e.Day.Date < DateTime.Today || e.Day.IsWeekend ? Color.LightGray : e.Cell.BackColor;
            e.Day.IsSelectable = e.Day.Date < DateTime.Today || e.Day.IsWeekend ? false : e.Day.IsSelectable;
        }
        protected void btnDisponibilidade_Click(object sender, EventArgs e)
        {
            DAL dal = new DAL();
            Utilizador user = Session["User"] as Utilizador;
            dal.ExecDisponibilidadesPessoais(user, CalendarAvailability.SelectedDate.Date);
            Session["DiasFormacaoUser"] = dal.SelectIndisponibilidadeUtilizadorDiasFormacao(user);
            Session["SickDays"] = dal.SelectIndisponibilidadeUtilizadorNotDiasFormacao(user);
            Session["DisponibilidadesUser"] = new DAL().SelectIndisponibilidadeUtilizador(user);
        }
        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtPwAtual.Text) && !string.IsNullOrEmpty(txtPwNova.Text) && !string.IsNullOrEmpty(txtPwConfirmar.Text))
            {
                Utilizador utilizador = Session["User"] as Utilizador;
                string currentPW = Class_Library.Auxiliar.EncryptPass(txtPwAtual.Text);
                if (currentPW == utilizador.Password)
                {
                    if (txtPwNova.Text == txtPwConfirmar.Text)
                    {
                        if (Auxiliar.ValidatePassword(txtPwNova.Text))
                        {
                            new DAL().UpdateUtilizadorPassword(utilizador, txtPwNova.Text);
                            ScriptManager.RegisterStartupScript(this, this.GetType(),
                            "alert",
                            "alert('Password modificada com sucesso!');window.location ='UserPage.aspx';",
                            true);
                        }
                        else
                        {
                            lblFail.Text = "Password deve ter no mínimo 8 caracteres 1 algarismo e 1 letra";
                        }
                        txtPwNova.Text = "";
                        txtPwConfirmar.Text = "";
                        txtPwAtual.Text = "";
                        //lblFail.Text = "";
                    }
                    else
                    {
                        txtPwNova.Text = "";
                        txtPwConfirmar.Text = "";
                        txtPwAtual.Text = "";
                        lblFail.Text = "Novas Passwords não coincidem";
                    }
                }
                else
                {
                    lblFail.Text = "Password atual incorrecta";
                }
            }
            else
            {
                lblFail.Text = "existem campos por preencher";
            }
        }
        protected void CalendarAvailability_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
        {
            UpdatePanelDisponibilidade.Update();
        }
        protected void ButtonChangeEmail_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(TextBoxChangeEmail.Text))
            {
                Utilizador user = Session["User"] as Utilizador;
                if (Auxiliar.ValidateEmail(TextBoxChangeEmail.Text))
                {
                    user.Email = TextBoxChangeEmail.Text;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(),
                "alert",
                "alert('E-mail inválido');",
                true);
                }
                bool updated = new DAL().UpdateUtilizador(user);
                if (updated)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(),
                "alert",
                "alert('Email modificado com sucesso');window.location='UserPage.aspx';",
                true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(),
                "alert",
                "alert('Não foi possivel modificar o e-mail');",
                true);
                }
            }
        }
    }
}