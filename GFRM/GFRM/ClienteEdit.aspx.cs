﻿using Class_Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GFRM
{
    public partial class ClienteEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Utilizador user = Session["User"] as Utilizador;
                if (user.Permissao != "Admin" && user.Permissao != "Coordenador")
                {
                    Response.Redirect("ErrorPage.aspx");
                }
                FillFields();
            }
        }
        protected void ButtonEdit_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(TextBoxAddress.Text) && !string.IsNullOrEmpty(TextBoxContact.Text) &&
                !string.IsNullOrEmpty(TextBoxName.Text) && !string.IsNullOrEmpty(TextBoxAddress.Text))
            {
                Cliente cliente = Session["ClientEdit"] as Cliente;
                string entidade = cliente.Entidade;
                cliente.Entidade = TextBoxEntity.Text;
                cliente.Contacto = TextBoxContact.Text;
                cliente.Morada = TextBoxAddress.Text;
                cliente.Nome = TextBoxName.Text;
                bool updated = new DAL().UpdateCliente(cliente, entidade);
                if (updated)
                {
                    Session["ClientEdit"] = null;
                    Response.Redirect("ClienteMNG.aspx");
                }
            }
        }
        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ClienteMNG.aspx");
        }
        protected void FillFields()
        {
            Cliente cliente = Session["ClientEdit"] as Cliente;
            if (cliente != null)
            {
                TextBoxAddress.Text = cliente.Morada;
                TextBoxContact.Text = cliente.Contacto;
                TextBoxEntity.Text = cliente.Entidade;
                TextBoxName.Text = cliente.Nome;
            }
        }
    }
}