﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dashboard.Master" AutoEventWireup="true" CodeBehind="FormacaoEdit.aspx.cs" Inherits="GFRM.FormacaoEdit" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="scrpMng" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="updPanel" ChildrenAsTriggers="false" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <main class="courseGrid">
                <section class="title-grid-item">
                    <h1 class="text-center">
                        <asp:Label ID="lblCardHead" runat="server" Text="Editar Formação" Style="font-weight: 400; font-size: 24px;"></asp:Label>
                    </h1>
                    <hr>
                </section>
                <section class="subtitleCourseData-grid-item">
                    <h3 style="font-weight: 400; font-size: 20px; margin-top: 16px;">Dados Formação</h3>
                    <hr>
                </section>
                <section class="subtitleDayData-grid-item">
                    <h3 style="font-weight: 400; font-size: 20px; margin-top: 16px;">Dias Formação</h3>
                    <hr>
                </section>
                <section class="subtitleDays-grid-item">
                    <h3 style="font-weight: 400; font-size: 20px; margin-top: 16px;">Dias Formação Marcados</h3>
                    <hr>
                </section>
                <section class="courseData-grid-item gridcard">
                    <div class="card-body">
                        <div class="form-group row">
                            <asp:Label ID="lblNome" CssClass="col-sm-12" runat="server" Text="Nome"></asp:Label>
                            <asp:TextBox ID="txtNome" runat="server" CssClass="form-control col-sm-12"></asp:TextBox>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lblCliente" runat="server" CssClass="col-sm-12" Text="Cliente"></asp:Label>
                            <asp:DropDownList ID="ddlCliente" runat="server" CssClass="form-control col-sm-12" AutoPostBack="true" OnSelectedIndexChanged="ddlCliente_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lblCategoria" CssClass="col-sm-12" runat="server" Text="Categoria"></asp:Label>
                            <asp:DropDownList ID="ddlCategoria" runat="server" CssClass="form-control col-sm-12" AutoPostBack="true" OnSelectedIndexChanged="ddlCategoria_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lblFormador" runat="server" CssClass="col-sm-12" Text="Formador"></asp:Label>
                            <asp:DropDownList ID="ddlFormador" AutoPostBack="true" OnSelectedIndexChanged="ddlFormador_SelectedIndexChanged" runat="server" CssClass="form-control col-sm-12"></asp:DropDownList>
                        </div>
                        <div class="form-group row">
                            <asp:Label ID="lblLocalFormacao" CssClass="col-sm-12" runat="server" Visible="false">Local Formação</asp:Label>
                            <asp:RadioButtonList ID="rbLocalFormacao" Visible="true" CssClass="col-sm-12 form-control-range" runat="server">
                                <asp:ListItem Enabled="true">Cliente</asp:ListItem>
                                <asp:ListItem>Atec</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                </section>
                <section class="courseDaydata-grid-item gridcard">
                    <div class="card-body">
                        <div class="form-group">
                            <asp:Calendar ID="CalendarAvailability" SelectedDayStyle-BackColor="Black"
                                OnDayRender="CalendarAvailability_DayRender" OnSelectionChanged="CalendarAvailability_SelectionChanged"
                                runat="server" WeekendDayStyle-BackColor="OrangeRed" WeekendDayStyle-ForeColor="Black"
                                FirstDayOfWeek="Monday" CssClass="calendary"
                                OnVisibleMonthChanged="CalendarAvailability_VisibleMonthChanged"></asp:Calendar>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="LabelDuration" runat="server" Text="Duração"></asp:Label>
                            <asp:TextBox ID="TextBoxDuration" runat="server" TextMode="Number" min="0" max="24" Text="8" placeholder="Duracao" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="LabelStart" runat="server" Text="Hora de Início"></asp:Label>
                            <asp:TextBox ID="TextBoxStartTime" runat="server" TextMode="Time" value="09:00" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="LabelFinish" runat="server" Text="Hora de Fim"></asp:Label>
                            <asp:TextBox ID="TextBoxEndTime" runat="server" TextMode="Time" value="18:00" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </section>
                <section class="courseDays-grid-item gridcard">
                    <div class="card-body form-group">
                        <asp:Label ID="lblDatas" runat="server" Text="Dias Formação"></asp:Label>
                        <asp:ListBox ID="lbDatas" runat="server" AutoPostBack="true" Style="min-width: 100%; min-height: 250px;" OnSelectedIndexChanged="lbDatas_SelectedIndexChanged"></asp:ListBox>
                        <asp:Button ID="btnRemove" Visible="false" runat="server" CssClass="btn btn-danger btndelete" OnClick="btnRemove_Click" ToolTip="Remover" />
                    </div>
                    <div class="card-body form-group">
                        <asp:Label ID="LabelValue" runat="server" Text="Valor/Hora pago ao formador"></asp:Label>
                        <asp:TextBox ID="TextBoxValue" runat="server" placeholder=" Em Euros(€)" CssClass="form-control"></asp:TextBox>
                    </div>
                </section>
                <section class="submitCancel-grid-item">
                    <asp:Button Text="Editar Recursos" CssClass="btn btn-primary" ID="btnUpdate" OnClick="btnUpdate_Click" runat="server" />
                    <asp:Button ToolTip="Cancelar" CssClass=" btn btn-danger btncancel" ID="btnCancel" OnClick="btnCancel_Click" OnClientClick="" runat="server" />
                </section>
            </main>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlCliente" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlCategoria" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlCliente" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlFormador" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="CalendarAvailability" EventName="SelectionChanged" />
            <asp:AsyncPostBackTrigger ControlID="lbDatas" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="btnRemove" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
