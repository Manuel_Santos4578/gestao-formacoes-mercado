﻿using Class_Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GFRM
{
    public partial class NewMasterPage : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilizador user = Session["User"] as Utilizador ?? new DAL().SelectUtilizador("Admin");
            Session["User"] = user;
            //Session.Timeout = 240;
            Ut.Text = user.Nome;
            if (user != null)
            {
                switch (user.Permissao)
                {
                    case "Formador":
                        FindControl("Recursos").Visible = false;
                        FindControl("Formacoes").Visible = false;
                        FindControl("Utilizadores").Visible = false;
                        FindControl("Clientes").Visible = false;
                        FindControl("Label1").Visible = true;
                        FindControl("Estatisticas").Visible = false;
                        FindControl("Categorias").Visible = false;
                        FindControl("bell").Visible = false;
                        break;
                    case "Coordenador":
                        FindControl("Recursos").Visible = false;
                        FindControl("Formacoes").Visible = true;
                        FindControl("Utilizadores").Visible = true;
                        FindControl("Clientes").Visible = true;
                        FindControl("Label1").Visible = false;
                        FindControl("Estatisticas").Visible = false;
                        FindControl("Categorias").Visible = false;
                        FindControl("bell").Visible = false;
                        break;
                    case "Worker":
                        FindControl("Recursos").Visible = true;
                        FindControl("Formacoes").Visible = false;
                        FindControl("Utilizadores").Visible = false;
                        FindControl("Clientes").Visible = false;
                        FindControl("Label1").Visible = false;
                        FindControl("Estatisticas").Visible = false;
                        FindControl("Categorias").Visible = true;
                        FindControl("bell").Visible = false;
                        break;
                    default:
                        int count = 0;
                        List<RecursoConsumivel> recursoConsumivels = new DAL().SelectRecursosConsumiveis();
                        foreach (RecursoConsumivel recursoConsumivel in recursoConsumivels)
                        {
                            if (recursoConsumivel.Quantidade <= recursoConsumivel.ValorMinimo)
                            {
                                lblAlertResource.Text += recursoConsumivel.Nome + " Atingiu o limite mínimo!<br/>";
                                count++;
                            }
                        }
                        if (count != 0)
                        {
                            FindControl("bell").Visible = true;
                        }
                        else
                        {
                            FindControl("bell").Visible = false;
                        }
                        FindControl("Recursos").Visible = true;
                        FindControl("Formacoes").Visible = true;
                        FindControl("Utilizadores").Visible = true;
                        FindControl("Clientes").Visible = true;
                        FindControl("Label1").Visible = false;
                        FindControl("Estatisticas").Visible = true;
                        FindControl("Categorias").Visible = true;
                        break;
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
        protected void LogoutButton(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("Login.aspx");
        }
        protected void bell_Click(object sender, EventArgs e)
        {


        }
    }
}