﻿using Class_Library;
using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GFRM
{
    public partial class Formacoes : System.Web.UI.Page
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["conString"].ConnectionString);
        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilizador userlogged = Session["User"] as Utilizador;
            if (userlogged.Permissao != "Admin" && userlogged.Permissao != "Coordenador")
            {
                Response.Redirect("ErrorPage.aspx");
            }
            if (!Page.IsPostBack)
            {
                ddlEstados();
                ddlFormadores();
                ddlClientes();
                ddlEstado.SelectedIndex = ddlEstado.Items.IndexOf(ddlEstado.Items.FindByText("A decorrer"));
                Grid();
            }
        }
        protected void btnAdicionar_Click(object sender, EventArgs e)
        {
            Response.Redirect("FormacaoAdd.aspx");
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gvFormacoes.Rows)
            {
                if (row.RowIndex == gvFormacoes.SelectedIndex)
                {
                    Formacao formacao = new DAL().SelectFormacao(int.Parse(row.Cells[0].Text));
                    Session["FormacaoDetails"] = formacao;
                    Response.Redirect("FormacaoDetails.aspx");
                }
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gvFormacoes.Rows)
            {
                if (row.RowIndex == gvFormacoes.SelectedIndex)
                {
                    Formacao formacao = new DAL().SelectFormacao(int.Parse(row.Cells[0].Text));
                    Session["FormacaoEdit"] = formacao;
                    switch (formacao.Estado)
                    {
                        case "Terminada":
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert",
                            "alert('Formação terminou e não pode ser editada!');", true);
                            break;
                        case "Por iniciar":
                            Response.Redirect("FormacaoEdit.aspx");
                            break;
                        case "A decorrer":
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert",
                            "alert('FormacaoEditRecursos.aspx');", true);
                            break;
                        default:
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert",
                            "alert('Ocorreu um erro');", true);
                            break;
                    }
                }
            }
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            DAL dal = new DAL();
            foreach (GridViewRow row in gvFormacoes.Rows)
            {
                if (row.RowIndex == gvFormacoes.SelectedIndex)
                {
                    Formacao formacao = new Formacao
                    {
                        Id = int.Parse(row.Cells[0].Text)
                    };
                    bool deleted = dal.DeleteFormacao(formacao);
                    if (deleted) Grid();
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnUpdate.Visible = false;
                }
            }
        }
        protected void Grid()
        {
            List<Formacao> ListFormacoes = new List<Formacao>();
            switch (ddlCliente.SelectedIndex)
            {
                case 0:
                    switch (ddlFormador.SelectedIndex)
                    {
                        case 0:
                            ListFormacoes = new DAL().SelectFormacoes();
                            break;
                        default:
                            ListFormacoes = new DAL().SelectFormacoes(new Utilizador { Username = ddlFormador.SelectedItem.Value });
                            break;
                    }
                    break;
                default:
                    switch (ddlFormador.SelectedIndex)
                    {
                        case 0:
                            ListFormacoes = new DAL().SelectFormacoes(ddlCliente.SelectedItem.Value);
                            break;
                        default:
                            ListFormacoes = new DAL().SelectFormacoes(ddlCliente.SelectedItem.Value, new Utilizador { Username = ddlFormador.SelectedItem.Value });
                            break;
                    }
                    break;
            }
            List<Formacao> newList = new List<Formacao>();
            switch (ddlEstado.SelectedItem.Text)
            {
                case "Terminada":
                    foreach (Formacao formacao in ListFormacoes)
                    {
                        if (formacao.Estado == ddlEstado.SelectedItem.Text) newList.Add(formacao);
                    }
                    break;
                case "A decorrer":
                    foreach (Formacao formacao in ListFormacoes)
                    {
                        if (formacao.Estado == ddlEstado.SelectedItem.Text) newList.Add(formacao);
                    }
                    break;
                case "Por iniciar":
                    foreach (Formacao formacao in ListFormacoes)
                    {
                        if (formacao.Estado == ddlEstado.SelectedItem.Text) newList.Add(formacao);
                    }
                    break;
                default:
                    newList = ListFormacoes;
                    break;
            }
            gvFormacoes.DataSource = newList;
            gvFormacoes.DataBind();
        }
        protected void gvFormacoes_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvFormacoes.PageIndex = e.NewPageIndex;
            Grid();
        }
        protected void gvFormacoes_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gvFormacoes.Rows)
            {
                if (row.RowIndex == gvFormacoes.SelectedIndex)
                {
                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                    btnUpdate.Visible = true;
                }
                else
                {
                    row.BackColor = ColorTranslator.FromHtml("#FFFFFF");
                    row.ToolTip = "Clique para selecionar.";
                }
            }
        }
        protected void gvFormacoes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(gvFormacoes, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = "Clique para selecionar.";
            }
            try
            {
                if (e.Row.Cells[1].Text == "A decorrer")
                {
                    e.Row.Cells[1].BackColor = Color.LightGreen;
                }
                else if (e.Row.Cells[1].Text == "Por iniciar")
                {
                    e.Row.Cells[1].BackColor = Color.LightCyan;
                }
                else if (e.Row.Cells[1].Text == "Terminada")
                {
                    e.Row.Cells[1].BackColor = Color.Coral;
                }
            }
            catch (Exception ex)
            {

            }
        }
        protected void reset_Click(object sender, EventArgs e)
        {
            ddlCliente.SelectedIndex = 0;
            ddlEstado.SelectedIndex = 0;
            ddlFormador.SelectedIndex = 0;
            Grid();
        }
        // added a button with ID=btnDownload and double clicked it's onclick event to auto create method
        protected void btnDownload_Click(object sender, EventArgs e)
        {
            DAL dal = new DAL();
            string entidade = ddlCliente.SelectedIndex != 0 ? ddlCliente.SelectedValue : "";
            Utilizador formador = new Utilizador
            {
                Username = ddlFormador.SelectedIndex != 0 ? ddlFormador.SelectedValue : "",
                Nome = ddlFormador.SelectedIndex != 0 ? ddlFormador.SelectedItem.Text : ""
            };
            dt = dal.GetFormacoesForExcel();
            ExportTableData(dt);
        }
        // this does all the work to export to excel
        public void ExportTableData(DataTable dtdata)
        {
            XLWorkbook wb = new XLWorkbook();
            {
                wb.Worksheets.Add(dt, "Detalhes Formação");
                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename=Detalhes Formações.xlsx");
                MemoryStream MyMemoryStream = new MemoryStream();
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }
        protected void ddlCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            Grid();
        }
        protected void ddlFormador_SelectedIndexChanged(object sender, EventArgs e)
        {
            Grid();
        }
        protected void ddlEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            Grid();
        }
        protected void ddlClientes()
        {
            try
            {
                ddlCliente.DataSource = new DAL().SelectClientes();
                ddlCliente.DataTextField = "Nome";
                ddlCliente.DataValueField = "Entidade";
                ddlCliente.DataBind();
                ListItem item = new ListItem("Selecione o Cliente", "");
                ddlCliente.Items.Insert(0, item);
                ddlCliente.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                // Erro
            }
        }
        protected void ddlFormadores()
        {
            try
            {
                ddlFormador.DataSource = new DAL().SelectUtilizadores("", "", "", "Worker");
                ddlFormador.DataTextField = "Nome";
                ddlFormador.DataValueField = "Username";
                ddlFormador.DataBind();
                ListItem item = new ListItem("Selecione o Formador", "%");
                ddlFormador.Items.Insert(0, item);
                ddlFormador.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                // Erro
            }
        }
        protected void ddlEstados()
        {
            try
            {
                ListItem item = new ListItem("Selecione o Estado", "%");
                ddlEstado.Items.Insert(0, item);
                ddlEstado.SelectedIndex = 0;
                ddlEstado.Items.Insert(1, new ListItem("Terminada"));
                ddlEstado.Items.Insert(2, new ListItem("A decorrer"));
                ddlEstado.Items.Insert(3, new ListItem("Por iniciar"));
            }
            catch (Exception ex)
            {
                // Erro
            }
        }

    }
}