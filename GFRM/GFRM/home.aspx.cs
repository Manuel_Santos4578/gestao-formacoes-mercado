﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClosedXML.Excel;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using ClassLibrary;
using Class_Library;
using System.Drawing;

namespace GRFM.Paginas
{
    public partial class home : System.Web.UI.Page
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["conString"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            //Utilizador user = Session["User"] as Utilizador;
            //if (user.Password == user.FirstPassword)
            //{
            //    Response.Redirect("UserPage.aspx");
            //}
            if (!IsPostBack)
            {
                ddlClientes();
                GridUser();
                warning();
            }
        }
        protected void warning()
        {
            Utilizador user = Session["User"] as Utilizador;
            if (user.Permissao == "Admin")
            {
                int count = 0;
                List<RecursoConsumivel> recursoConsumivels = new DAL().SelectRecursosConsumiveis();
                foreach (RecursoConsumivel recursoConsumivel in recursoConsumivels)
                {
                    if (recursoConsumivel.Quantidade <= recursoConsumivel.ValorMinimo)
                    {
                        count++;
                    }
                }
            }
        }
        protected void GridUser()
        {
            Utilizador user = Session["User"] as Utilizador;
            List<Formacao> ListFormacoes = new List<Formacao>();
            if (ddlCliente.SelectedIndex == 0)
            {
                ListFormacoes = new DAL().SelectFormacoes(user);
            }
            else
            {
                ListFormacoes = new DAL().SelectFormacoes(ddlCliente.SelectedItem.Value, user);
            }
            List<Formacao> newList = new List<Formacao>();

            foreach (Formacao formacao in ListFormacoes)
            {
                if (formacao.Estado != "Terminada") newList.Add(formacao);
            }
            gvHome.DataSource = newList;
            gvHome.DataBind();
        }
        protected void btnDownload_Click(object sender, EventArgs e)
        {
            Utilizador user = Session["User"] as Utilizador;
            DataTable dt = new DAL().GetFormacoesForExcel(user);
            ExportTableData(dt);
        }
        public void ExportTableData(DataTable dtdata)
        {
            XLWorkbook wb = new XLWorkbook();
            {
                wb.Worksheets.Add(dtdata, "Formação");
                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename=Listagem Formações.xlsx");
                MemoryStream MyMemoryStream = new MemoryStream();
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }
        protected void gvHome_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvHome.PageIndex = e.NewPageIndex;
            GridUser();
        }
        protected void gvHome_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gvHome.Rows)
            {
                if (row.RowIndex == gvHome.SelectedIndex)
                {
                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                    btnEdit.Visible = true;
                }
                else
                {
                    row.BackColor = ColorTranslator.FromHtml("#FFFFFF");
                    row.ToolTip = "Clique para selecionar.";
                }
            }
        }
        protected void gvHome_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(gvHome, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = "Clique para selecionar.";
            }
            try
            {
                if (e.Row.Cells[1].Text == "A decorrer")
                {
                    e.Row.Cells[1].BackColor = Color.LightGreen;
                }
                else if (e.Row.Cells[1].Text == "Por iniciar")
                {
                    e.Row.Cells[1].BackColor = Color.LightCyan;
                }
                else if (e.Row.Cells[1].Text == "Terminada")
                {
                    e.Row.Cells[1].BackColor = Color.Coral;
                }
            }
            catch (Exception ex)
            {

            }
        }
        protected void reset_Click(object sender, EventArgs e)
        {
            ddlCliente.SelectedIndex = 0;
            GridUser();
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gvHome.Rows)
            {
                if (row.RowIndex == gvHome.SelectedIndex)
                {
                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;

                    Formacao formacao = new DAL().SelectFormacao(int.Parse(row.Cells[0].Text));
                    Session["FormacaoDetails"] = formacao;
                    Response.Redirect("FormacaoDetails.aspx");
                }
            }

        }
        protected void ddlClientes()
        {
            try
            {
                ddlCliente.DataSource = new DAL().SelectClientes();
                ddlCliente.DataTextField = "Nome";
                ddlCliente.DataValueField = "Entidade";
                ddlCliente.DataBind();
                ListItem item = new ListItem("Selecione o Cliente", "%");
                ddlCliente.Items.Insert(0, item);
                ddlCliente.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                // Erro
            }

        }
        protected void ddlCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridUser();
        }
    }
}