﻿using Class_Library;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;

namespace GFRM
{
    public partial class Statistics : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilizador user = Session["User"] as Utilizador;
            if (user.Permissao != "Admin")
            {
                Response.Redirect("ErrorPage.aspx");
            }
        }
        //public void mostraEstatisticas()
        //{
        //    ChartConsumiveis.Series[0].ChartType = SeriesChartType.Bar;
        //    ChartConsumiveis.ChartAreas[0].AxisX.LabelStyle.Interval = 1;
        //    ChartConsumiveis.ChartAreas[0].AxisX.IsLabelAutoFit = true;
        //    ChartConsumiveis.ChartAreas[0].AxisX.ScaleView.Size = 2;
        //    ChartAquisicao.ChartAreas[0].AxisX.LabelStyle.Interval = 1;
        //    ChartAquisicao.Series[0].ChartType = SeriesChartType.Bar;

        //    Dictionary<string, int> saidasMes = lerSaidasMes();
        //    foreach (KeyValuePair<string, int> saida in saidasMes)
        //    {
        //        ChartConsumiveis.Series[0].Points.AddXY($"x:{saida.Key}",$" y:{saida.Value}");
        //        //LabelSaidasResultado.Text += saida.Key + ": " + saida.Value + "<br/>";
        //    }
        //    Dictionary<string,int> entradasMes = lerEntradasMes();
        //    foreach (KeyValuePair<string, int> entrada in entradasMes)
        //    {
        //        ChartAquisicao.Series[0].Points.AddXY(entrada.Key, entrada.Value);
        //        //LabelEntradasResultado.Text += entrada.Key + ": " + entrada.Value + "<br/>";
        //    }
        //}
        protected Dictionary<string, int> lerSaidasMes()
        {
            return new DAL().saidasMes();
        }
        public string Grafico()
        {
            Dictionary<string, int> saidasMes = lerSaidasMes();
            foreach (KeyValuePair<string, int> saida in saidasMes)
            {
                lblHidden.Text += "{" + $"'label': '{saida.Key}',y: {saida.Value}" + "},";
            }
            lblHidden.Text = (lblHidden.Text.Length > 1) ? lblHidden.Text.Remove(lblHidden.Text.Length - 1, 1) : lblHidden.Text;
            return lblHidden.Text;

        }
        public string Grafico2()
        {
            Dictionary<string, int> entradasMes = lerEntradasMes();
            foreach (KeyValuePair<string, int> entrada in entradasMes)
            {
                lblHidden2.Text += "{" + $"'label': '{entrada.Key}',y: {entrada.Value}" + "},";
            }
            lblHidden2.Text = (lblHidden2.Text.Length > 1) ? lblHidden2.Text.Remove(lblHidden2.Text.Length - 1, 1) : lblHidden2.Text;
            return lblHidden2.Text;

        }
        protected Dictionary<string, int> lerEntradasMes()
        {
            return new DAL().entradasMes();
        }

        protected Dictionary<DateTime, int> LoginsMes(DateTime? date=null)
        {
            return new DAL().LoginsMes(date);
        }
        protected string Grafico3(DateTime? date=null)
        {
            Dictionary<DateTime, int> loginsMes = LoginsMes(date);
            foreach (KeyValuePair<DateTime, int> login in loginsMes)
            {
                if (login.Key.Year != 0)
                    lblHidden3.Text += "{" + $"'label': '{login.Key.Month}/{login.Key.Year}',y: {login.Value}" + "},";
                else
                    lblHidden3.Text += "{" + $"'label': '{login.Key.Month}',y: {login.Value}" + "},";
            }
            lblHidden3.Text = (lblHidden3.Text.Length > 1) ? lblHidden3.Text.Remove(lblHidden3.Text.Length - 1, 1) : lblHidden3.Text;
            return lblHidden3.Text;
        }
    }
}