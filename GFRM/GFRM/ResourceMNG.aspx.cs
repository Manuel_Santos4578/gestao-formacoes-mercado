﻿using Class_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GFRM
{
    public partial class ResourceMNG : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilizador user = Session["User"] as Utilizador;
            if (user.Permissao != "Admin" && user.Permissao != "Worker")
            {
                Response.Redirect("ErrorPage.aspx");
            }
            btnEdit.Visible = false;
            btnDelete.Visible = false;
            if (!IsPostBack)
            {
                //searchBox.Attributes.Add("onKeyPress",
                //   "doClick('" + search.ClientID + "',event)");


                string comeback = (string)Session["comeback"];
                if (comeback == "NoConsume")
                {
                    rb.SelectedValue = "NaoConsumivel";
                }
                else
                {
                    rb.SelectedValue = "Consumivel";
                }


                DropDownCategoria();
                Grid();

            }


        }
        public void DropDownCategoria()
        {
            try
            {
                ddlCat.DataSource = new DAL().GetCategorias();
                ddlCat.DataTextField = "Nome";
                ddlCat.DataValueField = "Nome";
                ddlCat.DataBind();
                ListItem item = new ListItem("Selecione a categoria", "%");
                ddlCat.Items.Insert(0, item);
                ddlCat.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                // Erro
            }
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), $"SetKeyPressEvent('{searchBox.ClientID}','{search.ClientID}', 13, 'keypress',true);", false);
        }

        protected void btnAdicionar_Click(object sender, EventArgs e)
        {
            Response.Redirect("ResourceAdd.aspx");
        }

        #region Botões Search e Reset
        protected void searchButton_Click(object sender, EventArgs e)
        {
            string searchTerm = searchBox.Text.ToLower();

            //check if the search input is at least 3 chars
            if (searchTerm.Length >= 0)
            {
                //always check if the viewstate exists before using it
                if (ViewState["myViewState"] == null)
                    return;

                //cast the viewstate as a datatable
                DataTable dt = ViewState["myViewState"] as DataTable;

                //make a clone of the datatable
                DataTable dtNew = dt.Clone();

                //search the datatable for the correct fields
                foreach (DataRow row in dt.Rows)
                {
                    //add your own columns to be searched here
                    if (row["Nome"].ToString().ToLower().Contains(searchTerm)/* || row["Categoria"].ToString().ToLower().Contains(searchTerm)*/)
                    {
                        //when found copy the row to the cloned table
                        dtNew.Rows.Add(row.ItemArray);
                    }
                }
                //rebind the grid
                GridView1.DataSource = dtNew;
                GridView1.DataBind();


            }

            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), $"SetKeyPressEvent('{searchBox.ClientID}','{search.ClientID}', 13, 'keypress',true);", true);
        }

        protected void resetSearchButton_Click(object sender, EventArgs e)
        {
            //always check if the viewstate exists before using it
            if (ViewState["myViewState"] == null)
                return;

            //cast the viewstate as a datatable
            DataTable dt = ViewState["myViewState"] as DataTable;
            searchBox.Text = "";
            //rebind the grid
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        #endregion
        #region GridView
        protected void Grid()
        {
            if (ddlCat.SelectedIndex != 0 || !string.IsNullOrWhiteSpace(searchBox.Text))
            {
                GridView1.Columns[3].Visible = true;
                if (rb.SelectedValue == "Consumivel")
                {
                    DataTable dt = new DAL().GetRecursosConsumiveis(ddlCat.SelectedValue, searchBox.Text);

                    //save the datatable into a viewstate for later use
                    ViewState["myViewState"] = dt;

                    //bind the datasource to the gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    GridView1.Columns[3].Visible = false;
                    GridView1.Columns[4].Visible = false;
                    //to make sure the data isn't loaded in postback
                    //use a datatable for storing all the data
                    DataTable dt = new DAL().GetRecursosNaoConsumiveis(ddlCat.SelectedValue, searchBox.Text);

                    //save the datatable into a viewstate for later use
                    ViewState["myViewState"] = dt;

                    //bind the datasource to the gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
            }
            else
            {
                GridView1.Columns[3].Visible = true;
                GridView1.Columns[4].Visible = true;
                if (rb.SelectedValue == "Consumivel")
                {
                    DataTable dt = new DAL().GetRecursosConsumiveis();
                    //save the datatable into a viewstate for later use
                    ViewState["myViewState"] = dt;
                    //bind the datasource to the gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    GridView1.Columns[3].Visible = false;
                    GridView1.Columns[4].Visible = false;
                    //to make sure the data isn't loaded in postback
                    //use a datatable for storing all the data
                    DataTable dt = new DAL().GetRecursosNaoConsumiveis(ddlCat.SelectedValue, searchBox.Text);

                    //save the datatable into a viewstate for later use
                    ViewState["myViewState"] = dt;

                    //bind the datasource to the gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }

            }
        }
        #endregion   
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            Grid();
        }   //Paginação


        #region Selectable row
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {


            foreach (GridViewRow row in GridView1.Rows)
            {

                if (row.RowIndex == GridView1.SelectedIndex)
                {

                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                }
                else
                {

                    row.BackColor = ColorTranslator.FromHtml("#FFFFFF");
                    row.ToolTip = "Click to select this row.";
                }
            }
        }
        protected void OnRowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(GridView1, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = "Click to select this row.";
            }
        }
        #endregion

        protected void rb_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCat.SelectedIndex = 0;
            searchBox.Text = "";
            Grid();
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), $"SetKeyPressEvent('{searchBox.ClientID}','{search.ClientID}', 13, 'keypress',true);", true);
        }

        protected void btnShowPopup_Click(object sender, EventArgs e)
        {

        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridView1.Rows)
            {
                if (row.RowIndex == GridView1.SelectedIndex)
                {

                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                    if (rb.SelectedValue == "Consumivel")
                    {
                        RecursoConsumivel recursoConsumivel = new RecursoConsumivel
                        {
                            Id = int.Parse(row.Cells[0].Text),
                            Nome = row.Cells[1].Text,
                            Categoria = row.Cells[2].Text,
                            Quantidade = int.Parse(row.Cells[3].Text),
                            ValorMinimo = int.Parse(row.Cells[4].Text)
                        };
                        Session["RecursoEdit"] = recursoConsumivel;

                        Response.Redirect("RecursoEditConsume.aspx");
                    }
                    else
                    {
                        RecursoNaoConsumivel recursoNaoConsumivel = new RecursoNaoConsumivel
                        {
                            Id = int.Parse(row.Cells[0].Text),
                            Nome = row.Cells[1].Text,
                            Categoria = row.Cells[2].Text,
                        };
                        Session["RecursoEdit"] = recursoNaoConsumivel;
                        Session["DispononibilidadesNoConsume"] = new DAL().SelectIndisponibilidadeRecurso(recursoNaoConsumivel);
                        Response.Redirect("RecursoEditNoConsume.aspx");
                    }
                }
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            DAL dal = new DAL();
            foreach (GridViewRow row in GridView1.Rows)
            {
                if (row.RowIndex == GridView1.SelectedIndex)
                {

                    row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    row.ToolTip = string.Empty;
                    if (rb.SelectedValue == "Consumivel")
                    {
                        RecursoConsumivel recursoConsumivel = new RecursoConsumivel
                        {
                            Id = int.Parse(row.Cells[0].Text),
                            Nome = row.Cells[1].Text,
                            Categoria = row.Cells[2].Text,
                            Quantidade = int.Parse(row.Cells[3].Text)
                        };

                        bool deleted = dal.DeleteRecursoConsumivel(recursoConsumivel);
                        if (deleted)
                            Grid();
                    }
                    else
                    {
                        RecursoNaoConsumivel recursoNaoConsumivel = new RecursoNaoConsumivel
                        {
                            Id = int.Parse(row.Cells[0].Text),
                            Nome = row.Cells[1].Text,
                            Categoria = row.Cells[2].Text,
                        };

                        bool deleted = dal.DeleteRecursoNaoConsumivel(recursoNaoConsumivel);
                        if (deleted)
                            Grid();
                    }
                }
            }
        }

        protected void ddlCat_SelectedIndexChanged(object sender, EventArgs e)
        {
            Grid();
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), $"SetKeyPressEvent('{searchBox.ClientID}','{search.ClientID}', 13, 'keypress',true);", true);
        }
    }
}