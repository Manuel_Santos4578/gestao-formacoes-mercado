<%@ Page Title="" Language="C#" MasterPageFile="~/dashboard.Master" AutoEventWireup="true" CodeBehind="RecursoEditNoConsume.aspx.cs" Inherits="GFRM.RecursoEditNoConsume" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server" ID="scriptManager"></asp:ScriptManager>
    <asp:UpdatePanel runat="server" ChildrenAsTriggers="false" ID="updPanel" UpdateMode="Conditional">
        <ContentTemplate>
            <section class="container-fluid">
                <section class="row">
                    <section class="col-md-1"></section>
                    <section class="col-md-10">
                        <section class="card">
                            <section class="card-body">
                                <h4 class="text-center">
                                    <asp:Label ID="lblCardHead" runat="server" Text="Editar Recurso N�o Consum�vel"></asp:Label></h4>
                            </section>
                        </section>
                    </section>
                    <section class="col-md-1"></section>
                </section>
                <section class="row">
                    <section class="col-md-1"></section>
                    <section class="col-md-10 ">
                        <section class="card">
                            <section class="card-header">
                                <h4>Detalhes</h4>
                            </section>
                            <section class="card-body">
                                <section style="color: black; font-size: 20px">
                                    <section class="col-md-12 row">
                                        <p class="col-md-4">Nome:</p>
                                        <p class="col-md-8">
                                            <asp:TextBox ID="TextBoxName" runat="server" CssClass="form-control"></asp:TextBox>
                                        </p>
                                    </section>
                                    <section class="col-md-12 row">
                                        <p class="col-md-4">Categoria:</p>
                                        <p class="col-md-8">
                                            <asp:DropDownList ID="DropDownCategory" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </p>
                                    </section>
                                </section>
                            </section>
                            <section class="card-footer">
                                <section class=" float-right">
                                    <asp:Button ID="ButtonEdit" class="btn btn-primary btncheck" runat="server" ToolTip="Aceitar Altera��o" OnClick="ButtonEdit_Click" />
                                    <asp:Button ID="ButtonCancel" class="btn btn-danger btncancel" runat="server" ToolTip="Cancelar" OnClick="ButtonCancel_Click" />
                                </section>
                            </section>
                        </section>
                        <section class="card">
                            <section class="card-header">
                                <h4 class="text-center">Disponibilidades</h4>
                            </section>
                            <section class="card-body text-center">
                                <asp:Calendar ID="NoConsumeAvailabilityCalendar" runat="server" TitleStyle-ForeColor="Black" WeekendDayStyle-BackColor="LightGray" 
                                    Style="margin:auto" DayHeaderStyle-ForeColor="Black" 
                                    SelectedDayStyle-BackColor="Black" SelectedDayStyle-ForeColor="White" OnDayRender="NoConsumeAvailabilityCalendar_DayRender"
                                    OnSelectionChanged="NoConsumeAvailabilityCalendar_SelectionChanged" OnVisibleMonthChanged="NoConsumeAvailabilityCalendar_VisibleMonthChanged"></asp:Calendar>
                            </section>
                            <section class="card-footer">
                                <section class="text-center">
                                    <asp:Button ID="btnAvailability" runat="server" Text="Mudar disponibilidade" CssClass="btn btn-primary" OnClick="btnAvailability_Click" />
                                </section>
                            </section>
                        </section>
                        <section class="card">
                            <section class="card-header">
                                <h4 class="text-center">Forma��es Alocadas</h4>
                            </section>
                            <section class="card-body table-responsive">
                                <asp:GridView runat="server" ID="gvFormacoes" AutoGenerateColumns="false" AllowPaging="true" PageSize="5"
                                    CssClass="table table-bordered" OnPageIndexChanging="gvFormacoes_PageIndexChanging" DataKeyNames="Id"
                                    HeaderStyle-CssClass="text-center">
                                    <Columns>
                                        <asp:BoundField DataField="Nome" HeaderText="Forma��o" />
                                        <asp:BoundField DataField="Formador.Nome" HeaderText="Formador" />
                                        <asp:BoundField DataField="Cliente.Nome" HeaderText="Cliente" />
                                        <asp:BoundField DataField="Categoria" HeaderText="Categoria" />
                                        <asp:BoundField DataField="LocalFormacao" HeaderText="Local" />
                                        <asp:BoundField DataField="DataInicioString" HeaderText="Data Inicio" />
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <asp:Label ID="lblEmpty" runat="server" Text="Este recurso n�o est� alocado a nenhuma forma��o"></asp:Label>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </section>
                        </section>
                    </section>
                    <div class="col-md-1"></div>
                </section>
            </section>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="NoConsumeAvailabilityCalendar" EventName="SelectionChanged" />
        </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript" src="Recursos/script.js"></script>
</asp:Content>
