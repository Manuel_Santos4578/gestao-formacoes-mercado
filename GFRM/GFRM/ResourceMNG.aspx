﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dashboard.Master" AutoEventWireup="true" CodeBehind="ResourceMNG.aspx.cs" Inherits="GFRM.ResourceMNG" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href='https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css' rel='stylesheet' />
    <script src='https://code.jquery.com/jquery-3.3.1.js'></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <style type="text/css">
        .hiddencol {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <asp:ScriptManager ID="scpManager" runat="server"></asp:ScriptManager>

    <div class="card">
        <div class="card-body">
            <asp:Label ID="lblCardHead" runat="server" Text="Recursos"></asp:Label>
            <asp:Button ID="btnAdicionar" runat="server" CssClass="btn btn-primary btnadd" ToolTip="Adicionar Novo/a" OnClick="btnAdicionar_Click" />
        </div>
    </div>
    <div class="card table-responsive">
        <div class="card-body">
            <asp:UpdatePanel ID="updPanelGrid" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                <ContentTemplate>
                    <asp:Button ID="search" runat="server" Text="Pesquisar" CssClass="btn btn-primary" OnClick="searchButton_Click" Style="float: right; margin-bottom: 20px;" />
                    <asp:TextBox ID="searchBox" runat="server" CssClass="form-control" Style="width: 300px; float: right; margin-right: 10px; height: 40px;"></asp:TextBox>
                    <asp:Button ID="reset" runat="server" OnClick="resetSearchButton_Click" Style="float: right"/>
                    <asp:DropDownList ID="ddlCat" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlCat_SelectedIndexChanged" runat="server" Style="float: right;"></asp:DropDownList>

                    <asp:RadioButtonList ID="rb" runat="server" AutoPostBack="true" RepeatDirection="Horizontal" Style="float: left;" OnSelectedIndexChanged="rb_SelectedIndexChanged">
                        <asp:ListItem class="col-md-6" Value="Consumivel" Selected="True"> Consumível</asp:ListItem>
                        <asp:ListItem class="col-md-6" Value="NaoConsumivel"> Não Consumível</asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:Button ID="btnEdit" runat="server" CssClass="btn btn-primary btnedit" Visible="false" ToolTip="Editar" OnClick="btnEdit_Click" />
                    <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger btndelete" Visible="false" ToolTip="Apagar" OnClick="btnDelete_Click" 
                        OnClientClick="if ( ! deleteConfirm()) return false;"/>

                    <asp:GridView ID="GridView1" CssClass="table table-bordered" runat="server" ShowHeaderWhenEmpty="false" AutoGenerateColumns="false" AllowPaging="True" PageSize="15"
                        HeaderStyle-CssClass="text-center" OnPageIndexChanging="GridView1_PageIndexChanging" DataKeyNames="Id" OnRowDataBound="OnRowDataBound"
                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField DataField="Id" Visible="true" ReadOnly="true" HeaderText="ID" />
                            <asp:BoundField DataField="Nome" HeaderText="Nome" />
                            <asp:BoundField DataField="Categoria" HeaderText="Categoria" />
                            <asp:BoundField DataField="Quantidade" HeaderText="Quantidade" ReadOnly="true" />
                            <asp:BoundField DataField="ValorMinimo" HeaderText="Valor Minimo" ReadOnly="true" />
                        </Columns>
                        <EmptyDataTemplate>
                            <asp:Label ID="Label1" runat="server" style="text-align:center" Text="Não existem dados para mostrar"></asp:Label></EmptyDataTemplate>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="GridView1" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="rb" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="reset" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="GridView1" EventName="PageIndexChanging" />
                    <asp:AsyncPostBackTrigger ControlID="ddlCat" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="search" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            SetKeyPressEvent('<%=searchBox.ClientID %>','<%=search.ClientID %>', 13, 'keypress', true);
        });

        function SetKeyPressEvent(origem, target, key, tipo, setFocus) {
            if (setFocus) {
                let valorOriginal = $('#' + origem).val();
                $('#' + origem).focus().val("").val(valorOriginal);
            }
            $('#' + origem).bind(tipo, function (e) {
                if (e.keyCode === key) { // 13 is enter key
                    $("#" + target).click();
                    e.preventDefault();
                }

            });
        }

        function deleteConfirm() {
            return confirm("Pretende mesmo eliminar este recurso? Esta ação vai eliminar TODA A QUANTIDADE existente em stock!");
        }
    </script>
</asp:Content>
