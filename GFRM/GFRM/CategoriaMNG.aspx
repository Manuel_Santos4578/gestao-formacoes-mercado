﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dashboard.Master" AutoEventWireup="true" CodeBehind="CategoriaMNG.aspx.cs" Inherits="GFRM.CategoriaMNG" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="scpManager" runat="server"></asp:ScriptManager>

    <asp:UpdatePanel ID="updPanelUser" ChildrenAsTriggers="false" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="card">
                <div class="card-body">
                    <asp:Label ID="lblCardHead" runat="server" Text="Categorias"></asp:Label>
                    <asp:Button ID="btnAdicionar" runat="server" CssClass="btn btn-primary btnadd" ToolTip="Adicionar Novo/a" OnClick="enableBtnAndBox_Click" />
                </div>
            </div>
            <div id="cardCat" class="card table-responsive">
                <div class="card-body">
                    <p>
                        <asp:GridView ID="GridView1" CssClass="table table-bordered" runat="server" AutoGenerateColumns="false" AllowPaging="True" HeaderStyle-CssClass="text-center"
                            DataKeyNames="Nome" OnPageIndexChanging="GridView1_PageIndexChanging" ShowHeaderWhenEmpty="false" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" OnRowDataBound="OnRowDataBound">
                            <Columns>
                                <asp:BoundField DataField="Nome" HeaderText="Nome" />
                            </Columns>
                            <EmptyDataTemplate>
                                <asp:Label ID="lblEmpty" Visible="false" runat="server" Text="Não existem dados para mostrar"></asp:Label>
                                </EmptyDataTemplate>
                        </asp:GridView>
                        
                        <asp:TextBox runat="server" ID="txtCategoria" Visible="false" CssClass="form-control" Style="width: 230px"></asp:TextBox>
                    </p>
                    <div style="float: left">
                        <asp:Button ID="btnEdit" runat="server" CssClass="btn btn-primary btnedit" Visible="false" ToolTip="Editar" OnClick="btnEdit_Click" 
                            OnClientClick="if ( ! editConfirm()) return false;"/>
                        <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger btndelete" Visible="false" ToolTip="Apagar" OnClick="btnDelete_Click"
                            OnClientClick="if ( ! deleteConfirm()) return false;"/>
                        <asp:Button ID="btnCheck" runat="server" CssClass="btn btn-primary btncheck" Visible="false" ToolTip="Aceitar" OnClick="btnAdicionar_Click" />
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="GridView1" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnAdicionar" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnEdit" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnCheck" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <script>
        function editConfirm() {
            return confirm('Pretende modificar a categoria selecionada?');
        }

        function deleteConfirm() {
            return confirm("Pretende mesmo eliminar a categoria selecionada?");
        }
    </script>
</asp:Content>
