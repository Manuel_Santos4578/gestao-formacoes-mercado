﻿using System;
using System.Data.SqlClient;
using System.Reflection;

namespace Apontamentos
{
    public static class SqlCommandCreator
    {
        public static SqlCommand Select(object objectToSelect, SqlConnection connection)
        {
            Type objType = objectToSelect.GetType();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;

            command.CommandText = $"Select * From [{objType.Name}]";

            return command;
        }
        public static SqlCommand Insert(object objectToInsert, SqlConnection connection)
        {
            Type objType = objectToInsert.GetType();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;

            command.CommandText = $"Insert INTO [{objType.Name}] (";

            PropertyInfo[] props = objType.GetProperties();
            foreach (PropertyInfo prop in props)
            {
                command.CommandText+= (prop.Name != "Id") ? $"[{prop.Name}]," : "" ;
                /*  if (prop.Name != "Id") command.CommandText += $"[{prop.Name}],"; */
            }
            command.CommandText =
                command.CommandText.Remove(command.CommandText.Length - 1, 1);
            command.CommandText += $") VALUES (";
            foreach (PropertyInfo prop in props)
            {
                command.CommandText += (prop.Name != "Id") ? $"@{prop.Name}," : "";
                /* if (prop.Name != "Id") command.CommandText += $"@{prop.Name},"; */
            }
            command.CommandText = command.CommandText.Remove(command.CommandText.Length - 1, 1);
            command.CommandText += $")";

            return command;
        }
        public static SqlCommand Update(object objectToUpdate, SqlConnection connection)    
        {
            Type objType = objectToUpdate.GetType();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;

            command.CommandText = $"Update [{objType.Name}] SET ";
            PropertyInfo[] props = objType.GetProperties();
            foreach (PropertyInfo prop in props)
            {
                command.CommandText += (prop.Name != "Id") ? $"{prop.Name}=@{prop.Name} " : "";
                /* if (prop.Name != "Id")
                    command.CommandText += $"{prop.Name}=@{prop.Name},"; */
            }
            command.CommandText += " Where ";
            foreach (PropertyInfo prop in props)
            {
                command.CommandText += (prop.Name == "Id") ? $"{prop.Name}=@{prop.Name} " : "";
                /*if (prop.Name == "Id") command.CommandText += $"{prop.Name}=@{prop.Name} ";*/
            }
            return command;
        }
        public static SqlCommand Delete(object objectToDelete, SqlConnection connection)
        {
            Type objType = objectToDelete.GetType();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;

            command.CommandText = $"DELETE FROM [{objType.Name}] Where ";
            PropertyInfo[] props = objType.GetProperties();
            foreach (PropertyInfo prop in props)
            {
                command.CommandText += (prop.Name == "Id") ? $"{prop.Name} = @{prop.Name} " : "";
                /* if (prop.Name == "Id")
                {
                    command.CommandText += $"{prop.Name} = @{prop.Name} ";
                    break;
                } */
            }
              
            return command;
        }
        public static void Where(SqlCommand command, string condicao)
        {
            command.CommandText += " Where " + condicao;
        }
        public static void OrderBy(SqlCommand command, string variavel, bool sentido)
        {
            command.CommandText += " Order by " + variavel;
            if (!sentido)
            {
                command.CommandText += " desc";
            }
        }
        public static void GroupBy(SqlCommand command,string variavel)
        {
            command.CommandText += " group by "+variavel;
        }
    }
}