﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Net;
using System.Net.Mail;

namespace Class_Library
{
    public static class Auxiliar
    {
        static readonly string[] ValidEmailDomains = new string[] {"gmail.com","outlook.com","hotmail.com","live.com.pt",
            "yahoo.com","sapo.pt","meo.pt","amazon.com" };
        public static string EncryptPass(string valor)
        {
            const string SharedSecret = "Aqu1 Nao Entr@s";
            if (string.IsNullOrEmpty(valor))
                return string.Empty;
            valor += SharedSecret;
            byte[] data = System.Text.Encoding.ASCII.GetBytes(valor);
            data = new System.Security.Cryptography.SHA256Managed().ComputeHash(data);
            return Convert.ToBase64String(data);
        }
        public static string RandomStringGen()
        {
            Random random = new Random();
            int length = random.Next(10, 20);
            const string AllowedChars = "0123456789QAZXSWEDCVFRTGBNHYUJMKIOLPqazxswedcvfrtgbnhyujmkiolp#$%&_+-";
            string rnd = "";
            for (int i = 0; i < length; i++)
            {
                System.Threading.Thread.Sleep(50);
                byte index = (byte)random.Next(0, AllowedChars.Length);
                rnd += AllowedChars[index];
            }
            return rnd;
        }
        public static string RandomStringGen(int length)
        {
            const string AllowedChars = "0123456789QAZXSWEDCVFRTGBNHYUJMKIOLPqazxswedcvfrtgbnhyujmkiolp#$%&_";
            string rnd = "";
            Random random = new Random();
            for (int i = 0; i < length; i++)
            {
                System.Threading.Thread.Sleep(50);
                byte index = (byte)random.Next(0, AllowedChars.Length);
                rnd += AllowedChars[index];
            }
            return rnd;
        }
        public static bool ValidateEmail(string emailAddress)
        {
            try
            {
                // usar emailregex
                string domainName = emailAddress.Split('@').Last();
                if (!string.IsNullOrEmpty(domainName) && ValidEmailDomains.FirstOrDefault(domain => domain == domainName) != null)
                {
                    if (domainName.Contains("."))
                    {
                        domainName.Replace(".", "\\.");
                    }
                    Regex regex = new Regex("^([a-zA-Z0-9_.]{5,20})@" + domainName + "$");
                    Match match = regex.Match(emailAddress);
                    return match.Success;
                }
            }
            catch (Exception excep)
            {

            }
            return false;
        }
        public static bool ValidatePassword(string password)
        {
            if (!string.IsNullOrWhiteSpace(password))
            {
                Regex regex = new Regex("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$");
                Match match = regex.Match(password);
                return match.Success;
            }
            return false;
        }
        public static bool ValidateName(string name)
        {

            if (!string.IsNullOrWhiteSpace(name))
            {
                Regex regex = new Regex("^([A - z])([a - z]){ 2,}$");
                Match match = regex.Match(name);
                return match.Success;
            }
            return true;

        }
        public static bool SendEmail(string myEmail, string targetEmail, string password, string CC = "")
        {
            try
            {
                // necessário configurar acesso na conta de email
                SmtpClient smtpClient = new SmtpClient("smtp.office365.com  host ", 587 /*port*/);
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new System.Net.NetworkCredential(myEmail, password);
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.EnableSsl = true;
                MailMessage mail = new MailMessage();
                mail.Body = $"Foi registado na aplicação de GFM.\n As suas credênciais de acesso são:\n Username:  \n Password: \n " +
                    $"Por questões de segurança, aconselhamos a alteração da sua password\n" +
                    $"Atentamente,\n" +
                    $"Equipa GFRM.";
                mail.Subject = "Credenciais GFRM";

                //Setting From , To and CC
                mail.From = new MailAddress(myEmail);
                mail.To.Add(new MailAddress(targetEmail));
                smtpClient.Send(mail);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }
        public static List<string> GetAllOperationalIpAddresses()
        {
            List<string> WorkingIpAddresses = new List<string>();
            var networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface item in networkInterfaces)
            {
                if (item.OperationalStatus == OperationalStatus.Up) //&& item.NetworkInterfaceType == ?
                {
                    foreach (UnicastIPAddressInformation ip in item.GetIPProperties().UnicastAddresses)
                    {
                        if (ip.Address.AddressFamily == AddressFamily.InterNetwork & !IPAddress.IsLoopback(ip.Address))
                        {
                            WorkingIpAddresses.Add(ip.Address.ToString());
                        }
                    }
                }
            }
            return WorkingIpAddresses;
        }

        //public static bool ValidarInserirUtilizador(Utilizador user)
        //{
        //    List<Utilizador> list = DAL.ObterUtilizadores();
        //    bool find = true;
        //    if (list.FirstOrDefault(o => o.Nome == user.Nome) != null) // mais eficiente que o ciclo abaixo que faz o mesmo
        //        find = false;
        //}
    }
}
