﻿using System;
using System.Collections.Generic;
using System.Linq;
using Class_Library;

namespace ClassLibrary
{
    public static class BLL
    {
        public static bool ValidarUtilizador(Utilizador user)
        {
            List<Utilizador> list = new DAL().SelectUtilizadores();
            if (list != null)
            {
                if (list.FirstOrDefault(dbUser => dbUser.Username == user.Username
                    || dbUser.Email == user.Email) != null)
                {
                    return false;
                }
            }
            return true;
        } 
        public static bool ValidarLogin(Utilizador user)
        {
            Utilizador dbUser = new DAL().SelectUtilizador(user.Username);
            if (dbUser != null)
            {
                if (user.Username == dbUser.Username && user.Password == dbUser.Password 
                    || user.Email == dbUser.Email && user.Password == dbUser.Password)
                {
                    return true;
                }
            }
            return false;
        }
        //public static bool ValidarSaidaRecursoConsumivel(int Id, int valor)
        //{
        //    RecursoConsumivel recurso = new DAL().SelectRecursoConsumivel(Id);
        //    if (recurso.Quantidade < valor || valor < 0)
        //    {
        //        return false;
        //    }
        //    return true;
        //}
        public static bool ValidarInserirCliente(Cliente cliente)
        {
            Cliente dbCliente = new DAL().SelectCliente(cliente.Entidade);
            if (dbCliente != null)
            {
                return false;
            }
            return true;
        }
        public static bool ValidarInserirCategoria(string categoria)
        {
            List<string> categorias = new DAL().SelectCategorias();
            if (categorias.FirstOrDefault(dbCategoria => dbCategoria == categoria) != null)
            {
                return false;
            }
            return true;
        }
        public static bool ValidarInserirPermissao(string permissao)
        {
            List<string> permissoes = new DAL().SelectPermissoes();
            if (permissoes.FirstOrDefault(dbPermissao => dbPermissao == permissao) != null)
            {
                return false;
            }
            return true;
        }
        public static bool ValidarSaida(int Quantidade, int Saida)
        {
            return (Quantidade >= Saida) ? true : false;
        }
        public static bool ValidarDisponibilidade(string username, DateTime dateTime)
        {
            List<Disponibilidade> disponibilidades = new DAL().SelectIndisponibilidadeUtilizador(new Utilizador() { Username = username });
            if (disponibilidades.FirstOrDefault(disponibilidade => disponibilidade.Data == dateTime && disponibilidade.Disponivel == false) != null)
            {
                return false;
            }
            return true;
        }
    }
}
