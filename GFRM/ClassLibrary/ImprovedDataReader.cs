﻿using System;
using System.Data.SqlClient;

namespace Apontamentos
{
    public class ImprovedSQLDataReader {

        
        public SqlDataReader dataR { get; set; }

        public ImprovedSQLDataReader()
        {
            
        }
        public ImprovedSQLDataReader(SqlDataReader dataR)
        {
            this.dataR = dataR;
        }

        public int GetInt32(int index)
        {
           return dataR.IsDBNull(index) ? 0 : dataR.GetInt32(index);  
        }
        public int GetInt32(string columnName)
        {
            return dataR.IsDBNull(dataR.GetOrdinal(columnName)) ? 0 : dataR.GetInt32(dataR.GetOrdinal(columnName));
        }
        public string GetString(int index)
        {
            return dataR.IsDBNull(index) ? "" : dataR.GetString(index);
        }
        public string GetString(string columnName)
        {
            return dataR.IsDBNull(dataR.GetOrdinal(columnName)) ? "" : dataR.GetString(dataR.GetOrdinal(columnName));
        }
        public DateTime GetDateTime(int index)
        {
            return dataR.IsDBNull(index) ? DateTime.Now : dataR.GetDateTime(index);
        }
        public DateTime GetDateTime(string columnName)
        {
            return dataR.IsDBNull(dataR.GetOrdinal(columnName)) ? DateTime.Now : dataR.GetDateTime(dataR.GetOrdinal(columnName));
        }
        public bool GetBool(int index)
        {
            return dataR.IsDBNull(index) ? false : dataR.GetBoolean(index);
        }
        public bool GetBool(string columnName)
        {
            return dataR.IsDBNull(dataR.GetOrdinal(columnName)) ? false : dataR.GetBoolean(dataR.GetOrdinal(columnName));
        }
        public double GetDouble(int index)
        {
            return dataR.IsDBNull(index) ? 0 : dataR.GetDouble(index);
        }
        public double GetDouble(string columnName)
        {
            return dataR.IsDBNull(dataR.GetOrdinal(columnName)) ? 0 : dataR.GetDouble(dataR.GetOrdinal(columnName));
        }
        public TimeSpan GetTimeSpan(int index)
        {
            return dataR.IsDBNull(index) ? new TimeSpan() : dataR.GetTimeSpan(index);
        }
        public TimeSpan GetTimeSpan(string columnName)
        {
            return dataR.IsDBNull(dataR.GetOrdinal(columnName)) ? new TimeSpan() : dataR.GetTimeSpan(dataR.GetOrdinal(columnName));
        }
    }
}
