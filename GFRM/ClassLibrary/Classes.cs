﻿using System;
using System.Collections.Generic;

namespace Class_Library
{
    public class Cliente
    {
        public string Entidade { get; set; }
        public string Nome { get; set; }
        public string Contacto { get; set; }
        public string Morada { get; set; }
    }
    public class Utilizador
    {
        public string Username { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstPassword { get; set; }
        public List<string> Categorias { get; set; }
        public string CategoriasString { get { return GetCategoriasString(); } }
        public Disponibilidade _Disponibilidade { get; set; }
        public string Permissao { get; set; }
        public Utilizador()
        {
            _Disponibilidade = new Disponibilidade();
            Categorias = new List<string>();
        }
        public Utilizador(string password)
        {
            AlterarPassword(password);
            _Disponibilidade = new Disponibilidade();
            Categorias = new List<string>();
        }
        public bool AlterarPassword(string password)
        {
            if (!string.IsNullOrEmpty(password))
            {
                this.Password = Auxiliar.EncryptPass(password);
                return true;
            }
            return false;
        }
        public string GetCategoriasString()
        {
            string categorias = "";
            foreach (string categoria in Categorias)
            {
                categorias += categoria + ", ";
            }
            categorias = categorias.Remove(categorias.Length - 2, 2);
            return categorias;
        }
    }
    public class DiaFormacao
    {
        public DateTime Data { get; set; }
        public int Duracao { get; set; }
        public TimeSpan HoraInicio { get; set; }
        public TimeSpan HoraFim { get; set; }
        public DiaFormacao()
        {
            HoraInicio = new TimeSpan(0, 0, 0);
            HoraFim = new TimeSpan(0, 0, 0);
            Data = new DateTime();
        }
        public string ValorString
        {
            get
            {
                return $"{this.Data.ToString("dd-MM-yyyy HH:mm")}";
            }
        }
        public static List<DiaFormacao> OrderByDateTime(List<DiaFormacao> myDatetimes)
        {
            DateTime dateTimeAux;
            foreach (DiaFormacao myDatetime in myDatetimes)
            {
                foreach (DiaFormacao myDatetime1 in myDatetimes)
                {
                    if (myDatetime.Data < myDatetime1.Data)
                    {
                        dateTimeAux = myDatetime1.Data;
                        myDatetime1.Data = myDatetime.Data;
                        myDatetime.Data = dateTimeAux;
                    }
                }
            }
            return myDatetimes;
        }
    }
    public class Formacao
    {
        public const string Atec = "Atec Porto";
        public int Id { get; set; }
        public string Nome { get; set; }
        public Cliente Cliente { get; set; }
        public Utilizador Formador { get; set; }
        public double ValorPagoFormador { get; set; }
        public DateTime DataInicioFormacao { get; set; }
        public string DataInicioString { get { return this.DataInicioFormacao.ToShortDateString(); } }
        public string Categoria { get; set; }
        public string LocalFormacao { get; set; }
        public Utilizador UltimoModificador { get; set; }
        public List<RecursoNaoConsumivel> ListaNConsumiveis { get; set; }
        public List<RecursoConsumivel> ListaConsumivel { get; set; }
        public List<DiaFormacao> ListaDiaFormacao { get; set; }
        public string Estado
        {
            get
            {
                if (this.DataInicioFormacao < DateTime.Today && this.ListaDiaFormacao[ListaDiaFormacao.Count - 1].Data >= DateTime.Today)
                {
                    return "A decorrer";
                }
                else if (this.DataInicioFormacao < DateTime.Today && this.ListaDiaFormacao[ListaDiaFormacao.Count - 1].Data < DateTime.Today)
                {
                    return "Terminada";
                }
                else
                {
                    return "Por iniciar";
                }
            }
        }
        public Formacao()
        {
            ListaConsumivel = new List<RecursoConsumivel>();
            ListaNConsumiveis = new List<RecursoNaoConsumivel>();
            ListaDiaFormacao = new List<DiaFormacao>();
            Formador = new Utilizador();
            Cliente = new Cliente();
            UltimoModificador = new Utilizador();
        }
    }
    public class Disponibilidade
    {
        public DateTime Data { get; set; }
        public bool Disponivel { get; set; }
        public Disponibilidade()
        {
            Data = new DateTime();
            Disponivel = false;
        }
    }
    public class RecursoNaoConsumivel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Categoria { get; set; }
        public Disponibilidade _Disponibilidade { get; set; }
        public RecursoNaoConsumivel()
        {
            _Disponibilidade = new Disponibilidade();
        }
    }
    public class RecursoConsumivel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Categoria { get; set; }
        public int Quantidade { get; set; }
        public int ValorMinimo { get; set; }
        public int Saida { get; set; }

    }
}
