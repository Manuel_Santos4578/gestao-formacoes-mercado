﻿using Apontamentos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Data;

namespace Class_Library
{
    public class DAL
    {
        #region Properties
        public SqlConnection Conexao;
        #endregion
        #region constructors
        public DAL()
        {
            Conexao = new SqlConnection(ConfigurationManager.ConnectionStrings["conString"].ConnectionString);
        }
        public DAL(string conString)
        {
            Conexao = new SqlConnection(conString);
        }
        #endregion

        #region Category
        public List<string> SelectCategorias()
        {
            try
            {
                if (Conexao.State == ConnectionState.Closed) Conexao.Open();
                List<string> lista = new List<string>();

                SqlCommand commando = new SqlCommand("SELECT Nome from CategoriasView", Conexao);
                SqlDataReader dataReader = commando.ExecuteReader();

                if (dataReader.HasRows)
                {
                    ImprovedSQLDataReader improvedSQLDataReader = new ImprovedSQLDataReader(dataReader);
                    while (dataReader.Read())
                    {
                        string categoria = improvedSQLDataReader.GetString("Nome");
                        lista.Add(categoria);
                    }
                }
                dataReader.Close();
                Conexao.Close();
                return lista;
            }
            catch (Exception ex)
            {
                Conexao.Close();
            }
            return new List<string>();
        }
        public DataTable GetCategorias()
        {
            try
            {
                DataTable dataTable = new DataTable();
                SqlDataAdapter adapter = new SqlDataAdapter("SELECT Nome FROM [Categoria]", Conexao);
                adapter.Fill(dataTable);
                return dataTable;
            }
            catch (Exception ex) { }
            if (Conexao.State == ConnectionState.Open) Conexao.Close();
            return new DataTable();
        }
        public bool InsertCategoria(string Categoria)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("exec CategoriasProc @Verb, @Nome", Conexao);
                commando.Parameters.AddWithValue("@Nome", Categoria);
                commando.Parameters.AddWithValue("@Verb", "Insert");
                byte checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                return (checker != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        public bool UpdateCategoria(string categoriaModificar, string nova)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("exec CategoriasProc @Verb, @Categoria,@Nome", Conexao);
                commando.Parameters.AddWithValue("@Verb", "Update");
                commando.Parameters.AddWithValue("@Categoria", categoriaModificar);
                commando.Parameters.AddWithValue("@Nome", nova);
                byte checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                return (checker != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        public bool DeleteCategoria(string categoria)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("exec CategoriasProc @Verb, @Nome", Conexao);
                commando.Parameters.AddWithValue("@Verb", "Delete");
                commando.Parameters.AddWithValue("@Nome", categoria);
                byte checker = 0;
                checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                return (checker != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        #endregion
        #region Permission
        public List<string> SelectPermissoes()
        {
            List<string> lista = new List<string>();
            try
            {
                if (Conexao.State == ConnectionState.Closed) Conexao.Open();
                SqlCommand commando = new SqlCommand("Select Nome from PermissoesView", Conexao);
                SqlDataReader dataReader = commando.ExecuteReader();
                if (dataReader.HasRows)
                {
                    ImprovedSQLDataReader improvedSQLDataReader = new ImprovedSQLDataReader(dataReader);
                    while (dataReader.Read())
                    {
                        string permissao = improvedSQLDataReader.GetString("Nome");
                        lista.Add(permissao);
                    }
                }
                dataReader.Close();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conexao.Close();
            }
            return lista;
        }
        public DataTable GetPermissoes()
        {
            DataTable dataTable = new DataTable();
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter("SELECT Nome FROM [Permissao]", Conexao);
                adapter.Fill(dataTable);
                return dataTable;
            }
            catch (Exception ex)
            {
            }
            if (Conexao.State == ConnectionState.Open) Conexao.Close();
            return dataTable;
        }
        public bool InsertPermissao(string Permissao)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("exec PermissoesProc 'Insert', @Nome", Conexao);
                commando.Parameters.AddWithValue("@Nome", Permissao);
                byte checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                return (checker != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        public bool UpdatePermissao(string permissaoModificar, string nova)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("exec PermissoesProc 'Update', @Permissao , @Nome", Conexao);
                commando.Parameters.AddWithValue("@Permissao", permissaoModificar);
                commando.Parameters.AddWithValue("@Nome", nova);
                byte checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                return (checker != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        public bool DeletePermissao(string permissao)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("exec PermissoesProc 'Delete', @Nome", Conexao);
                commando.Parameters.AddWithValue("@Nome", permissao);
                byte checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                return (checker != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        #endregion
        #region Clients
        public List<Cliente> SelectClientes()
        {
            try
            {
                if (Conexao.State == ConnectionState.Closed) Conexao.Open();
                List<Cliente> lista = new List<Cliente>();
                SqlCommand commando = new SqlCommand("Select * from ClientesView", Conexao);
                SqlDataReader dataReader = commando.ExecuteReader();
                if (dataReader.HasRows)
                {
                    ImprovedSQLDataReader improvedSQLDataReader = new ImprovedSQLDataReader(dataReader);
                    while (dataReader.Read())
                    {
                        Cliente cliente = new Cliente()
                        {
                            Entidade = improvedSQLDataReader.GetString("Entidade"),
                            Nome = improvedSQLDataReader.GetString("Nome"),
                            Contacto = improvedSQLDataReader.GetString("Contacto"),
                            Morada = improvedSQLDataReader.GetString("Morada")
                        };
                        lista.Add(cliente);
                    }
                }
                dataReader.Close();
                Conexao.Close();
                return lista;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return new List<Cliente>();
            }
        }
        public DataTable GetClientes(string entidade = "%", string nome = "%")
        {
            try
            {
                DataTable dataTable = new DataTable();
                SqlCommand commando = new SqlCommand("SELECT * FROM [Cliente] Where Entidade like @Entidade and Nome like @Nome", Conexao);
                commando.Parameters.AddWithValue("@Entidade", $"%{entidade}%");
                commando.Parameters.AddWithValue("@Nome", $"%{nome}%");
                SqlDataAdapter adapter = new SqlDataAdapter(commando);
                adapter.Fill(dataTable);
                return dataTable;
            }
            catch (Exception ex)
            {
            }
            if (Conexao.State == ConnectionState.Open) Conexao.Close();
            return new DataTable();
        }
        public Cliente SelectCliente(string entidade)
        {
            try
            {
                if (Conexao.State == ConnectionState.Closed) Conexao.Open();
                SqlCommand commando = new SqlCommand("exec ClientesProc @Verb, @Entidade", Conexao);
                commando.Parameters.AddWithValue("@Verb", "Select");
                commando.Parameters.AddWithValue("@Entidade", entidade);
                SqlDataReader dataReader = commando.ExecuteReader();
                if (dataReader.HasRows)
                {
                    ImprovedSQLDataReader improvedSQLDataReader = new ImprovedSQLDataReader(dataReader);
                    while (dataReader.Read())
                    {
                        Cliente cliente = new Cliente()
                        {
                            Entidade = improvedSQLDataReader.GetString("Entidade"),
                            Nome = improvedSQLDataReader.GetString("Nome"),
                            Contacto = improvedSQLDataReader.GetString("Contacto"),
                            Morada = improvedSQLDataReader.GetString("Morada")
                        };
                        dataReader.Close();
                        Conexao.Close();
                        return cliente;
                    }
                }
                dataReader.Close();
                Conexao.Close();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conexao.Close();
            }
            return null;
        }
        public bool InsertCliente(Cliente cliente)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = SqlCommandCreator.Insert(cliente, Conexao);
                commando.Parameters.AddWithValue("Entidade", cliente.Entidade);
                commando.Parameters.AddWithValue("Nome", cliente.Nome);
                commando.Parameters.AddWithValue("Contacto", cliente.Contacto);
                commando.Parameters.AddWithValue("Morada", cliente.Morada);
                byte checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                return (checker != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        public bool UpdateCliente(Cliente cliente, string entidade)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
            }
            catch (Exception excon)
            {
                return false;
            }
            SqlCommand commando = new SqlCommand("Update [Cliente] SET Entidade = @EntidadeNova, Nome = @Nome, Contacto = @Contacto, Morada = @Morada " +
                                                    "Where Entidade = @Entidade", Conexao);
            try
            {
                commando.Parameters.AddWithValue("Entidade", entidade);
                commando.Parameters.AddWithValue("EntidadeNova", cliente.Entidade);
                commando.Parameters.AddWithValue("Nome", cliente.Nome);
                commando.Parameters.AddWithValue("Contacto", cliente.Contacto);
                commando.Parameters.AddWithValue("Morada", cliente.Morada);

                byte checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                return (checker != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        public bool DeleteCliente(Cliente cliente)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();

            }
            catch (Exception conex)
            {
                return false;
            }
            SqlCommand commando = new SqlCommand("DELETE FROM [Cliente] Where Entidade = @Entidade ", Conexao);
            try
            {
                commando.Parameters.AddWithValue("@Entidade", cliente.Entidade);
                byte checker = 0;
                checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                return (checker != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        #endregion
        #region Users
        public List<Utilizador> SelectUtilizadores(string username = "", string nome = "", string categoria = "", string permissao = "", string email = "")
        {
            List<Utilizador> lista = new List<Utilizador>();
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando;
                if (permissao == "Worker")
                {
                    commando = new SqlCommand("Select * FROM [Utilizador] left join CategoriaUtilizador on Utilizador.Username = CategoriaUtilizador.Utilizador"
                                                   + " Where Username like @Username and Nome like @Nome " +
                                                    "and Permissao not like @Permissao and Email like @Email and Categoria like @Categoria", Conexao);
                }
                else
                {
                    commando = new SqlCommand(@"Select * FROM [Utilizador] left join CategoriaUtilizador on Utilizador.Username = CategoriaUtilizador.Utilizador 
                                               Where Username like @Username and Nome like @Nome and Permissao like @Permissao and Email like @Email and Categoria like @Categoria", Conexao);
                }
                commando.Parameters.AddWithValue("@Username", $"%{username}%");
                commando.Parameters.AddWithValue("@Nome", $"%{nome}%");
                commando.Parameters.AddWithValue("@Categoria", $"%{categoria}%");
                commando.Parameters.AddWithValue("@Permissao", $"%{permissao}%");
                commando.Parameters.AddWithValue("@Email", $"%{email}%");
                SqlDataReader dataR = commando.ExecuteReader();
                if (dataR.HasRows)
                {
                    ImprovedSQLDataReader improvedSQLDataReader = new ImprovedSQLDataReader(dataR);
                    string UsernameFlag = "";
                    while (dataR.Read())
                    {
                        if (UsernameFlag != improvedSQLDataReader.GetString("Username"))
                        {
                            Utilizador utilizador = new Utilizador()
                            {
                                Username = improvedSQLDataReader.GetString("Username"),
                                Nome = improvedSQLDataReader.GetString("Nome"),
                                Email = improvedSQLDataReader.GetString("Email"),
                                Permissao = improvedSQLDataReader.GetString("Permissao"),
                                Password = improvedSQLDataReader.GetString("Password"),
                                FirstPassword = improvedSQLDataReader.GetString("FirstPassword"),
                            };
                            UsernameFlag = utilizador.Username;
                            utilizador.Categorias.Add(improvedSQLDataReader.GetString("Categoria"));
                            lista.Add(utilizador);
                        }
                        else
                        {
                            if (lista.Count != 0)
                                lista[lista.Count - 1].Categorias.Add(improvedSQLDataReader.GetString("Categoria"));
                        }
                    }
                    dataR.Close();
                }
            }
            catch (Exception ex)
            {
            }
            Conexao.Close();
            return lista;
        }
        public Utilizador SelectUtilizador(string username)
        {
            try
            {
                if (Conexao.State == ConnectionState.Closed) Conexao.Open();

                string query = "SELECT * From [Utilizador] inner join CategoriaUtilizador on Utilizador.Username = CategoriaUtilizador.Utilizador ";
                SqlCommand commando = new SqlCommand(query, Conexao);
                SqlCommandCreator.Where(commando, $"Username like @Username or Email = @Email");
                commando.Parameters.AddWithValue("@Username", username);
                commando.Parameters.AddWithValue("@Email", username);
                SqlDataReader dataReader = commando.ExecuteReader();

                if (dataReader.HasRows)
                {
                    Utilizador utilizador = new Utilizador();
                    ImprovedSQLDataReader improvedSQLDataReader = new ImprovedSQLDataReader(dataReader);
                    while (dataReader.Read())
                    {
                        string UsernameFlag = "";
                        if (UsernameFlag != improvedSQLDataReader.GetString("Username"))
                        {
                            utilizador.Username = improvedSQLDataReader.GetString("Username");
                            utilizador.Nome = improvedSQLDataReader.GetString("Nome");
                            utilizador.Email = improvedSQLDataReader.GetString("Email");
                            utilizador.Password = improvedSQLDataReader.GetString("Password");
                            utilizador.FirstPassword = improvedSQLDataReader.GetString("FirstPassword");
                            utilizador.Permissao = improvedSQLDataReader.GetString("Permissao");
                            utilizador.Categorias.Add(improvedSQLDataReader.GetString("Categoria"));
                            UsernameFlag = utilizador.Username;
                        }
                        else
                        {
                            if (utilizador.Categorias.Count != 0)
                            {
                                utilizador.Categorias.Add(improvedSQLDataReader.GetString("Categoria"));
                            }
                        }
                    }
                    dataReader.Close();
                    Conexao.Close();
                    return utilizador;
                }
                dataReader.Close();
            }
            catch (Exception)
            {
            }
            Conexao.Close();
            return null;
        }
        public bool InsertUtilizador(Utilizador utilizador)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();

                SqlCommand commando = new SqlCommand("exec UtilizadoresProc @Verb, @Username, @Nome, @Email, @Permissao, @Password, @FirstPassword", Conexao);
                commando.Parameters.AddWithValue("@Username", utilizador.Username);
                commando.Parameters.AddWithValue("@Nome", utilizador.Nome);
                commando.Parameters.AddWithValue("@Verb", "Insert");
                commando.Parameters.AddWithValue("@Email", utilizador.Email);
                commando.Parameters.AddWithValue("@Password", utilizador.Password);
                commando.Parameters.AddWithValue("@FirstPassword", utilizador.Password);
                commando.Parameters.AddWithValue("@Permissao", utilizador.Permissao);
                byte checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                if (checker != 0)
                {
                    foreach (string categoria in utilizador.Categorias)
                    {
                        InsertCategoriaUtilizador(utilizador, categoria);
                    }
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        public bool InsertCategoriaUtilizador(Utilizador utilizador, string categoria)
        {
            byte checker = 0;
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("Insert into CategoriaUtilizador (Categoria,Utilizador) values (@Categoria,@Utilizador)", Conexao);
                commando.Parameters.AddWithValue("@Utilizador", utilizador.Username);
                commando.Parameters.AddWithValue("@Categoria", categoria);
                checker = (byte)commando.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                //throw new Exception("Falhou ligação à DB");
            }
            catch (Exception ex)
            {

            }
            finally
            {
                Conexao.Close();
            }
            return (checker != 0) ? true : false;
        }
        public bool DeleteCategoriaUtilizador(Utilizador utilizador)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("DELETE FROM [CategoriaUtilizador] Where Utilizador = @Username", Conexao);
                commando.Parameters.AddWithValue("@Username", utilizador.Username);
                byte checker = 0;
                checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                if (checker != 0) return true;
                return false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        public bool UpdateUtilizador(Utilizador utilizador)
        {
            if (Conexao.State != ConnectionState.Open) Conexao.Open();
            SqlCommand commando = new SqlCommand("exec UtilizadoresProc @Verb, @Username, @Nome, @Email, @Permissao", Conexao);
            try
            {
                commando.Parameters.AddWithValue("@Verb", "Update");
                commando.Parameters.AddWithValue("@Nome", utilizador.Nome);
                commando.Parameters.AddWithValue("@Email", utilizador.Email);
                commando.Parameters.AddWithValue("@Permissao", utilizador.Permissao);
                commando.Parameters.AddWithValue("@Username", utilizador.Username);
                byte checker = 0;
                checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                if (checker != 0)
                {
                    DeleteCategoriaUtilizador(utilizador);
                    foreach (string categoria in utilizador.Categorias)
                    {
                        InsertCategoriaUtilizador(utilizador, categoria);
                    }
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        public bool UpdateUtilizadorPassword(Utilizador utilizador, string password)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("exec UtilizadoresProc @Verb, @Username, @Nome, @Email, @Permissao, @Password", Conexao);
                utilizador.AlterarPassword(password);
                commando.Parameters.AddWithValue("@Password", utilizador.Password);
                commando.Parameters.AddWithValue("@Username", utilizador.Username);
                commando.Parameters.AddWithValue("@Nome", utilizador.Nome);
                commando.Parameters.AddWithValue("@Verb", "Change");
                commando.Parameters.AddWithValue("@Permissao", utilizador.Permissao);
                commando.Parameters.AddWithValue("@Email", utilizador.Email);

                byte checker = 0;
                checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                if (checker != 0) return true;
                return false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        public bool UpdateUtilizadorPasswordReset(Utilizador utilizador, out string pw)
        {
            string password = Auxiliar.RandomStringGen(10);
            pw = password;
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("exec UtilizadoresProc @Verb, @Username, @Nome, @Email,@Permissao, @Password", Conexao);
                utilizador.AlterarPassword(password);
                commando.Parameters.AddWithValue("@Password", utilizador.Password);
                commando.Parameters.AddWithValue("@Username", utilizador.Username);
                commando.Parameters.AddWithValue("@Email", utilizador.Email);
                commando.Parameters.AddWithValue("@Permissao", "");
                commando.Parameters.AddWithValue("@Verb", "Reset");
                commando.Parameters.AddWithValue("@FirstPassword", utilizador.Password);
                byte checker = 0;
                checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                if (checker != 0) return true;
                return false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
            }
            return false;
        }
        public bool DeleteUtilizador(Utilizador utilizador)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("exec UtilizadoresProc @Verb, @Username, @Nome, @Email", Conexao);
                commando.Parameters.AddWithValue("@Verb", "Delete");
                commando.Parameters.AddWithValue("@Username", utilizador.Username);
                commando.Parameters.AddWithValue("@Nome", "");
                commando.Parameters.AddWithValue("@Email", utilizador.Email);
                byte checker = 0;
                checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                return (checker != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        public List<Disponibilidade> SelectIndisponibilidadeUtilizador(Utilizador utilizador)
        {
            List<Disponibilidade> lista = new List<Disponibilidade>();
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("Select * from [DisponibilidadeUtilizador] Where Utilizador = @Utilizador and Disponivel = 0", Conexao);
                commando.Parameters.AddWithValue("@Utilizador", utilizador.Username);
                SqlDataReader dataReader = commando.ExecuteReader();
                if (dataReader.HasRows)
                {
                    ImprovedSQLDataReader improvedSQLDataReader = new ImprovedSQLDataReader(dataReader);
                    while (dataReader.Read())
                    {
                        Disponibilidade disponibilidade = new Disponibilidade()
                        {
                            Data = improvedSQLDataReader.GetDateTime("Data"),
                            Disponivel = improvedSQLDataReader.GetBool("Disponivel")
                        };
                        lista.Add(disponibilidade);
                    }
                    dataReader.Close();
                }
                dataReader.Close();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conexao.Close();
            }
            return lista;
        }
        public List<Disponibilidade> SelectIndisponibilidadeUtilizadorDiasFormacao(Utilizador utilizador)
        {
            List<Disponibilidade> lista = new List<Disponibilidade>();
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("select * from DisponibilidadeUtilizador as DU1 " +
                    "inner join Utilizador on DU1.Utilizador = Utilizador.Username " +
                    "Where DU1.Data in (select Data from DiaFormacao " +
                    "inner join Formacao on Formacao.Id = DiaFormacao.Formacao " +
                    "Where Formador = @Formador) and Utilizador = @Formador", Conexao);
                commando.Parameters.AddWithValue("@Formador", utilizador.Username);
                SqlDataReader dataReader = commando.ExecuteReader();
                if (dataReader.HasRows)
                {
                    ImprovedSQLDataReader improvedSQLDataReader = new ImprovedSQLDataReader(dataReader);
                    while (dataReader.Read())
                    {
                        Disponibilidade disponibilidade = new Disponibilidade()
                        {
                            Data = improvedSQLDataReader.GetDateTime("Data"),
                            Disponivel = improvedSQLDataReader.GetBool("Disponivel")
                        };
                        lista.Add(disponibilidade);
                    }
                    dataReader.Close();
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conexao.Close();
            }
            return lista;
        }
        public List<Disponibilidade> SelectIndisponibilidadeUtilizadorNotDiasFormacao(Utilizador utilizador)
        {
            List<Disponibilidade> lista = new List<Disponibilidade>();
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("select * from DisponibilidadeUtilizador as DU1 " +
                    "inner join Utilizador on DU1.Utilizador = Utilizador.Username " +
                    "Where DU1.Data not in (select Data from DiaFormacao " +
                    "inner join Formacao on Formacao.Id = DiaFormacao.Formacao " +
                    "Where Formador = @Formador) and Utilizador = @Formador", Conexao);
                commando.Parameters.AddWithValue("@Formador", utilizador.Username);
                SqlDataReader dataReader = commando.ExecuteReader();
                if (dataReader.HasRows)
                {
                    ImprovedSQLDataReader improvedSQLDataReader = new ImprovedSQLDataReader(dataReader);
                    while (dataReader.Read())
                    {
                        Disponibilidade disponibilidade = new Disponibilidade()
                        {
                            Data = improvedSQLDataReader.GetDateTime("Data"),
                            Disponivel = improvedSQLDataReader.GetBool("Disponivel")
                        };
                        lista.Add(disponibilidade);
                    }
                    dataReader.Close();
                }
                dataReader.Close();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conexao.Close();
            }
            return lista;
        }
        public bool InsertDisponibilidadeUtilizador(Utilizador utilizador)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("Insert into [DisponibilidadeUtilizador] (Utilizador,Data,Disponivel) values (@Utilizador,@Data,@Disponivel)", Conexao);
                commando.Parameters.AddWithValue("@Utilizador", utilizador.Username);
                commando.Parameters.AddWithValue("@Data", utilizador._Disponibilidade.Data);
                commando.Parameters.AddWithValue("@Disponivel", utilizador._Disponibilidade.Disponivel);
                byte checker = 0;
                checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                return (checker != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        public void ExecDisponibilidadesPessoais(Utilizador utilizador, DateTime date)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("exec IndisponibilidadesPessoais @Data, @User", Conexao);
                commando.Parameters.AddWithValue("@Data", date.Date);
                commando.Parameters.AddWithValue("@User", utilizador.Username);
                commando.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
            Conexao.Close();
        }
        public bool UpdateDisponibilidadeUtilizador(Utilizador utilizador)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("Update [DisponibilidadeUtilizador] Set Disponivel = @Disponivel Where Utilizador = @Utilizador And Data = @Data", Conexao);
                commando.Parameters.AddWithValue("@Utilizador", utilizador.Username);
                commando.Parameters.AddWithValue("@Data", utilizador._Disponibilidade.Data);
                commando.Parameters.AddWithValue("@Disponivel", utilizador._Disponibilidade.Disponivel);
                byte checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                return (checker != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        #endregion
        #region CourseDay
        public List<DiaFormacao> SelectDiasFormacao(Formacao formacao = null)
        {
            List<DiaFormacao> lista = new List<DiaFormacao>();
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("Select * from [DiaFormacao]", Conexao);
                if (formacao != null)
                {
                    SqlCommandCreator.Where(commando, "Formacao = @Formacao");
                    commando.Parameters.AddWithValue("@Formacao", formacao.Id);
                }
                SqlDataReader dataR = commando.ExecuteReader();
                if (dataR.HasRows)
                {
                    ImprovedSQLDataReader improvedSQLDataReader = new ImprovedSQLDataReader(dataR);
                    while (dataR.Read())
                    {
                        DiaFormacao diaFormacao = new DiaFormacao()
                        {
                            Data = improvedSQLDataReader.GetDateTime("Data"),
                            Duracao = improvedSQLDataReader.GetInt32("Duracao"),
                            HoraInicio = improvedSQLDataReader.GetTimeSpan("HoraInicio"),
                        };
                        lista.Add(diaFormacao);
                    }
                }
                dataR.Close();
                Conexao.Close();
                return lista;
            }
            catch (Exception ex)
            {
                Conexao.Close();
            }
            return lista;
        }
        public bool InsertDiaFormacao(DiaFormacao diaFormacao, Formacao formacao)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("Insert into [DiaFormacao] (Formacao,Data,Duracao,HoraInicio,HoraFim) " +
                    "values (@Formacao,@Data,@Duracao,@HoraInicio,@HoraFim)", Conexao);
                commando.Parameters.AddWithValue("@Formacao", formacao.Id);
                commando.Parameters.AddWithValue("@Data", diaFormacao.Data);
                commando.Parameters.AddWithValue("@Duracao", diaFormacao.Duracao);
                commando.Parameters.AddWithValue("@HoraInicio", diaFormacao.HoraInicio);
                commando.Parameters.AddWithValue("@HoraFim", diaFormacao.HoraFim);
                byte checker = 0;
                checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                return (checker != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        public bool UpdateDiaFormacao(DiaFormacao diaFormacao, int formacao)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("Update [DiaFormacao] SET Duracao = @Duracao, HoraInicio = @HoraInicio WHERE Formacao = @Formacao AND Data = @Data", Conexao);
                commando.Parameters.AddWithValue("@Formacao", formacao);
                commando.Parameters.AddWithValue("@Data", diaFormacao.Data);
                commando.Parameters.AddWithValue("@Duracao", diaFormacao.Duracao);
                commando.Parameters.AddWithValue("@HoraInicio", diaFormacao.HoraInicio);

                byte checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                return (checker != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        public bool DeleteDiasFormacao(Formacao formacao)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand command = new SqlCommand("Delete from DiaFormacao Where Formacao = @Formacao", Conexao);
                command.Parameters.AddWithValue("@Formacao", formacao.Id);
                byte checker = (byte)command.ExecuteNonQuery();
                Conexao.Close();
                return checker != 0 ? true : false;
            }
            catch (Exception)
            {
                Conexao.Close();
            }
            return false;
        }
        public bool DeleteDiaFormacao(DiaFormacao diaFormacao, int formacao)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("DELETE FROM [DiaFormacao] Where Formacao = @Formacao AND Data = @Data", Conexao);
                commando.Parameters.AddWithValue("@Formacao", formacao);
                commando.Parameters.AddWithValue("@Data", diaFormacao.Data);
                byte checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                return (checker != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        #endregion
        #region Courses
        public List<Formacao> SelectFormacoes(Utilizador user = null)
        {
            List<Formacao> lista = new List<Formacao>();
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("", Conexao);
                if (user == null)
                {
                    commando.CommandText = "Select * from [Formacao] inner join DiaFormacao on DiaFormacao.Formacao = Formacao.Id order by Formacao.Id Desc";
                }
                else if (user.Permissao == "Formador")
                {
                    commando.CommandText = "Select *" +
                        " from Formacao inner join DiaFormacao on DiaFormacao.Formacao = Formacao.Id " +
                        "Where Formador = @SessionFormador " +
                        "Order by Formacao.Id Desc";
                    commando.Parameters.AddWithValue("@SessionFormador", user.Username);
                }
                else
                {
                    commando.CommandText = "Select * from Formacao inner join DiaFormacao on DiaFormacao.Formacao = Formacao.Id Where Formador = @SessionFormador order by Formacao.Id Desc";
                    commando.Parameters.AddWithValue("@SessionFormador", user.Username);
                }
                SqlDataReader dataR = commando.ExecuteReader();
                if (dataR.HasRows)
                {
                    ImprovedSQLDataReader improvedSQLDataReader = new ImprovedSQLDataReader(dataR);
                    int id = 0;
                    while (dataR.Read())
                    {
                        if (improvedSQLDataReader.GetInt32("Id") != id)
                        {
                            Formacao formacao = new Formacao()
                            {
                                Id = improvedSQLDataReader.GetInt32("Id"),
                                Categoria = improvedSQLDataReader.GetString("Categoria"),
                                DataInicioFormacao = improvedSQLDataReader.GetDateTime("DataInicioFormacao"),
                                Nome = improvedSQLDataReader.GetString("Nome"),
                                LocalFormacao = improvedSQLDataReader.GetString("LocalFormacao"),
                                ValorPagoFormador = improvedSQLDataReader.GetDouble("ValorPagoFormador"),

                            };
                            formacao.Cliente.Entidade = improvedSQLDataReader.GetString("Cliente");
                            formacao.UltimoModificador.Username = improvedSQLDataReader.GetString("UltimoModificador");
                            formacao.ListaDiaFormacao.Add(new DiaFormacao
                            {
                                Data = improvedSQLDataReader.GetDateTime("Data"),
                                HoraInicio = improvedSQLDataReader.GetTimeSpan("HoraInicio"),
                                HoraFim = improvedSQLDataReader.GetTimeSpan("HoraFim"),
                                Duracao = improvedSQLDataReader.GetInt32("Duracao")
                            });
                            formacao.Formador.Username = improvedSQLDataReader.GetString("Formador");
                            id = formacao.Id;
                            lista.Add(formacao);
                        }
                        else
                        {
                            lista[lista.Count - 1].ListaDiaFormacao.Add(new DiaFormacao
                            {
                                Data = improvedSQLDataReader.GetDateTime("Data"),
                                HoraInicio = improvedSQLDataReader.GetTimeSpan("HoraInicio"),
                                HoraFim = improvedSQLDataReader.GetTimeSpan("HoraFim"),
                                Duracao = improvedSQLDataReader.GetInt32("Duracao")
                            });
                        }
                    }
                    dataR.Close();
                    foreach (Formacao formacao1 in lista)
                    {
                        string cliente = formacao1.Cliente.Entidade;
                        string ultimoModificador = formacao1.UltimoModificador.Username;
                        string formador = formacao1.Formador.Username;
                        formacao1.Cliente = SelectCliente(cliente);
                        formacao1.Formador = SelectUtilizador(formador);
                        formacao1.UltimoModificador = SelectUtilizador(ultimoModificador);
                    }
                }
                dataR.Close();
                Conexao.Close();
                return lista;
            }
            catch (Exception ex)
            {
                Conexao.Close();
            }
            return lista;
        }
        public List<Formacao> SelectFormacoes(string clienteSearch, Utilizador user = null)
        {
            List<Formacao> lista = new List<Formacao>();
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("Select * From Formacao inner join DiaFormacao on Diaformacao.Formacao=Formacao.Id Where Formador like @SessionFormador and Cliente like @Cliente order by Formacao.Id Desc", Conexao);
                if (user != null)
                    commando.Parameters.AddWithValue("@SessionFormador", user.Username);
                else
                    commando.Parameters.AddWithValue("@sessionFormador", "%");
                commando.Parameters.AddWithValue("@Cliente", clienteSearch);
                SqlDataReader dataR = commando.ExecuteReader();
                if (dataR.HasRows)
                {
                    int id = 0;
                    ImprovedSQLDataReader improvedSQLDataReader = new ImprovedSQLDataReader(dataR);
                    while (dataR.Read())
                    {
                        if (id != improvedSQLDataReader.GetInt32("Id"))
                        {
                            Formacao formacao = new Formacao()
                            {
                                Id = improvedSQLDataReader.GetInt32("Id"),
                                Categoria = improvedSQLDataReader.GetString("Categoria"),
                                DataInicioFormacao = improvedSQLDataReader.GetDateTime("DataInicioFormacao"),
                                Nome = improvedSQLDataReader.GetString("Nome"),
                                LocalFormacao = improvedSQLDataReader.GetString("LocalFormacao"),
                                ValorPagoFormador = improvedSQLDataReader.GetDouble("ValorPagoFormador")
                            };
                            formacao.Cliente.Entidade = improvedSQLDataReader.GetString("Cliente");
                            formacao.UltimoModificador.Username = improvedSQLDataReader.GetString("UltimoModificador");
                            formacao.Formador.Username = improvedSQLDataReader.GetString("Formador");
                            formacao.ListaDiaFormacao.Add(new DiaFormacao
                            {
                                Data = improvedSQLDataReader.GetDateTime("Data"),
                                HoraInicio = improvedSQLDataReader.GetTimeSpan("HoraInicio"),
                                HoraFim = improvedSQLDataReader.GetTimeSpan("HoraFim"),
                                Duracao = improvedSQLDataReader.GetInt32("Duracao")
                            });
                            id = formacao.Id;
                            lista.Add(formacao);
                        }
                        else
                        {
                            lista[lista.Count - 1].ListaDiaFormacao.Add(new DiaFormacao
                            {
                                Data = improvedSQLDataReader.GetDateTime("Data"),
                                HoraInicio = improvedSQLDataReader.GetTimeSpan("HoraInicio"),
                                HoraFim = improvedSQLDataReader.GetTimeSpan("HoraFim"),
                                Duracao = improvedSQLDataReader.GetInt32("Duracao")
                            });
                        }
                    }
                    dataR.Close();
                    foreach (Formacao formacao1 in lista)
                    {
                        string cliente = formacao1.Cliente.Entidade;
                        string ultimoModificador = formacao1.UltimoModificador.Username;
                        string formador = formacao1.Formador.Username;

                        formacao1.Cliente = SelectCliente(cliente);
                        formacao1.Formador = SelectUtilizador(formador);
                        formacao1.UltimoModificador = SelectUtilizador(ultimoModificador);
                    }
                }
                dataR.Close();
                Conexao.Close();
                return lista;
            }
            catch (Exception)
            {
                Conexao.Close();
            }
            return lista;
        }
        public List<Formacao> SelectFormacoes(RecursoNaoConsumivel naoConsumivel)
        {
            List<Formacao> lista = new List<Formacao>();
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("exec FormacoesOfRecursoNoConsume @recurso", Conexao);
                commando.Parameters.AddWithValue("recurso", naoConsumivel.Id);
                SqlDataReader dataR = commando.ExecuteReader();
                if (dataR.HasRows)
                {
                    ImprovedSQLDataReader improvedSQLDataReader = new ImprovedSQLDataReader(dataR);
                    while (dataR.Read())
                    {
                        Formacao formacao = new Formacao()
                        {
                            Id = improvedSQLDataReader.GetInt32("Id"),
                            Nome = improvedSQLDataReader.GetString("Nome"),
                            Categoria = improvedSQLDataReader.GetString("Categoria"),
                            LocalFormacao = improvedSQLDataReader.GetString("LocalFormacao"),
                            DataInicioFormacao = improvedSQLDataReader.GetDateTime("DataInicioFormacao")
                        };
                        formacao.Formador.Username = improvedSQLDataReader.GetString("Formador");
                        formacao.Cliente.Entidade = improvedSQLDataReader.GetString("Cliente");
                        lista.Add(formacao);
                    }
                }
                dataR.Close();
                if (lista.Count != 0)
                {
                    foreach (Formacao formacaoFetch in lista)
                    {
                        string cliente = formacaoFetch.Cliente.Entidade;
                        string formador = formacaoFetch.Formador.Username;
                        formacaoFetch.Formador = SelectUtilizador(formador);
                        formacaoFetch.Cliente = SelectCliente(cliente);
                    }
                }
                Conexao.Close();
                return lista;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return lista;
            }
        }
        public Formacao SelectFormacao(int id)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("Select * from Formacao inner join DiaFormacao on formacao.Id = DiaFormacao.Formacao Where Id=@Id", Conexao);
                commando.Parameters.AddWithValue("@Id", id);
                SqlDataReader dataReader = commando.ExecuteReader();
                if (dataReader.HasRows)
                {
                    string cliente = "";
                    string ultimoModificador = "";
                    string formador = "";
                    Formacao formacao = new Formacao();
                    ImprovedSQLDataReader improvedSQLDataReader = new ImprovedSQLDataReader(dataReader);
                    int idF = 0;
                    while (dataReader.Read())
                    {
                        if (improvedSQLDataReader.GetInt32("Id") != idF)
                        {
                            formacao = new Formacao()
                            {
                                Id = improvedSQLDataReader.GetInt32("Id"),
                                Categoria = improvedSQLDataReader.GetString("Categoria"),
                                DataInicioFormacao = improvedSQLDataReader.GetDateTime("DataInicioFormacao"),
                                Nome = improvedSQLDataReader.GetString("Nome"),
                                LocalFormacao = improvedSQLDataReader.GetString("LocalFormacao"),
                                ValorPagoFormador = improvedSQLDataReader.GetDouble("ValorPagoFormador")

                            };
                            formacao.Cliente.Entidade = improvedSQLDataReader.GetString("Cliente");
                            formacao.UltimoModificador.Username = improvedSQLDataReader.GetString("UltimoModificador");
                            formacao.Formador.Username = improvedSQLDataReader.GetString("Formador");
                            cliente = formacao.Cliente.Entidade;
                            ultimoModificador = formacao.UltimoModificador.Username;
                            formador = formacao.Formador.Username;
                            formacao.ListaDiaFormacao.Add(new DiaFormacao
                            {
                                Data = improvedSQLDataReader.GetDateTime("Data"),
                                HoraInicio = improvedSQLDataReader.GetTimeSpan("HoraInicio"),
                                HoraFim = improvedSQLDataReader.GetTimeSpan("HoraFim"),
                                Duracao = improvedSQLDataReader.GetInt32("Duracao")
                            });
                            idF = formacao.Id;
                        }
                        else
                        {
                            formacao.ListaDiaFormacao.Add(new DiaFormacao
                            {
                                Data = improvedSQLDataReader.GetDateTime("Data"),
                                HoraInicio = improvedSQLDataReader.GetTimeSpan("HoraInicio"),
                                HoraFim = improvedSQLDataReader.GetTimeSpan("HoraFim"),
                                Duracao = improvedSQLDataReader.GetInt32("Duracao")
                            });
                        }
                    }
                    dataReader.Close();
                    formacao.Cliente = SelectCliente(cliente);
                    formacao.Formador = SelectUtilizador(formador);
                    formacao.UltimoModificador = SelectUtilizador(ultimoModificador);
                    List<RecursoNaoConsumivel> naoConsumiveis = SelectRecursosNaoConsumiveis(formacao);
                    formacao.ListaNConsumiveis = naoConsumiveis;
                    dataReader.Close();
                    Conexao.Close();
                    return formacao;
                }
                dataReader.Close();
            }
            catch (Exception ex)
            {
            }
            Conexao.Close();
            return null;
        }
        public DataTable GetFormacoesForExcel(Utilizador user = null)
        {
            DataTable table = new DataTable();
            SqlCommand commando = new SqlCommand("", Conexao);
            if (user == null)
            {
                commando.CommandText = "Select * from Formacao inner join DiaFormacao on Formacao.Id=DiaFormacao.Formacao Order by Id DESC";
            }
            else
            {
                commando.CommandText = "Select * from Formacao inner join DiaFormacao on Formacao.Id=DiaFormacao.Formacao Where Formador = @Formador Order by Id DESC, DiaFormacao.Data";
                commando.Parameters.AddWithValue("@Formador", user.Username);
            }
            try
            {
                SqlDataAdapter dataAdapter = new SqlDataAdapter(commando);
                dataAdapter.Fill(table);
            }
            catch (Exception ex) { }
            return table;
        }
        public bool InsertFormacao(Formacao formacao)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("Insert into [Formacao] (Nome,DataInicioFormacao,Cliente,UltimoModificador,LocalFormacao,Formador,Categoria,ValorPagoFormador)" +
                                                        "values (@Nome,@DataInicioFormacao,@Cliente,@UltimoModificador,@LocalFormacao,@Formador,@Categoria,@ValorPagoFormador); " +
                                                        "SELECT SCOPE_IDENTITY()", Conexao);
                commando.Parameters.AddWithValue("@Nome", formacao.Nome);
                commando.Parameters.AddWithValue("@DataInicioFormacao", formacao.DataInicioFormacao);
                commando.Parameters.AddWithValue("@Cliente", formacao.Cliente.Entidade);
                commando.Parameters.AddWithValue("@UltimoModificador", formacao.UltimoModificador.Username);
                commando.Parameters.AddWithValue("@LocalFormacao", formacao.LocalFormacao);
                commando.Parameters.AddWithValue("@Formador", formacao.Formador.Username);
                commando.Parameters.AddWithValue("@Categoria", formacao.Categoria);
                commando.Parameters.AddWithValue("@ValorPagoFormador", formacao.ValorPagoFormador);
                int checker = 0;
                checker = Convert.ToInt32(commando.ExecuteScalar());
                formacao.Id = (int)checker;
                Conexao.Close();
                if (checker != 0)
                {
                    if (formacao.ListaDiaFormacao.Count != 0)
                    {
                        foreach (DiaFormacao diaFormacao in formacao.ListaDiaFormacao)
                        {
                            InsertDiaFormacao(diaFormacao, formacao);
                            if (formacao.ListaNConsumiveis.Count != 0)
                            {
                                foreach (RecursoNaoConsumivel naoConsumivel in formacao.ListaNConsumiveis)
                                {
                                    naoConsumivel._Disponibilidade = new Disponibilidade() { Data = diaFormacao.Data, Disponivel = false };
                                    InsertDisponibilidadeRecurso(naoConsumivel, formacao.Id);
                                }
                            }
                        }
                    }
                    if (formacao.ListaConsumivel.Count != 0)
                    {
                        foreach (RecursoConsumivel consumivel in formacao.ListaConsumivel)
                        {
                            InsertSaida(consumivel, consumivel.Saida);
                        }
                    }
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        public bool UpdateFormacao(Formacao formacao, Utilizador utilizador)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("UPDATE [Formacao] SET Nome = @Nome, DataInicioformacao = @DataInicioFormacao, Cliente = @Cliente, UltimoModificador = @UltimoModificador, " +
                                                        "LocalFormacao = @LocalFormacao, Formador = @Formador Where Id = @Id", Conexao);
                commando.Parameters.AddWithValue("@Id", formacao.Id);
                commando.Parameters.AddWithValue("@Nome", formacao.Nome);
                commando.Parameters.AddWithValue("@DataInicioFormacao", formacao.DataInicioFormacao);
                commando.Parameters.AddWithValue("@Cliente", formacao.Cliente.Entidade);
                commando.Parameters.AddWithValue("@UltimoModificador", formacao.UltimoModificador.Username);
                commando.Parameters.AddWithValue("@LocalFormacao", formacao.Id);
                commando.Parameters.AddWithValue("@Formador", formacao.Formador.Username);
                byte checker = 0;
                checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                if (checker != 0)
                {
                    DeleteDiasFormacao(formacao);
                    foreach (DiaFormacao diaFormacao in formacao.ListaDiaFormacao)
                    {
                        InsertDiaFormacao(diaFormacao, formacao);
                    }
                }
                return (checker != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        public bool UpdateDisponibilidadesFormacao(Formacao formacao)
        {
            bool check = true;
            check = DeleteDisponibilidadesRecursoFormacao(formacao);
            foreach (RecursoNaoConsumivel recursoNaoConsumivel in formacao.ListaNConsumiveis)
            {
                check = check ? InsertDisponibilidadeRecurso(recursoNaoConsumivel, formacao.Id) : false;
            }
            foreach (RecursoConsumivel recursoConsumivel in formacao.ListaConsumivel)
            {
                check = check ? InsertSaida(recursoConsumivel, recursoConsumivel.Saida) : false;
            }
            return check;
        }
        public bool DeleteFormacao(Formacao formacao)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = SqlCommandCreator.Delete(formacao, Conexao);
                commando.Parameters.AddWithValue("@Id", formacao.Id);
                byte checker = 0;
                checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                return (checker != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        #endregion
        #region Consumables
        public DataTable GetRecursosConsumiveis(string categoria = "%", string nome = "%")
        {
            DataTable dataTable = new DataTable();
            try
            {
                SqlCommand commando = new SqlCommand("Select * from [RecursoConsumivel] Where Categoria like @Categoria and Nome like @Nome", Conexao);
                commando.Parameters.AddWithValue("@Categoria", "%" + categoria + "%");
                commando.Parameters.AddWithValue("@Nome", "%" + nome + "%");
                SqlDataAdapter adapter = new SqlDataAdapter(commando);
                adapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conexao.Close();
            }
            return dataTable;
        }
        public List<RecursoConsumivel> SelectRecursosConsumiveis()
        {
            List<RecursoConsumivel> lista = new List<RecursoConsumivel>();
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = SqlCommandCreator.Select(new RecursoConsumivel(), Conexao);
                SqlDataReader dataReader = commando.ExecuteReader();
                if (dataReader.HasRows)
                {
                    ImprovedSQLDataReader improvedSQLDataReader = new ImprovedSQLDataReader(dataReader);
                    while (dataReader.Read())
                    {
                        RecursoConsumivel recursoConsumivel = new RecursoConsumivel()
                        {
                            Id = improvedSQLDataReader.GetInt32("Id"),
                            Nome = improvedSQLDataReader.GetString("Nome"),
                            Categoria = improvedSQLDataReader.GetString("Categoria"),
                            Quantidade = improvedSQLDataReader.GetInt32("Quantidade"),
                            ValorMinimo = improvedSQLDataReader.GetInt32("ValorMinimo")
                        };
                        lista.Add(recursoConsumivel);
                    }
                }
                dataReader.Close();
            }
            catch (Exception)
            {
            }
            Conexao.Close();
            return lista;
        }
        public RecursoConsumivel SelectRecursoConsumivel(int id)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = SqlCommandCreator.Select(new RecursoConsumivel(), Conexao);
                SqlCommandCreator.Where(commando, $"Id = {id}");
                SqlDataReader dataReader = commando.ExecuteReader();
                if (dataReader.HasRows)
                {
                    ImprovedSQLDataReader improvedSQLDataReader = new ImprovedSQLDataReader(dataReader);
                    while (dataReader.Read())
                    {
                        RecursoConsumivel recursoConsumivel = new RecursoConsumivel()
                        {
                            Id = improvedSQLDataReader.GetInt32("Id"),
                            Categoria = improvedSQLDataReader.GetString("Categoria"),
                            Nome = improvedSQLDataReader.GetString("Nome"),
                            Quantidade = improvedSQLDataReader.GetInt32("Quantidade"),
                            ValorMinimo = improvedSQLDataReader.GetInt32("ValorMinimo")

                        };
                        dataReader.Close();
                        Conexao.Close();
                        return recursoConsumivel;
                    }
                }
                dataReader.Close();
            }
            catch (Exception)
            {

                throw;
            }
            Conexao.Close();
            return null;
        }
        public bool InsertEntrada(int id_recurso, int valor)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commandoe = new SqlCommand("Insert into [Entrada] (Data,Recurso,Valor) values (@Data,@Recurso,@Valor)", Conexao);
                commandoe.Parameters.AddWithValue("@Recurso", id_recurso);
                commandoe.Parameters.AddWithValue("@Data", DateTime.Now);
                commandoe.Parameters.AddWithValue("@Valor", valor);
                byte checker = 0;
                checker = (byte)commandoe.ExecuteNonQuery();
                Conexao.Close();
                return (checker != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        public bool InsertSaida(RecursoConsumivel recursoConsumivel, int valor)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("Insert into [Saida] (Data,Recurso,Valor) values (@Data,@Recurso,@Valor)", Conexao);
                commando.Parameters.AddWithValue("@Recurso", recursoConsumivel.Id);
                commando.Parameters.AddWithValue("@Data", DateTime.Now);
                commando.Parameters.AddWithValue("@Valor", valor);
                byte checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                return (checker != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        public bool InsertRecursoConsumivel(RecursoConsumivel recursoConsumivel, int valor)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("Insert into [RecursoConsumivel] (Nome,Categoria,ValorMinimo) values (@Nome,@Categoria,@ValorMinimo); SELECT SCOPE_IDENTITY()", Conexao);
                commando.Parameters.AddWithValue("@Nome", recursoConsumivel.Nome);
                commando.Parameters.AddWithValue("@ValorMinimo", recursoConsumivel.ValorMinimo);
                commando.Parameters.AddWithValue("@Categoria", recursoConsumivel.Categoria);
                int id_recurso = Convert.ToInt32(commando.ExecuteScalar());
                InsertEntrada(id_recurso, valor);
                Conexao.Close();
                return (id_recurso != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        public bool UpdateRecursoConsumivel(RecursoConsumivel recursoConsumivel)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("Update [RecursoConsumivel] SET Nome = @Nome, Categoria = @Categoria, ValorMinimo = @ValorMinimo Where Id = @Id", Conexao);
                commando.Parameters.AddWithValue("@Id", recursoConsumivel.Id);
                commando.Parameters.AddWithValue("@Nome", recursoConsumivel.Nome);
                commando.Parameters.AddWithValue("@Categoria", recursoConsumivel.Categoria);
                commando.Parameters.AddWithValue("@ValorMinimo", recursoConsumivel.ValorMinimo);
                byte checker = 0;
                checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                return (checker != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        public bool DeleteRecursoConsumivel(RecursoConsumivel recursoConsumivel)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("Delete from RecursoConsumivel Where Id = @Id", Conexao);
                commando.Parameters.AddWithValue("@Id", recursoConsumivel.Id);
                byte checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                return (checker != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        #endregion
        #region NonConsumables
        public List<RecursoNaoConsumivel> SelectRecursosNaoConsumiveis(Formacao formacao = null)
        {
            List<RecursoNaoConsumivel> lista = new List<RecursoNaoConsumivel>();
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando;
                if (formacao == null)
                {
                    commando = SqlCommandCreator.Select(new RecursoNaoConsumivel(), Conexao);
                }
                else
                {
                    commando = new SqlCommand("select RecursoNaoConsumivel.Id, RecursoNaoConsumivel.Nome, RecursoNaoConsumivel.Categoria from RecursoNaoConsumivel " +
                    "inner join DisponibilidadeRecurso on DisponibilidadeRecurso.Recurso = RecursoNaoConsumivel.Id " +
                    "Where Formacao = @Formacao Group by RecursoNaoConsumivel.Id, RecursoNaoConsumivel.Nome, RecursoNaoConsumivel.Categoria ", Conexao);
                    commando.Parameters.AddWithValue("@Formacao", formacao.Id);
                }
                SqlDataReader dataReader = commando.ExecuteReader();
                if (dataReader.HasRows)
                {
                    ImprovedSQLDataReader improvedSQLDataReader = new ImprovedSQLDataReader(dataReader);
                    while (dataReader.Read())
                    {
                        RecursoNaoConsumivel recursoNaoConsumivel = new RecursoNaoConsumivel()
                        {
                            Id = improvedSQLDataReader.GetInt32("Id"),
                            Nome = improvedSQLDataReader.GetString("Nome"),
                            Categoria = improvedSQLDataReader.GetString("Categoria")
                        };
                        lista.Add(recursoNaoConsumivel);
                    }
                }
                dataReader.Close();
            }
            catch (Exception ex)
            {
            }
            Conexao.Close();
            return lista;
        }
        public DataTable GetRecursosNaoConsumiveis(string categoria = "%", string nome = "%")
        {
            DataTable dataTable = new DataTable();
            try
            {
                SqlCommand commando = new SqlCommand($"SELECT * FROM [RecursoNaoConsumivel] Where Categoria like @Categoria and Nome like @Nome", Conexao);
                commando.Parameters.AddWithValue("@Categoria", "%" + categoria + "%");
                commando.Parameters.AddWithValue("@Nome", "%" + nome + "%");
                SqlDataAdapter adapter = new SqlDataAdapter(commando);
                adapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (Conexao.State == ConnectionState.Open) Conexao.Close();
            }
            return dataTable;
        }
        public DataTable GetRecursosNaoConsumiveis(List<DateTime> dateTimes, string nome = "%")
        {
            DataTable dataTable = new DataTable();
            try
            {
                string datas = "";
                foreach (DateTime dateTime in dateTimes)
                {
                    datas += dateTime.ToShortDateString() + ", ";
                }
                datas = datas.Remove(datas.Length - 2, 2);
                DateTime[] dateTimes1 = dateTimes.ToArray();
                SqlCommand commando = new SqlCommand();
                commando.Connection = Conexao;
                commando.CommandText = $"Select R1.Id, R1.Nome from [RecursoNaoConsumivel] as R1 " +
                    $"Where R1.Id not in (SELECT R2.Id FROM [RecursoNaoConsumivel] as R2 " +
                    $"left join [DisponibilidadeRecurso] on " +
                    $"DisponibilidadeRecurso.Recurso = R2.Id Where [DisponibilidadeRecurso].Data in (";
                byte count = 0;
                foreach (DateTime dateTime in dateTimes)
                {
                    commando.CommandText += $"@Data{count}, ";
                    count++;
                }
                commando.CommandText = commando.CommandText.Remove(commando.CommandText.Length - 2, 2);
                commando.CommandText += ") and Nome like @Nome " +
                    $"Group by R2.Id)";
                count = 0;
                foreach (DateTime date in dateTimes)
                {
                    commando.Parameters.AddWithValue($"@Data{count}", date.Date);
                    count++;
                }
                commando.Parameters.AddWithValue("@Nome", "%" + nome + "%");
                SqlDataAdapter adapter = new SqlDataAdapter(commando);
                adapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (Conexao.State == ConnectionState.Open) Conexao.Close();
            }
            return dataTable;
        }
        public RecursoNaoConsumivel SelectRecursoNaoConsumivel(int id)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = SqlCommandCreator.Select(new RecursoNaoConsumivel(), Conexao);
                SqlCommandCreator.Where(commando, $"Id = {id}");
                SqlDataReader dataReader = commando.ExecuteReader();
                if (dataReader.HasRows)
                {
                    ImprovedSQLDataReader improvedSQLDataReader = new ImprovedSQLDataReader(dataReader);
                    while (dataReader.Read())
                    {
                        RecursoNaoConsumivel recursoNaoConsumivel = new RecursoNaoConsumivel()
                        {
                            Id = improvedSQLDataReader.GetInt32("Id"),
                            Categoria = improvedSQLDataReader.GetString("Categoria"),
                            Nome = improvedSQLDataReader.GetString("Nome")
                        };
                        dataReader.Close();
                        Conexao.Close();
                        return recursoNaoConsumivel;
                    }
                }
                dataReader.Close();
            }
            catch (Exception ex)
            {
            }
            Conexao.Close();
            return null;
        }
        public bool InsertRecursoNaoConsumivel(RecursoNaoConsumivel recursoNaoConsumivel)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("Insert into [RecursoNaoConsumivel] (Nome,Categoria) values (@Nome,@Categoria)", Conexao);
                commando.Parameters.AddWithValue("@Nome", recursoNaoConsumivel.Nome);
                commando.Parameters.AddWithValue("@Categoria", recursoNaoConsumivel.Categoria);
                byte checker = 0;
                checker = (byte)commando.ExecuteNonQuery();
                //InsertDisponibilidadeRecurso(recursoNaoConsumivel);
                Conexao.Close();
                return (checker != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        public bool UpdateRecursoNaoConsumivel(RecursoNaoConsumivel recursoNaoConsumivel)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("UPDATE [RecursoNaoConsumivel] SET Nome = @Nome, Categoria = @Categoria Where Id = @Id ", Conexao);
                commando.Parameters.AddWithValue("@Id", recursoNaoConsumivel.Id);
                commando.Parameters.AddWithValue("@Nome", recursoNaoConsumivel.Nome);
                commando.Parameters.AddWithValue("@Categoria", recursoNaoConsumivel.Categoria);
                byte checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                return (checker != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        public bool DeleteRecursoNaoConsumivel(RecursoNaoConsumivel recursoNaoConsumivel)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = SqlCommandCreator.Delete(recursoNaoConsumivel, Conexao);
                commando.Parameters.AddWithValue("@Id", recursoNaoConsumivel.Id);
                byte checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                return (checker != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        public List<Disponibilidade> SelectIndisponibilidadeRecurso(RecursoNaoConsumivel recursoNaoConsumivel)
        {
            List<Disponibilidade> lista = new List<Disponibilidade>();
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("Select * from [DisponibilidadeRecurso] Where Recurso = @Recurso and Disponivel = 0", Conexao);
                commando.Parameters.AddWithValue("@Recurso", recursoNaoConsumivel.Id);
                SqlDataReader dataReader = commando.ExecuteReader();
                if (dataReader.HasRows)
                {
                    ImprovedSQLDataReader improvedSQLDataReader = new ImprovedSQLDataReader(dataReader);
                    while (dataReader.Read())
                    {
                        Disponibilidade disponibilidade = new Disponibilidade()
                        {
                            Data = improvedSQLDataReader.GetDateTime("Data"),
                            Disponivel = improvedSQLDataReader.GetBool("Disponivel")
                        };
                        lista.Add(disponibilidade);
                    }
                    dataReader.Close();
                }
                dataReader.Close();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conexao.Close();
            }
            return lista;
        }
        public bool InsertDisponibilidadeRecurso(RecursoNaoConsumivel recursoNaoConsumivel, int? idFormacao = null)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("Insert into [DisponibilidadeRecurso] (Recurso,Data,Disponivel,Formacao) values (@Recurso,@Data,@Disponivel,@Formacao)", Conexao);
                commando.Parameters.AddWithValue("@Recurso", recursoNaoConsumivel.Id);
                commando.Parameters.AddWithValue("@Data", recursoNaoConsumivel._Disponibilidade.Data);
                commando.Parameters.AddWithValue("@Disponivel", recursoNaoConsumivel._Disponibilidade.Disponivel);
                commando.Parameters.AddWithValue("@Formacao", idFormacao);
                byte checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                return (checker != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        public void ExecDisponibilidadesResource(RecursoNaoConsumivel naoConsumivel, DateTime date)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("exec IndisPonibilidadesRecursos @Data, @Resource", Conexao);
                commando.Parameters.AddWithValue("@Data", date.Date);
                commando.Parameters.AddWithValue("@Resource", naoConsumivel.Id);
                commando.ExecuteNonQuery();
                Conexao.Close();
            }
            catch (Exception ex)
            {
                Conexao.Close();
            }
        }
        public bool UpdateDisponibilidadeRecurso(RecursoNaoConsumivel recursoNaoConsumivel)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("UPDATE [DisponibilidadeRecurso] SET Disponivel = @Disponivel Where Recurso = @Recurso AND Data = @Data", Conexao);
                commando.Parameters.AddWithValue("@Disponivel", recursoNaoConsumivel._Disponibilidade.Disponivel);
                commando.Parameters.AddWithValue("@Data", recursoNaoConsumivel._Disponibilidade.Data);
                commando.Parameters.AddWithValue("@Recurso", recursoNaoConsumivel.Id);
                byte checker = (byte)commando.ExecuteNonQuery();
                Conexao.Close();
                return (checker != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Conexao.Close();
                return false;
            }
        }
        public bool DeleteDisponibilidadesRecursoFormacao(Formacao formacao)
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand command = new SqlCommand("Delete from DisponibilidadeRecurso Where Formacao = @Formacao", Conexao);
                command.Parameters.AddWithValue("@Formacao", formacao.Id);
                byte checker = (byte)command.ExecuteNonQuery();
                Conexao.Close();
                return checker != 0 ? true : false;
            }
            catch (Exception)
            {
                Conexao.Close();
            }
            return false;
        }
        #endregion
        #region Logins
        public async Task InsertLoginLogAsync(Utilizador utilizador, string IPAddess = "169.254.255.254")
        {
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("exec LogLoginProc @Utilizador, @IP", Conexao);
                commando.Parameters.AddWithValue("@Utilizador", utilizador.Username);
                commando.Parameters.AddWithValue("@IP", IPAddess);
                await Task.Run(() => commando.ExecuteNonQuery());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
            finally
            {
                Conexao.Close();
            }

        }
        #endregion
        #region saidasEntradas
        public Dictionary<string, int> saidasMes()
        {
            Dictionary<string, int> gastoMensal = new Dictionary<string, int>();
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                string queryString = "select RecursoConsumivel.Nome as Nome, sum(Saida.Valor) as Quantidadegasta from Saida inner join " +
                    "RecursoConsumivel on Saida.Recurso = RecursoConsumivel.Id where month(Data) = MONTH(getdate()) Group by RecursoConsumivel.Nome";
                SqlCommand command = new SqlCommand(queryString, Conexao);
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.HasRows)
                {
                    ImprovedSQLDataReader improvedSQLDataReader = new ImprovedSQLDataReader(dataReader);
                    while (dataReader.Read())
                    {
                        gastoMensal.Add(improvedSQLDataReader.GetString("Nome"), improvedSQLDataReader.GetInt32("Quantidadegasta"));
                    }
                }
                dataReader.Close();
            }
            catch (Exception ex)
            {
            }
            Conexao.Close();
            return gastoMensal;
        }
        #endregion
        #region entradasEntradas
        public Dictionary<string, int> entradasMes()
        {
            Dictionary<string, int> entradaMensal = new Dictionary<string, int>();
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                string queryString = "select RecursoConsumivel.Nome, sum(Entrada.Valor) as 'Quantidadeadquirida' from Entrada inner join " +
                    "RecursoConsumivel on Entrada.Recurso = RecursoConsumivel.Id where month(Data) = MONTH(getdate()) Group by RecursoConsumivel.Nome";
                SqlCommand command = new SqlCommand(queryString, Conexao);
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.HasRows)
                {
                    ImprovedSQLDataReader improvedSQLDataReader = new ImprovedSQLDataReader(dataReader);
                    while (dataReader.Read())
                    {
                        entradaMensal.Add(improvedSQLDataReader.GetString("Nome"), improvedSQLDataReader.GetInt32("Quantidadeadquirida"));
                    }
                }
                dataReader.Close();
            }
            catch (Exception ex)
            {
            }
            Conexao.Close();
            return entradaMensal;
        }
        #endregion
        #region Logins
        public Dictionary<DateTime, int> LoginsMes(DateTime? date = null)
        {
            Dictionary<DateTime, int> logins = new Dictionary<DateTime, int>();
            try
            {
                if (Conexao.State != ConnectionState.Open) Conexao.Open();
                SqlCommand commando = new SqlCommand("exec LoginsAutdit @Date", Conexao);
                if (date != null)
                {
                    commando.Parameters.AddWithValue("@Date", date);
                }
                else
                {
                    commando.CommandText = "exec LoginsAutdit";
                }
                SqlDataReader dataReader = commando.ExecuteReader();
                if (dataReader.HasRows)
                {
                    ImprovedSQLDataReader improvedSQLDataReader = new ImprovedSQLDataReader(dataReader);
                    while (dataReader.Read())
                    {
                        if (date != null)
                        {
                            logins.Add(new DateTime(1, improvedSQLDataReader.GetInt32("Mes"), 1), improvedSQLDataReader.GetInt32("NrLogins"));
                        }
                        else
                        {
                            logins.Add(new DateTime(improvedSQLDataReader.GetInt32("Ano"), improvedSQLDataReader.GetInt32("Mes"), 1), improvedSQLDataReader.GetInt32("NrLogins"));
                        }
                    }
                }
                dataReader.Close();
            }
            catch (Exception ex)
            {
            }
            Conexao.Close();
            return logins;
        }
        #endregion
    }
}
