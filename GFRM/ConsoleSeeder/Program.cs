﻿using System;
using System.Collections.Generic;
using Class_Library;

namespace ConsoleSeeder
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Escreve 1 | 2 | 3 | 4");
            int input = int.Parse(Console.ReadLine());
            DAL dAL = new DAL("Data Source=127.0.0.1;Initial Catalog=projectoFinal;Persist Security Info=True;User ID=tester;Password=123+qwe");

            switch (input)
            {
                case 1:
                    #region categorias
                    bool mecanica = dAL.InsertCategoria("Mecanica");
                    Console.WriteLine("Mecanica - " + mecanica);
                    bool electrotecnica = dAL.InsertCategoria("Electrotecnica");
                    Console.WriteLine("Electrotecnica - " + electrotecnica);
                    bool informatica = dAL.InsertCategoria("Informatica");
                    Console.WriteLine("Informatica - " + informatica);
                    bool mecatronica = dAL.InsertCategoria("Mecatronica");
                    Console.WriteLine("Mecatronica - " + mecatronica);
                    bool soldadura = dAL.InsertCategoria("Soldadura");
                    Console.WriteLine("Soldadura - " + soldadura);
                    #endregion
                    #region permissoes
                    bool admin = dAL.InsertPermissao("Admin");
                    Console.WriteLine("Permissao Admin - " + admin);
                    bool coordenador = dAL.InsertPermissao("Coordenador");
                    Console.WriteLine("Permissao Coordenador - " + coordenador);
                    bool formador = dAL.InsertPermissao("Formador");
                    Console.WriteLine("Permissao Formador - " + formador);
                    bool worker = dAL.InsertPermissao("Worker");
                    Console.WriteLine("Permissao Worker - " + worker);
                    #endregion
                    #region clientes
                    bool x123 = dAL.InsertCliente(new Cliente() { Entidade = "X123", Contacto = "X123@mail", Morada = "Rua do X123", Nome = "x123 Lda." });
                    Console.WriteLine("Cliente X123 - " + x123);
                    bool y456 = dAL.InsertCliente(new Cliente() { Entidade = "Y456", Contacto = "Y456@mail", Morada = "Rua do Y456", Nome = "Y456 Lda." });
                    Console.WriteLine("Cliente Y456 - " + y456);
                    bool z789 = dAL.InsertCliente(new Cliente() { Entidade = "Z789", Contacto = "Z789@mail", Morada = "Rua do Z789", Nome = "Z789 Lda." });
                    Console.WriteLine("Cliente Z789 - " + z789);
                    bool a321 = dAL.InsertCliente(new Cliente() { Entidade = "A321", Contacto = "A321@mail", Morada = "Rua do A321", Nome = "A321 Lda." });
                    Console.WriteLine("Cliente A321 - " + a321);
                    bool b654 = dAL.InsertCliente(new Cliente() { Entidade = "B654", Contacto = "B654@mail", Morada = "Rua do B654", Nome = "B654 Lda." });
                    Console.WriteLine("Cliente B654 - " + b654);
                    bool c987 = dAL.InsertCliente(new Cliente() { Entidade = "C987", Contacto = "C987@mail", Morada = "Rua do C987", Nome = "C987 Lda." });
                    Console.WriteLine("Cliente C987 - " + c987);
                    bool d000 = dAL.InsertCliente(new Cliente() { Entidade = "D000", Contacto = "D000@mail", Morada = "Rua do D000", Nome = "D000 Lda." });
                    Console.WriteLine("Cliente D000 - " + d000);
                    #endregion
                    #region utilizadores
                    List<string> categorias = new List<string>();
                    categorias.Add("Mecanica");
                    categorias.Add("Electrotecnica");
                    categorias.Add("Informatica");
                    Utilizador adminUser = new Utilizador("123+qwe") { Nome = "Admin", Username = "Admin", Email = "Admin@mail.com", Categorias = categorias, Permissao = "Admin" };
                    bool adminUserInserted = dAL.InsertUtilizador(adminUser);
                    Console.WriteLine("User Administrador - " + adminUserInserted);
                    Utilizador coordenadorUser = new Utilizador("123+qwe") { Nome = "Coordenador", Username = "Coordenador", Categorias = categorias, Email = "Coordenador@mail.com", Permissao = "Coordenador" };
                    bool coordenadorUserInserted = dAL.InsertUtilizador(coordenadorUser);
                    Console.WriteLine("User Coordenador - " + coordenadorUserInserted);
                    Utilizador formadorUser = new Utilizador("123+qwe") { Nome = "Formador", Username = "Formador", Categorias = categorias, Email = "Formador@mail.com", Permissao = "Formador" };
                    dAL.InsertUtilizador(formadorUser);
                    Utilizador formadorUser4 = new Utilizador("123+qwe") { Nome = "Formador2", Username = "Formador2", Categorias = categorias, Email = "Formador2@mail.com", Permissao = "Formador" };
                    dAL.InsertUtilizador(formadorUser4);
                    Utilizador formadorUser2 = new Utilizador("123+qwe") { Nome = "Formador3", Username = "Formador3", Categorias = categorias, Email = "Formador3@mail.com", Permissao = "Formador" };
                    dAL.InsertUtilizador(formadorUser2);
                    Utilizador formadorUser3 = new Utilizador() { Nome = "Formador4", Username = "Formador4", Categorias = categorias, Email = "Formador4@mail.com", Permissao = "Formador" };
                    dAL.InsertUtilizador(formadorUser3);
                    #endregion
                    #region recurso nao consumivel
                    bool recursoNaoConsumivel1 = dAL.InsertRecursoNaoConsumivel(new RecursoNaoConsumivel() { Nome = "Maquina 1", Categoria = "Mecanica", });
                    Console.WriteLine("Maquina 1 - " + recursoNaoConsumivel1);
                    dAL.InsertRecursoNaoConsumivel(new RecursoNaoConsumivel { Categoria = "Informatica", Nome = "Aquele PC" });
                    dAL.InsertRecursoNaoConsumivel(new RecursoNaoConsumivel { Categoria = "Mecanica", Nome = "Aquela Ferramenta" });
                    dAL.InsertRecursoNaoConsumivel(new RecursoNaoConsumivel { Categoria = "Mecanica", Nome = "A Outra Ferramenta" });
                    dAL.InsertRecursoNaoConsumivel(new RecursoNaoConsumivel { Categoria = "Informatica", Nome = "O outro PC" });
                    dAL.InsertRecursoNaoConsumivel(new RecursoNaoConsumivel { Categoria = "Mecanica", Nome = "Mais Ferramentas" });
                    dAL.InsertRecursoNaoConsumivel(new RecursoNaoConsumivel { Categoria = "Informatica", Nome = "Mais Pcs" });
                    dAL.InsertRecursoNaoConsumivel(new RecursoNaoConsumivel { Categoria = "Soldadura", Nome = "Solda de Soldar" });
                    #endregion
                    #region disponibilidade Recurso Nao Consumivel
                    List<RecursoNaoConsumivel> recursoNaoConsumivels = dAL.SelectRecursosNaoConsumiveis();
                    #endregion
                    #region recurso Consumivel
                    bool recursoConsumivel1 = dAL.InsertRecursoConsumivel(new RecursoConsumivel() { Nome = "Cabo 1", Categoria = "Informatica", ValorMinimo = 10 }, 20);
                    Console.WriteLine("Cabo 1 - " + recursoConsumivel1);
                    dAL.InsertRecursoConsumivel(new RecursoConsumivel { Nome = "Cabo 2", Categoria = "Informatica", ValorMinimo = 100 }, 50);
                    dAL.InsertRecursoConsumivel(new RecursoConsumivel { Nome = "Cabo 3", Categoria = "Informatica", ValorMinimo = 10 }, 30);
                    dAL.InsertRecursoConsumivel(new RecursoConsumivel { Nome = "Cabo 4", Categoria = "Informatica", ValorMinimo = 10 }, 10);
                    dAL.InsertRecursoConsumivel(new RecursoConsumivel { Nome = "Óleo 1", Categoria = "Mecanica", ValorMinimo = 10 }, 20);
                    dAL.InsertRecursoConsumivel(new RecursoConsumivel { Nome = "Óleo 2", Categoria = "Mecanica", ValorMinimo = 10 }, 40);
                    dAL.InsertRecursoConsumivel(new RecursoConsumivel { Nome = "Óleo 3", Categoria = "Mecanica", ValorMinimo = 10 }, 60);
                    #endregion
                    Console.WriteLine("DONE");
                    break;
                case 2:
                    bool exit = false;
                    while (!exit)
                    {
                        Console.Clear();
                        exit = false;
                        Console.WriteLine("Email:");
                        bool valid = Auxiliar.ValidateEmail(Console.ReadLine());
                        Console.WriteLine(valid.ToString());
                        Console.WriteLine("Exit?(y/n)");
                        string inputLine = Console.ReadLine();
                        if (inputLine == "y")
                        {
                            exit = true;
                        }
                    }
                    break;
                case 3:
                    Cliente cliente = new DAL().SelectCliente("D000");
                    Console.WriteLine(cliente.Entidade);
                    break;
                case 4:

                    Formacao formacao = dAL.SelectFormacao(1);
                    Utilizador utilizador = dAL.SelectUtilizador("Admin");
                    formacao.Nome = "Formacao Editada";
                    bool updated = dAL.UpdateFormacao(formacao, utilizador);
                    Console.WriteLine(updated);
                    break;
                default:
                    break;
            }
            Console.ReadKey();
        }
    }
}
