use master
GO
use projectoFinal
GO
select * from Categoria
GO
select * from Cliente
GO
select * from Permissao
GO
select * from Utilizador
GO
select * from DisponibilidadeUtilizador
GO
select * from DisponibilidadeRecurso
GO
select * from RecursoNaoConsumivel
GO
select * from RecursoConsumivel
GO
select * from Entrada
GO
select * from Saida
GO
select * from Formacao
GO
select * from DiaFormacao
GO
select * from CategoriaUtilizador
GO
select * from LoginLogs
GO
select * from Formacao inner join DiaFormacao on Formacao.Id = DiaFormacao.Formacao



select * from DisponibilidadeUtilizador as DU1
inner join Utilizador on DU1.Utilizador=Utilizador.Username
Where DU1.Data not in(select Data from DiaFormacao 
inner join Formacao on Formacao.Id = DiaFormacao.Formacao
Where Formador='Admin'
) and Utilizador ='Admin'

select * from DisponibilidadeUtilizador as DU1
inner join Utilizador on DU1.Utilizador=Utilizador.Username
Where DU1.Data in(select Data from DiaFormacao 
inner join Formacao on Formacao.Id = DiaFormacao.Formacao
Where Formador='Admin'
) and Utilizador ='Admin'

select * from DisponibilidadeUtilizador
Where DisponibilidadeUtilizador.Utilizador='Admin'
