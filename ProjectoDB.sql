use master
GO
drop database projectoFinal
GO
create database projectoFinal
GO
use projectoFinal
GO
/****** Object:  User [tester]    Script Date: 21/01/2020 22:12:41 ******/
CREATE USER [tester] FOR LOGIN [tester] WITH DEFAULT_SCHEMA=[db_owner]
GO
-- GFM
if OBJECT_ID('Utilizador') is null
create table Utilizador(
	Username varchar(255),
	Nome varchar(255) not null,
	Email varchar(255) not null,
	Password varchar(255) not null,
	Permissao varchar(50),
	FirstPassword varchar(255),
	constraint PK_Utilizador_Username primary key(Username),
	constraint UK_Utilizador_Email Unique(Email)
)
GO
if OBJECT_ID('Permissao') is null
create table Permissao(
	Nome varchar(50),
	constraint PK_Permissao_Nome primary key(Nome)
)
GO
if OBJECT_ID('CategoriaUtilizador') is null
create table CategoriaUtilizador(
	Utilizador varchar(255),
	Categoria varchar(255),
)
GO
if OBJECT_ID('Categoria') is null
create table Categoria(
	Nome varchar(255),
	constraint PK_Categoria_Nome primary key(Nome)
)
GO
if OBJECT_ID('Formacao') is null
create table Formacao(
	Id int identity,
	Nome varchar(255) not null,
	DataInicioFormacao date,
	Cliente varchar(255),
	UltimoModificador varchar(255),
	ValorPagoFormador float,
	Categoria varchar(255),
	LocalFormacao varchar(255),
	Formador varchar(255),
	constraint PK_Formacao_Id primary key(Id)
)
GO
if OBJECT_ID('DiaFormacao') is null
create table DiaFormacao(
	Data date not null,
	HoraInicio time not null,
	HoraFim time not null,
	Duracao int not null,
	Formacao int,
	constraint PK_DiaFormacao_Id primary key(Data,Formacao) 
)
GO
if OBJECT_ID('DisponibilidadeUtilizador') is null
create table DisponibilidadeUtilizador(
	Utilizador varchar(255) not null,
	Data date not null,
	Disponivel bit,
	constraint PK_DisponibilidadeUtilizador_Utilizador_Data primary key(Utilizador,Data)
)
GO
if OBJECT_ID('Cliente') is null
create table Cliente(
	Entidade varchar(255),
	Nome varchar(255),
	Contacto varchar(255),
	Morada varchar(255),
	constraint PK_Cliente_Id primary key(Entidade)
)
GO

-- Auditoria GFM
if OBJECT_ID('LogLogin') is null
create table LogLogin(
	Id int identity(1,1) primary key,
	Utilizador varchar(255),
	IPaddress varchar(255),
	Hora time default convert(time,getdate()),
	Data date default GetDate(),
	constraint FK_LogLogin_Utilizador foreign key(Utilizador) references Utilizador(Username) on update cascade on delete set null
)
GO
if OBJECT_ID('Auditoria') is null
create table Auditoria(
	Id int identity(1,1) primary key,
	Utilizador varchar(255),
	ActionType varchar(255),
	AffectedTable varchar(255),
	BeforeJSON varchar(max),
	CreatedAt date default GetDate(),
	constraint FK_Audit_Utilizador foreign key(Utilizador) references Utilizador(Username)
)
GO
-- GRFM
if OBJECT_ID('RecursoNaoConsumivel') is null
create table RecursoNaoConsumivel(
	Id int identity,
	Nome varchar(255) not null,
	Categoria varchar(255) ,
	constraint PK_RecursoNaoconsumivel_Id primary key(Id)
)
GO
if OBJECT_ID('RecursoConsumivel') is null
create table RecursoConsumivel(
	Id int identity,
	Nome varchar(255) not null,
	Categoria varchar(255),
	Quantidade int default 0,
	ValorMinimo int default 0,
	constraint PK_RecursoConsumivel_Id primary key(Id)
)
GO
if OBJECT_ID('DisponibilidadeRecurso') is null
create table DisponibilidadeRecurso(
	Recurso int not null,
	Data date not null,
	Formacao int,
	Disponivel bit,
	constraint PK_DisponibilidadeRecurso_Recruso_Data primary key(Recurso,Data)
)
GO
if OBJECT_ID('Entrada') is null
create table Entrada(
	Id int identity,
	Data date,
	Recurso int,
	Valor int,
	constraint PK_Entrada_Id primary key(Id)
)
GO
if OBJECT_ID('Saida') is null
create table Saida(
	Id int identity,
	Data date,
	Recurso int,
	Valor int,
	constraint PK_Saida_Id primary key(Id)
)
GO

-- Constraints GFM
alter table Utilizador
add constraint FK_Utilizador_Permissao foreign key(Permissao) references Permissao(Nome) on update cascade on delete set null
GO
alter table CategoriaUtilizador
add constraint FK_CategoriaUtilizador_Categoria foreign key(Categoria) references Categoria(Nome) on update cascade on delete cascade
GO
alter table CategoriaUtilizador
add constraint FK_CategoriaUtilizador_Utilizador foreign key(Utilizador) references Utilizador(Username) on update cascade on delete cascade
GO
alter table DisponibilidadeUtilizador
add constraint FK_DisponibilidadeUtilizador_Utilizador foreign key(Utilizador) references Utilizador(Username) on update cascade on delete cascade
GO
alter table Formacao
add constraint FK_Formacao_Cliente foreign key(Cliente) references Cliente(Entidade) on update cascade on delete set null
GO
alter table Formacao
add constraint FK_Formacao_UltimoModificador foreign key(UltimoModificador) references Utilizador(Username) on update cascade on delete set null
GO
alter table Formacao
add constraint FK_Formacao_Categoria foreign key(Categoria) references Categoria(Nome) on update no action on delete no action
alter table Formacao
add constraint FK_Formacao_Formador foreign key(Formador) references Utilizador(Username) on update no action on delete no action
GO
alter table DiaFormacao
add constraint FK_DiaFormacao_Formacao foreign key(Formacao) references Formacao(Id) on update cascade on delete cascade
GO

-- Constraints GRFM
alter table RecursoNaoConsumivel
add constraint FK_RecursoNaoConsumivel_Categoria foreign key(Categoria) references Categoria(Nome) on update cascade on delete set null
GO
alter table RecursoConsumivel
add constraint FK_RecursoConsumivel_Categoria foreign key(Categoria) references Categoria(Nome) on update cascade on delete set null
GO
alter table DisponibilidadeRecurso
add constraint FK_DisponibilidadeRecurso_Recurso foreign key(Recurso) references RecursoNaoConsumivel(Id) on update cascade on delete cascade
GO
alter table DisponibilidadeRecurso
add constraint FK_DisponibilidadeRecurso_Formacao foreign key(Formacao) references Formacao(Id) on update no action on delete cascade
GO
alter table Entrada
add constraint FK_Entrada_Recurso foreign key(Recurso) references RecursoConsumivel(Id)on update cascade on delete cascade
GO
alter table Saida
add constraint FK_Saida_Recurso foreign key(Recurso) references RecursoConsumivel(Id)on update cascade on delete cascade
GO

--TRIGGERS--
if OBJECT_ID('Entradas') is not null
	drop Trigger Entradas
GO
Create Trigger Entradas
ON Entrada
After INSERT as
declare @id Int, @valor Int
set @id = (Select Recurso From inserted)
set @valor = (Select Valor From inserted)
Update RecursoConsumivel Set Quantidade = Quantidade + @valor
Where RecursoConsumivel.Id = @id
GO
if OBJECT_ID('Saidas') is not null
	drop Trigger Saidas
GO
Create Trigger Saidas
ON Saida
After INSERT as
declare @id Int, @valor Int
set @id = (Select Recurso From inserted)
set @valor = (Select Valor From inserted)
Update RecursoConsumivel Set Quantidade = Quantidade - @valor
Where RecursoConsumivel.Id = @id
GO
if OBJECT_ID('IndisponibilizarUtilizador') is not null
	drop Trigger IndisponibilizarUtilizador
GO
Create Trigger IndisponibilizarUtilizador
on DiaFormacao
after Insert, update, delete as
	declare @Data date, @Username varchar(255)
	set @Data = (Select inserted.Data from inserted)
	set @Username = (select Formacao.Formador from Formacao inner join inserted on Formacao.Id=inserted.Formacao)
if exists (select * from inserted) and not exists (select * from deleted)
	begin
	insert into DisponibilidadeUtilizador(Data,Utilizador,Disponivel) values (@Data,@Username,0)
	end
if exists (select * from deleted) and not exists (select * from inserted)
	begin
	delete from DisponibilidadeUtilizador Where Utilizador = @Username and Data = @Data
	end
GO
if OBJECT_ID('IndisponibilizarRecursoNConsumivel') is not null
	drop Trigger IndisponibilizarRecursoNConsumivel
GO
Create Trigger IndisponibilizarRecursoNConsumivel
on Formacao after delete as
	declare @Formacao int
	set @Formacao = (Select Id from deleted)
	Delete from DisponibilidadeRecurso Where Formacao= @Formacao
GO
if OBJECT_ID('DeleteUtilizador') is not null
	drop Trigger DeleteUtilizador
GO
Create Trigger DeleteUtilizador on Utilizador 
after delete as 
declare @Username varchar(255)
set @Username = (Select Username from deleted)
delete from DisponibilidadeUtilizador Where DisponibilidadeUtilizador.Utilizador = @Username
delete from CategoriaUtilizador where CategoriaUtilizador.Utilizador = @Username
GO

-- Store Procedures
if OBJECT_ID('FormacoesFormador') is null
	exec('create procedure FormacoesFormador as set nocount on;')
GO
alter proc FormacoesFormador @SessionFormador varchar(250) as 
	Select * from Formacao
	inner join DiaFormacao on DiaFormacao.Formacao = Formacao.Id
	Where Formador= @SessionFormador
	Order by Formacao.Id DESC
GO
if OBJECT_ID('FormacaoFormadorDetails') is null
	exec('create procedure FormacaoFormadorDetails as set nocount on;')
GO
alter proc FormacaoFormadorDetails @Formacao int, @Formador varchar(250) as 
	Select Formacao.Nome,Formacao.Categoria,Formacao.LocalFormacao,Cliente.Nome,Formacao.DataInicioFormacao 
	From Formacao inner join Cliente on Cliente.Entidade = Formacao.Cliente
	Where Formacao.Formador = @Formador
GO
if OBJECT_ID('FormacaoFormadorDays') is null
	exec('create procedure FormacaoFormadorDays as set nocount on;')
GO
alter proc FormacaoFormadorDays @Formacao int, @Formador varchar(250)
as
	Select * from DiaFormacao 
	inner join Formacao on DiaFormacao.Formacao=Formacao.Id
	Where Formacao.Formador = @Formador
	Order by Formacao.Id DESC
GO
if OBJECT_ID('FormacaoRecursosNoConsume') is null
	exec('create procedure FormacaoRecursosNoConsume as set nocount on;')
GO
alter proc FormacaoRecursosNoConsume @Formacao int as
	Select * from RecursoNaoConsumivel 
	inner join DisponibilidadeRecurso on RecursoNaoConsumivel.Id=DisponibilidadeRecurso.Recurso
	Where DisponibilidadeRecurso.Formacao = @Formacao
GO
if OBJECT_ID('IndisponibilidadesPessoais') is null
	exec('create procedure IndisponibilidadesPessoais as set nocount on;')
GO
alter proc IndisponibilidadesPessoais @Data date, @User varchar(255) as
if exists (select * from DisponibilidadeUtilizador Where Data=@Data and Utilizador=@User)
begin
	delete from DisponibilidadeUtilizador where Data=@Data and Utilizador=@User
end
else
begin
	insert into DisponibilidadeUtilizador (Data,Utilizador,Disponivel) values (@Data,@User,0)
end
GO
if OBJECT_ID('IndisPonibilidadesRecursos') is null
	EXEC('CREATE PROCEDURE IndisPonibilidadesRecursos AS SET NOCOUNT ON;')
GO
alter proc IndisPonibilidadesRecursos @Data date, @Resource int as
if exists (select * from DisponibilidadeRecurso Where Data=@Data and Recurso=@Resource)
begin
	delete from DisponibilidadeRecurso where Data=@Data and Recurso=@Resource
end
else
begin
	insert into DisponibilidadeRecurso(Data,Recurso,Disponivel) values (@Data,@Resource,0)
end
GO
if OBJECT_ID('LogLoginProc') is null
	EXEC('CREATE PROCEDURE LogLoginProc AS SET NOCOUNT ON;')
GO
alter proc LogLoginProc @User varchar(255)='', @IpAddress varchar(255)='169.254.255.254' as
	insert into LogLogin (Utilizador,IPaddress) values (@User,@IpAddress)
GO
if OBJECT_ID('CategoriasProc') is null
	Exec('Create Procedure CategoriasProc as set nocount ON;')
GO
alter proc CategoriasProc @Verb varchar(100), @Name varchar(255),@NewName varchar(255)='' as
	if @Verb='Insert'
	begin
		insert into Categoria values (@Name)
	end
	else if @Verb='Update'
	begin
		Update Categoria set Nome=@NewName Where Nome=@Name
	end
	else if @Verb='Delete'
	begin
		delete from Categoria where Categoria.Nome=@Name
	end
GO
if OBJECT_ID('PermissoesProc') is null
	Exec('Create Procedure PermissoesProc as set nocount ON;')
GO
alter proc PermissoesProc @Verb varchar(100), @Name varchar(255),@NewName varchar(255)='' as
	if @Verb='Insert'
	begin
		insert into Permissao values (@Name)
	end
	else if @Verb='Update'
	begin
		Update Permissao set Nome=@NewName Where Nome=@Name
	end
	else if @Verb='Delete'
	begin
		delete from Permissao where Nome=@Name
	end
GO
if OBJECT_ID('CategoriasUtilizadorProc') is null
	Exec('Create Procedure CategoriasUtilizadorProc as set nocount ON;')
GO
alter proc CategoriasUtilizadorProc @Verb varchar(100), @Username varchar(255),@categoria varchar(255) as
	if @Verb='Insert'
	begin
		insert into CategoriaUtilizador (Utilizador,Categoria) values (@Username,@categoria)
	end
	else if @Verb='Delete'
	begin
		delete from CategoriaUtilizador where Utilizador=@Username
	end
GO
if OBJECT_ID('UtilizadoresProc') is null
	Exec('Create Procedure UtilizadoresProc as set nocount ON;')
GO
alter proc UtilizadoresProc @Verb varchar(255)='', @Username varchar(255)='', @Nome varchar(255)='', @Email varchar(255)='', @Permissao varchar(255)='',@Password varchar(255)='',@FirstPassword varchar(255)='' as
	if @Verb='Insert'
	begin
		insert into Utilizador (Username,Nome,Email,Permissao,Password,FirstPassword)
		values (@Username,@Nome,@Email,@Permissao,@Password,@FirstPassword)
	end
	if @Verb='Reset'
	begin
		Update Utilizador Set Password=@Password, FirstPassword=@Password
		Where Username=@Username or Email=@Email
	end
	if @Verb='Change'
	begin
		Update Utilizador set Password=@Password 
		Where Username=@Username or Email=@Email
	end
	if @Verb='Update'
	begin
		Update Utilizador set Nome=@Nome, Email=@Email,Permissao=@Permissao
		Where Username=@Username
	end
	if @Verb='Delete'
	begin
		Delete from Utilizador
		Where Username=@Username or Email=@Email
	end
GO
if OBJECT_ID('ClientesProc') is null
	Exec('Create Procedure ClientesProc as set nocount ON;')
GO
alter proc ClientesProc @Verb varchar(255)='',@Entidade varchar(255)='', @Nome varchar(255)='',@Morada varchar(255)='', @Contacto varchar(255)='', @EntidadeNova varchar(255)='' as
	if @Verb='Insert'
	begin
		insert into Cliente values (@Entidade,@Nome,@Morada,@Contacto)
	end
	if @Verb='Update'
	begin
		update Cliente set Entidade=@EntidadeNova, Nome=@Nome,Morada=@Morada,Contacto=@Contacto Where Entidade=@Entidade
	end
	if @Verb='Delete'
	begin
		Delete from Cliente Where Entidade=@Entidade
	end
	if @Verb='Select'
	begin
		select * from Cliente Where Entidade=@Entidade
	end
	if @Verb='Search'
	begin
		Select * from Cliente Where Nome like '%'+@Nome+'%'
	end
GO


if OBJECT_ID('FormacoesOfRecursoNoConsume') is null
	Exec('Create Procedure FormacoesOfRecursoNoConsume as set nocount ON;')
GO
alter proc FormacoesOfRecursoNoConsume @Recurso int=0 as
	select Formacao.* from Formacao 
	inner join DisponibilidadeRecurso on Formacao.Id = DisponibilidadeRecurso.Formacao
	Where DisponibilidadeRecurso.Recurso=@Recurso
	order by Formacao.Id DESC
GO

if OBJECT_ID('LoginsAutdit') is null
	Exec('Create Procedure LoginsAutdit as set nocount ON;')
GO
alter proc LoginsAutdit @Data date=null as
	if @Data is null
	begin
		Select Month(getDate()) as Mes,YEAR(getdate())as Ano ,Count(*) as NrLogins from LogLogin
		order by YEAR(getdate()),Month(getDate())
	end
	else
	begin
		Select Month(getDate()) as Mes,Count(*) as NrLogins from LogLogin
		Where YEAR(getdate())=Year(@Data)
		order by Month(getDate())
	end
GO
-- Views
if OBJECT_ID('MonthLog') is not null
	drop view MonthLog
GO
--Create View MonthLog as 
--	Select Month(getDate()) as Mes,YEAR(getdate())as Ano ,Count(*) as NrLogins from LogLogin
--	order by YEAR(getdate()),Month(getDate())
--	Where MONTH(getDate())=MONTH(Data) AND YEAR(Getdate())=YEAR(Data)
--GO
if OBJECT_ID('LoginLogs') is not null
	drop view LoginLogs
GO
create view LoginLogs as 
	Select * from LogLogin
GO
if OBJECT_ID('CategoriasView') is not null
	drop view CategoriasView
GO
create View CategoriasView as
select Nome as Nome from Categoria
GO
if OBJECT_ID('PermissoesView') is not null
	drop view PermissoesView
GO
create View PermissoesView as
select Nome as Nome from Permissao
GO
if OBJECT_ID('ClientesView') is not null
	drop view ClientesView
GO
create View ClientesView as
select * from Cliente
GO
-- WIP
--if OBJECT_ID('AuditUtilizador') is not null
--	drop trigger AuditUtilizador
--GO
--create trigger AuditUtilizador on Utilizador 
--after insert, update, delete as
--	if (select * from inserted) is null
--		begin
--			insert into Auditoria (Utilizador,ActionType,AffectedTable,BeforeJSON) values (SYSTEM_USER,'Delete','Utilizador','')
--		end
--	if (select * from deleted) is null
--		begin
--			insert into Auditoria (Utilizador,ActionType,AffectedTable,BeforeJSON) values (SYSTEM_USER,'Insert','Utilizador','')
--		end
--	if (select * from deleted) is not null and (select * from inserted) is not null
--		begin
--			insert into Auditoria (Utilizador,ActionType,AffectedTable,BeforeJSON) values (SYSTEM_USER,'Update','Utilizador','')
--		end
--GO

--  SELECT * FROM TableName ORDER BY id OFFSET 10 ROWS FETCH NEXT 10 ROWS ONLY
--alter proc PaginatedConsumes @Nome varchar(255)='', @Categoria varchar(255), @rows int=0 as
--select * from RecursoConsumivel
--Where Nome like '%'+@Nome+'%' and Categoria like'%'+@Categoria+'%'
--order by Id
--OFFSET @rows ROWS FETCH NEXT 3 ROWS ONLY
--GO 